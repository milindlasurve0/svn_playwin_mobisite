<div data-role="footer" data-position="fixed" >
    <div> &copy; Pan India Network Ltd. <a href="../pages/Disclaimer.php">Disclaimer</a> , <a href="../pages/TandC.php">T & C </a> </div>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-50445611-1', 'myplaywin.com');
      ga('send', 'pageview');

    </script>
  </div>