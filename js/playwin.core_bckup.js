var Playwin={};

Playwin.core={
    userDetails:{
        name:"",
        mobile:"",
        email:""
    },
    getParseClass:function(d,b,a){
        var c=Playwin.core.parseStr(d)+" "+Playwin.core.parseStr(b)+" "+Playwin.core.parseStr(a)
        },
    callUrlInBrowser:function(a,b){
        window.open(a,"_system")
        },
    parseStr:function(b){
        var a="";
        a=b.split(" ").join("1");
        a=a.split("(").join("2");
        a=a.split(")").join("3");
        a=a.split(".").join("4");
        a=a.split("[").join("5");
        a=a.split("]").join("6");
        return a
        },
    init:function(a){
        console.log("Inside Core INIT .");
        Playwin.core.loadResultData({});
        Playwin.core.loadAdsData({});
        AppNamespace.app.navigate("Home")
        },
    exit:function(){},
    loadAdsData:function(a){
        if(a.viewModel){
            a.viewModel.loadPanelVisible(true)
            }
            $.ajax({
            url:Playwin.config.urls.addMarketting,
            method:"GET",
            dataType:"JSON"
        }).done(function(b){
            var c=b;
            b=null;
            if(!c){
                return false
                }
                $.each(c,function(e,d){
                if(d.type=="image"){
                    window.localStorage.setItem("image"+d.id+"url",d.imageUrl);
                    window.localStorage.setItem("image"+d.id+"rurl",d.redirectUrl)
                    }else{
                    if(d.type=="text"){
                        window.localStorage.setItem("text"+d.id,JSON.stringify(d.text))
                        }
                    }
                if(d.id==6){
                str=d.text;
                advsData=null;
                return false
                }
            });
        window.localStorage.setItem("advsMarketting",JSON.stringify(c));
        c=null;
        if(a.viewModel){
            a.viewModel.loadPanelVisible(false)
            }
            if(a.navigateTo){
            AppNamespace.app.navigate(a.navigateTo)
            }
        }).fail(function(b,d,c){
    if(a.viewModel){
        a.viewModel.loadPanelVisible(false)
        }
        if(a.navigateTo){
        AppNamespace.app.navigate(a.navigateTo)
        }
        console.log("Error : Unable get ads & marketting info.")
    })
},

setBannerImage:function(ad_banner){


       $.ajax({
           url:Playwin.config.urls.addMarketting,
           method:"GET"
           
       }).done(function(b){
           var c=JSON.parse(b);
                 
                   
           $.each(c, function(k, v) {
               //
               if(c[k].pageName==ad_banner){
                   
                           
                   $('a.'+ad_banner).attr('href',c[k].redirectUrl);
                   $('img.'+ad_banner).attr('src',c[k].imageUrl);
               }
                     
           });
       //
       }).fail(function(c){
                   
           });



   },
 

loadResultData:function(listType){
    var b=[];
    //alert(listType);
    //var a=new $.Deferred();
    /*if(d.viewModel){
        d.viewModel.loadPanelVisible(true)
        }*/
    //var c=JSON.parse(window.localStorage.getItem("configObj"));
    //Playwin.config=c?c:getConfigPlainObj();
    //c=null;
    //$.get(Playwin.config.urls.results).done(function(e){
    
    $.support.cors = true;
    $.mobile.allowCrossDomainPages = true;
    $.ajax({
                type:"GET",
                url:Playwin.config.urls.results
            })
    .done(function(e){
        
        $( "#resultLoading" ).popup( "close" );
        
        //$("#loadingDiv").hide();
        var f=$.xml2json(e);
        e=null;
        
        if(!f.GameResult){
            alert("Game Results Load Error .");
            return false
            }
            b=f.GameResult.length==0?[]:f.GameResult;
        f=null;
        $.each(b,function(g,i){
            var h={};
            
            $.each(Playwin.config.gameList,function(l,n){
                //alert(n.name+"=="+i.Game);
                if(n.name==i.Game){
                    Playwin.config.gameList[l].lastResult={
                        Game:i.Game,
                        Result:i.Result,
                        ResultHtml:"",
                        DrawDate:Playwin.core.getFormatedDateTime(i.DrawDate,"ddd, dd-MMM-yyyy hh:mm TT"),
                        NextDrawDate:Playwin.core.getFormatedDateTime(i.NextDrawDate,"ddd, dd-MMM-yyyy hh:mm TT"),
                        NextDrawDateNF:i.NextDrawDate,
                        NextFirstPrize:i.NextFirstPrize,
                        RolldownAmount:i.RolldownAmount?i.RolldownAmount:"",
                        isMoreBalls:false
                    };
                    
                    var k=$.trim(i.Result);
                    var m=k.split(" ");
                    var j=0;
                    $.each(m,function(o,p){
                        if($.trim(p)!=""){
                            j++;
                            if(j==8){
                                Playwin.config.gameList[l].lastResult.isMoreBalls=true;
                                Playwin.config.gameList[l].lastResult.ResultHtml="<div  style='float:left;position:relative; padding:0px;'></div>"
                            }
                                if(j<=7){
                                    Playwin.config.gameList[l].lastResult.ResultHtml=Playwin.config.gameList[l].lastResult.ResultHtml+"<div "+(p!="TB"?"class='resultBall'>":"style='float:left;padding-top:5px;'>")+p+"</div>"
                                }
                           // alert("---> "+l);
                            
                            
                        }
                    })
                    
            }
            })
            
            
    });
    
    if(listType=="result"){
        $.each(Playwin.config.gameList,function(i,v){
            if(!v.lastResult.isMoreBalls){
                 $("#resultDiv"+v.id).html(v.lastResult.ResultHtml);
            }else{
                $("#resultDiv"+v.id).html("<div><a class=\"show_more_balls\" id = \"moreBalls_"+v.id+"\" >View Results</a></div>");
            }
            
            if(v.lastResult.RolldownAmount && v.lastResult.RolldownAmount != "" ){
                 $("#rolldown_amt_"+v.id).html( "&nbsp;&nbsp;Rolldown:"+v.lastResult.RolldownAmount);
            }          
            $("#draw_date_"+v.id).html(v.lastResult.DrawDate);
        });  
        $(".show_more_balls").on('click', function() {
              // alert("Helo");
               var arr = this.id.split("_");
               Playwin.core.showMoreBalls(arr[1]);                                           
               return false;
        });
    }
    //window.localStorage.setItem("configObj",JSON.stringify(Playwin.config));
    /*b=null;
    Playwin.config={
        urls:Playwin.config.urls
    };*/
    var type = window.location.hash;
    if(!(window.location.hash == "#daily" || window.location.hash == "#weekly")){
       type = "all";
    }
    var gameList=[];
   $.each(Playwin.config.gameList,function(i,v){
       if(type == "all" || type == "#"+v.type ){              
           gameList.push(v);
        }
        $("#jackpot_"+v.id).html(v.lastResult.NextFirstPrize);
        //$("#jackpot_"+v.id).html(v.lastResult.NextFirstPrize);
        
   });
   if(listType == "play"){
       window.setTimeout(
        function() {
            if($(".timers").length==0){
                window.setTimeout(
                    function() {
                        Playwin.core.setTimers(gameList);
                    }, 5000);
            }else{
                Playwin.core.setTimers(gameList);
            }
       }, 3000);   
   }
    
 })
 .fail(function(e,g,f){
        $( "#resultLoading" ).popup( "close" );
        alert("Error : Unable get results info.");
        //DevExpress.ui.dialog.alert("Error : Unable get results info.","Error!")
    })
},
showMoreBalls:function (gid){
            var html="";
            $.each(Playwin.config.gameList,function(gli,glv){
                if(glv.id==gid){
                    var rStr=$.trim(glv.lastResult.Result);
                    var rArr=rStr.split(" ");
                    $.each(rArr,function(ri,rv){
                        if($.trim(rv)!=""){
                            html=html+"<div "+(rv!="TB"?"class='resultBall'>":"style='float:left;padding-top:5px;'>")+rv+"</div>"
                        }
                    })
                }
            });
            $("#showMoreBalls").html(html);
             $( "#showMoreBallsPopUp" ).popup("open");
        },
 setTimers:function(dataArr){
                                            //alert(JSON.stringify(dataArr));
                                            Playwin.core.TimerPlugin.Timer = [];
                                            Playwin.core.TimerPlugin.TotalSeconds = [];
                                             var x = "";
                                             var id = 0;
                                            $(".timers")
                                            .each(
                                                function() { //alert(this.id);
                                                    var arr = this.id
                                                    .split("_");
                                                    if (arr[0] == "timer") {//alert(arr[1]);
                                                        x = "";
                                                        id = 0;
                                                        $
                                                        .each(
                                                            dataArr,
                                                            function(
                                                                i,
                                                                v) {
                                                                    //alert(JSON.stringify(v));
                                                                   // alert(v.id);
                                                                   // alert(arr[1]);
                                                                   // alert(parseInt(v.id) == arr[1]);
                                                                if (parseInt(v.id) == arr[1]) {
                                                                    x = v.lastResult.NextDrawDateNF;
                                                                    id = v.id
                                                                }else{
                                                                    
                                                                }
                                                            });
                                                            //alert(x);
                                                        if (x == undefined || x == "") {
                                                            //alert("uder");
                                                            return true;
                                                        }
                                                        //alert(x);
                                                        var dtStr = x
                                                        .split(" ");
                                                        var dtStrArr = dtStr[0]
                                                        .split("/");
                                                        var tmStrArr = dtStr[1]
                                                        .split(":");
                                                        var dt = new Date(
                                                            dtStrArr[2],
                                                            dtStrArr[1] - 1,
                                                            dtStrArr[0],
                                                            tmStrArr[0],
                                                            tmStrArr[1],
                                                            tmStrArr[2],
                                                            0);
                                                            
                                                        var diff = parseInt((dt
                                                            .getTime() / 1000)
                                                        - (new Date()
                                                            .getTime() / 1000));
                                                       //alert(diff);
                                                        Playwin.core.TimerPlugin
                                                        .createTimer(
                                                            "timer_"
                                                            + id,
                                                            diff)
                                                    }
                                                });
                                            window.setTimeout("Playwin.core.TimerPlugin.Start()",1000)

                                        },
getFormatedDateTime:function(g,d,c){
    var e;
    if(c){
        e=g
        }else{
        if(d==null||d==""){
            d="dddd, dd-MMM-yyyy hh:mm TT"
            }
            var a=g.split(" ");
        var f=a[0].split("/");
        var b=a[1].split(":");
        var e=new Date(f[2],f[1]-1,f[0],b[0],b[1],b[2],0);
        f=null;
        b=null
        }
        return($.fullCalendar.formatDate(e,d))
    },
registerUser:function(a){},
backKeyDown:function(a){
    AppNamespace.app.navigate("#_back")
    },
TimerPlugin:{
    Timer:[],
    TotalSeconds:[],
    State:"",
    createTimer:function(b,a){
        Playwin.core.TimerPlugin.Timer.push(document.getElementById(b));
        Playwin.core.TimerPlugin.TotalSeconds.push(a)
        },
    Start:function(){
        if(Playwin.core.TimerPlugin.State!="Running"){
            Playwin.core.TimerPlugin.State="Running";
            Playwin.core.TimerPlugin.Tick()
            }
        },
Tick:function(){
    $.each(Playwin.core.TimerPlugin.TotalSeconds,function(b,a){
        if(Playwin.core.TimerPlugin.TotalSeconds[b]<=0){
            return true
            }
            Playwin.core.TimerPlugin.TotalSeconds[b]-=1;
        Playwin.core.TimerPlugin.UpdateTimer(b)
        });
    if(Playwin.core.TimerPlugin.State=="Running"){
        window.setTimeout("Playwin.core.TimerPlugin.Tick()",1000)
        }
    },
UpdateTimer:function(b){
    var f=Playwin.core.TimerPlugin.TotalSeconds[b];
    var e=Math.floor(f/86400);
    f-=e*86400;
    var d=Math.floor(f/3600);
    f-=d*(3600);
    var a=Math.floor(f/60);
    f-=a*(60);
    var days = "";
    if(e > 1){
        days = e+" days ";
    }else if(e == 1){
        days = e+" day ";
    }
    var c = days + Playwin.core.TimerPlugin.LeadingZero(d)+":"+Playwin.core.TimerPlugin.LeadingZero(a)+":"+Playwin.core.TimerPlugin.LeadingZero(f);
    Playwin.core.TimerPlugin.Timer[b].innerHTML=c
    },
LeadingZero:function(a){
    return(a<10)?"0"+a:+a
    }
}
};