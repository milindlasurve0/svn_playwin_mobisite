/*
 *game play script for Playwin android app
 */
/*
BetString	3,6,13,14,10
DrawPointer	1
GameID	9
ItzNumber	867012140114
ItzPassword	1628
TotalDraw	1
sale_amount	10
 */
var Playwin = (typeof Playwin !== undefined) ? Playwin : {};

Playwin.play = {
    selectedGame : 0,
    jsonDetails : {
        currJackpot : {
            id : "",
            name : "",
            amt : "",
            displayName : ""
        },
        draw : {
            id : "",
            name : ""
        },
        nextDraw : "",
        noOfDraw : "",
        ticketPrice: 0,
        totalTicketPrice: 0,
        tickets : []
    },
    playGameResponse:[],
    tabClick:function(data){
            
            if(data=="draws"){
                this.tabVisible(true);
                this.notTabVisible(false);
                $("#tab1").addClass("sel");
                $("#tab2").removeClass("sel");
                
                var val=$("#draw_options").val();
                var arr=val.split("_",3);
                var DrawID=arr[2];
                $("#all_draws").val(DrawID);
                $("#no_draw").val("1");
                var sum=Playwin.play.calculateTotalBetAmount()
                
                
            }else{
                if(data=="no_draws"){
                    $("#tab2").addClass("sel");
                    $("#tab1").removeClass("sel");
                    this.tabVisible(false);
                    this.notTabVisible(true)
                }
            }
        },
    playGameSuccessRender:function (){
                var response = JSON.stringify(Playwin.play.playGameResponse);
                var redirectUrl = "betPlaceTicket.php?typePage="+$("#typePage").val()+"&gameType="+$("#gameType").val()+"&response="+response;
                document.location.replace(redirectUrl);
                
                return;
                var htmlStr = "";
                //alert(Playwin.play.playGameResponse.toSource());
                 $.each(Playwin.play.playGameResponse.tickets,function(i,ticket){
                    if(ticket.err){
                      htmlStr = htmlStr +  "<div >"
                            +"<div >"+ticket.errMsg+"</div>"
                          +"</div>";
                    }else{
                        htmlStr = htmlStr +  "<div class=\"resultTable\">"+
                            
                    "<div >";
                    //$.each(ticket.details.Panel , function(j,panel){
                        htmlStr = htmlStr + "<div  class=\"resultRow\">"+
                          "<div class=\"resultData\">"+ticket.details.Panel.BetLine+"</div></br>"+
                          "<div class=\"resultData\">"+(ticket.details.Panel.LP==0?'LP:0':'')+"</div>"+
                          "</div>";
                    //});
                    
                    htmlStr = htmlStr + "</div>"+
                    
                    /*"<div class=\"resultRow\" style=\"margin-top:10px\">"+
                      "<div class=\"resultTitle\">Total No Of Bets</div>"+
                      "<div  class=\"resultData\">"+ticket.details.Panel.length+"</div>"+
                    "</div>"+*/
                    "<div class=\"resultRow\">"+
                      "<div class=\"resultTitle\">M.R.P (Incl all taxes)</div>"+
                      "<div  class=\"resultData\">"+ticket.details.Mrp+"</div>"+
                    "</div>"+
                    "<div class=\"resultRow\">"+
                      "<div class=\"resultTitle\">Total </div>"+
                      "<div  class=\"resultData\">"+ticket.details.Cost+"</div>"+
                    "</div>"+
                    "<div class=\"resultRow\">"+
                      "<div class=\"resultTitle\">Bet Date </div>"+
                      "<div class=\"resultData\">"+ticket.details.BetDate+"</div>"+
                    "</div>"+
                    "<div class=\"resultRow\">"+
                      "<div class=\"resultTitle\">Draw </div>"+
                      "<div  class=\"resultData\">"+ticket.details.DrawID+"</div>"+
                      "<div class=\"resultTitle\" style=\"padding-left:10px\"> on </div>"+
                      "<div  class=\"resultData\">"+ticket.details.DrawDate+"</div>"+
                    "</div>"+
                    "<div class=\"resultRow\">"+
                      "<div  class=\"resultData\">"+ticket.details.TSN+"</div>"+
                      "<div  class=\"resultData\">"+ticket.details.SLSN+"</div>"+
                    "</div>"+

                  "</div>";
                          
                    }
                    //htmlStr = htmlStr + "";
                 });
                htmlStr = htmlStr + "<div class=\"resultTable\">"
                +"<div class=\"resultRow\">"
                  +"<div data-bind=\"dxButton: {text: 'Play Again'},click: playGameBtn , clickBubble: false\"></div>"
                +"</div>"
                +"<div class=\"resultRow\">"
                 +"<div style=\"font-size:1.1em; font-weight:bold\">Your Card Balance is: <span >"+Playwin.play.playGameResponse.CardBalance+"</span></div>"
                  +    "<div class=\"dx-button2\" style=\"float:left;width:72px;margin-left:10px;\">"
                    +"<span id=\"gameplay_"+Playwin.play.selectedGame+"\" class=\"dx-button-text gameplay\">GamePlay</span>"
                    +"</div></div>"
                  +"<!-- add here-->"
                  +"<div class=\"adSpaceHomeBig\"><a data-bind=\"click:openBannerRUrl.bind($data,bannerImgRUrl())\"><img data-bind=\"attr:{src:bannerImgUrl}\"></a></div>"
              "</div>";
              
              $("#playGameSuccessContent").html(htmlStr);
              $("#playGamePage").hide();
              $("#playGameSuccess").show();
              
              
              },
    init : function(gid) {
		Playwin.play.loadData(gid);
    /*var request = $.ajax({
			type : "GET",
			url : Playwin.config.urls.gameDetails,// http://www.ashops.local/shops/testXml
			success : function(xmlData) {
				// var xmlDoc = $.parseXML( xmlStr );
				
				var jsonObj = $.xml2json(xmlData);
				$.each(jsonObj.Draw, function(i, val) {
					var g = "Playwin.config.gameDetails." + "g" + val.GameID;
					var gid = "Playwin.config.gameDetails." + "g" + val.GameID
							+ ".draws";

					if (eval(g) && eval(gid)) {

						eval(gid).push(val);

					}
				});

			},
			complete : function(xmlData) {
				// on complete event
				$(function() {
					Playwin.play.loadData(gid);
				});
				
			},
			error : function(xhr, err) {
				alert("Error : Game details load Error .");
			}

		});*/

    },
    loadData : function(default_game) {
        Playwin.play.selectedGame = default_game;
        // if(default_game="")default_game = "4" ;
        var no_draw = 7;
		

        // set current draw
        var game_details = eval("Playwin.config.gameDetails." + "g"
            + Playwin.play.selectedGame);
        // alert(game_details);
        // var g = "games."+"g"+default_game;
        // default_game_details = eval(g);
        //if ( $('#myDiv')[0] ) { alert("Hello"); }else{alert("no");}
        $("#c_draw").html(game_details.name + "( " + game_details.amt + " )");
        $("#c_draw_name").html(game_details.name);
        Playwin.play.jsonDetails.currJackpot.id = Playwin.play.selectedGame;
        Playwin.play.jsonDetails.currJackpot.name = game_details.name;
        Playwin.play.jsonDetails.currJackpot.amt = game_details.amt;
        Playwin.play.jsonDetails.currJackpot.displayName = game_details.name + "( "
        + game_details.amt + " )";
        if(Playwin.play.selectedGame == "9"){
            Playwin.play.jsonDetails.ticketPrice = 0;
			
        }else {			
            Playwin.play.jsonDetails.ticketPrice = game_details.ticketPrice;
        }
		
        // ---------------------

        /*// render no of draw list
		var no_draw_option = ""
		for (i = 1; i <= no_draw; i++) {
			no_draw_option = no_draw_option + "<option value='" + i + "'>" + i
					+ "</option>";
		}
		$("#no_draw").html(no_draw_option);
		// ---------------------------------*/

        /*// render no of draws selection list
		var no_draw_option = ""
		for (i = 1; i <= no_draw; i++) {
			no_draw_option = no_draw_option + "<option value='" + i + "'>" + i
					+ "</option>";
		}
		$("#no_draw").html(no_draw_option);
		// ---------------------------------*/

        /*// render available draws for selected game
		var avail_draw_options = "";
		$.each(game_details.draws, function(i, val) {
			avail_draw_options = avail_draw_options
					+ "<option value='"
					+ val.DrawID
					+ "'>"
					+ Playwin.core.getFormatedDateTime(val.DrawTime,
							"dddd, dd-MMM-yyyy hh:mm TT") + "</option>";
		});
		//$("#all_draws").html(avail_draw_options);*/
        // ---------------------------------

        /*Playwin.play.jsonDetails.nextDraw = Playwin.core.getFormatedDateTime(
				game_details.draws[0].DrawTime, "dddd, dd-MMM-yyyy hh:mm TT");
		$("#next_draw").html(Playwin.play.jsonDetails.nextDraw);*/

        /*$("#all_draws").bind(
				"change",
				function() {
					if (game_details.draws[0].DrawID != $("#all_draws").val()) {
						$("#no_draw").html("<option value='1'>1</option>");
					} else {
						var no_draw_val = $("#no_draw").val();
						var no_draw_option = "";
						for (i = 1; i <= no_draw; i++) {
							no_draw_option = no_draw_option + "<option value='"
									+ i + "'>" + i + "</option>";
						}
						$("#no_draw").html(no_draw_option);
						$("#no_draw").val(no_draw_val);
					}
				});*/

        $("#new_row_id").val(2);

        $(document).on("click", "a.row_remove", function() {
            var id = this.id;
            var idArr = id.split("_");
            $("#row_" + idArr[2]).remove();
            Playwin.play.calculateTotalBetAmount();
            // alert(idArr[2]);
            return false;
        });
        /*$("#row_add").bind("click", function() {
			var id = $("#new_row_id").val();
			// alert(id);
			if ($('#draws_table tr').length > 11) {// add only 10 rows at a
													// time
				alert("You can not add more then 10 tickets .");
				return false;
			}

			Playwin.play.addTicketRow(Playwin.play.selectedGame, id);
			id = 1 + parseInt(id);
			$("#new_row_id").val(id);

			// $("#row_id_"+id).val(id);
			return false;
		});*/

        

        

        // for keno game
        if (Playwin.play.selectedGame == "9") {
            var no_spot_min = 2;
            var no_spot_max = 10;
            var amt_min = 10;
            var amt_max = 100;

            // render no of spot selection list
           
            // ---------------------------------
            $(document)
            .on(
                "change",
                "#amt_option",
                function() {
                    // show prize structure table
                    //alert("hello");
                    var tblStr = "<table><thead><td>Match</td><td>Prize</td></thead><tbody id='prize_structure'></tbody></table>";
                    $("#prize_amt_div").html(tblStr);
                    tblBody = "";
                    var matchPrz = Playwin.play.getPrizeStructure(
                        $("#spot_option").val(), $(
                            "#amt_option").val());
                    //alert(matchPrz.toSource());
                    $.each(matchPrz, function(i, val) {
                        tblBody = tblBody + "<tr><td>" + val.match
                        + "</td><td>" + val.prize
                        + "</td></tr>";
                    })
                    $("#prize_structure").html(tblBody);

                });
        //			$(document)
        //					.on(
        //							"click",
        //							"#keno_row_confirm",
        //							function() {
        //							});

        }

    },
    getPrizeStructure : function(spot, amt) {
        var ps = eval("Playwin.config.gameDetails.g9.prizeStructure." + "s"
            + spot);
        var structure = [];
        $.each(ps, function(i, val) {
            var temp = {
                match : "",
                prize : ""
            };
            temp.match = i.split("m")[1];
            temp.prize = val * amt * 10;
            temp.prize = temp.prize >= 6000000 ? 6000000 : temp.prize;
            structure.push(temp);
        });
        return structure;
    },
    addTicketRow : function(gid, id) {
        Playwin.play.kenoRowConfirm();
        /*var row = "<tr id='row_" + id + "'>";
       
        var gameDetails = eval("Playwin.config.gameDetails.g" + gid);
        var gameGroups =gameDetails.ballGroups;
        $.each(gameGroups, function(n, gd) {// iterate for each groups
            $.each(gd.balls, function(m, bd) {// iterate for each ball in
                // group

                row = row + "<td style=''><div class=\"ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset\">" + "<input id='txtp" + id + "t_" + n
                + "' name='txtp" + id
                + "[]' maxlength='2' class='lottotbox lb" + id + " tp"
                + id + " " + n + id + "' type='text'>" + "</div></td>";
            });
        });
       
        row = row
        + "<td style='' width='45px'>"
        + "<div class=\" ui-checkbox\"><input id='row_lb_"
        + id
        + "' class='row_lp' type='checkbox'>"
        + "</div></td>"
        + "<td style='' width='45px' id='row_adrm_"
        + id
        + "'>"
        + "<input type='hidden' id='row_id_"
        + id
        + "' name='row_id[]' value='"
        + id
        + "'/><input type='hidden' id='ticket_price_"
        + id
        + "' name='ticket_price[]' class='ticket_price' value='"
        + gameDetails.ticketPrice
        + "'/><a href='#' id='row_rm_"
        + id
        + "' class='row_remove' title='Remove Draw'> <strong> Del <strong> </a>"
        + "</td>" + "</tr>";

        $("#draws_table").append(row);
        var amt = parseInt($("#total_amount").val());
        amt = amt + 10;
        $("#total_amount").val(amt);
       */ 
        
    },
    validateTickets : function(gid) {
        
        if($("tbody#draws_table tr").length <= 1 ){
            return {
                'status':false , 
                'msg':"Please Enter at least one ticket!"
            };
        }
        var tickets = [];
        var rows = [];
        var duplicate;
        var noInRange;
        var dup_rows;
        dup_rows = [];
        var gd = eval("Playwin.config.gameDetails." + "g" + gid);
        var notinrange_rows;
        notinrange_rows = [];
        var ret = {
            "status" : true,
            "dup_rows" : [],
            "notinrange_rows" : []
        };

        // duplicate = false;
        $.each($("input[name='row_id[]']"), function() {
            rows.push($(this).val());
        });
        // if(rows.length != $("#no_draw").val()){
        // alert("No of draws and no of tickets should be equal .");
        // ret.status = false;
        // }else{
        $
        .each(
            rows,
            function(i, val) {// for each row of ticket

                var ticketData = $('input.lb' + val).map(
                    function() {
                        return this.value;
                    }).toArray();
                var ticketDet = {
                    "lp_flag" : $("#lp_row_" + val).prop("checked"),
                    "values" : ticketData
                };
                tickets.push(ticketDet);
							
                $
                .each(
                    gd.ballGroups,
                    function(n, gdet) {// iterate for
                        // each groups

                        var list_of_values = $('input.' + n + (val))
                        .map(function() {
                            return this.value;
                        }).toArray();
                        // alert('input.'+n+val);
                        // alert(list_of_values);
                        duplicate = false;
                        noInRange = true;

                        if (!$("#row_lb_" + val).prop(
                            "checked")
                        && Playwin.play.selectedGame != "9") { // if
                            // lucky
                            // pic
                            // then
                            // discard
                            // that
                            // row
                            // for
                            // validate
                            $
                            .each(
                                list_of_values,
                                function(j,
                                    x) {
                                    noInRange = parseInt(x) >= gdet.limit.min
                                    && parseInt(x) <= gdet.limit.max;
                                    duplicate = list_of_values
                                    .indexOf(x) !== list_of_values
                                    .lastIndexOf(x);
                                    if (duplicate) {
                                        dup_rows
                                        .push("r"
                                            + (i + 1)
                                            + n);
                                        return false;
                                    } else if (!noInRange) {
                                        notinrange_rows
                                        .push("r"
                                            + (i + 1)
                                            + n);
                                        return false;
                                    }
                                ;
                                });
                        }

                    });

            });

        // }

        ret.tickets = tickets;
        if (dup_rows.length > 0 || notinrange_rows.length > 0) {
            ret.status = false;
            ret.dup_rows = dup_rows;
            ret.notinrange_rows = notinrange_rows;
        }
        // alert(ret.toSource());
        return ret;

    },
    initCardSubmit : function(type) {
		// Render ticket and selected details
		//$("#c_draw").html(Playwin.submitCard.ticketsJson.currJackpot.name);
		//$("#selected_draw").html(Playwin.submitCard.ticketsJson.nextDraw);
		//$("#no_draw").html(Playwin.submitCard.ticketsJson.noOfDraw);
		//$("#amount").html(Playwin.submitCard.ticketsJson.currJackpot.amt);
		// bind submit card event
                $( "#playCardSubmitPopUp" ).popup("open");  
		$("#cardSubmitType").val(type);

	},
        validateRow:function(chkId){
            var chkId=1;
            if($("#spot_option").val() == ""){
                    alert("Please select a spot .");return false;	
            }else  if($("#amt_option").val() == ""){
                    alert("Please select a amount to bet .");return false;
            }

            
            //---------------new -----------
            
            var list_of_values = $('input.spot_inputs').map(function() {
                                                return this.value;
                                })
                                .toArray();
           
            
            var g=list_of_values.slice(0);
            var gtemp=list_of_values.slice(0);
            if(Playwin.play.selectedGame==2){
                g.pop()
                }
                g=g.sort();
              
            var j=false,a=[];
            for(var b=0;b<g.length;b++){
                
                if(g[b]!=0&&g[b+1]==g[b]){
                    j=true;
                    break
                }
                if(parseInt(g[b]) != g[b]){
                    j=true;
                    break
                }
            }
            var ret;
            if(j){
                alert("Please select UNIQUE Ticket Values .","Error!");  
                ret = false;
            }else{
                ret = true;
            }
           
                var gameGroups = eval("Playwin.config.gameDetails.g"+Playwin.play.selectedGame+".ballGroups");
                $.each(gameGroups,function(grp, gdet) {
                        if(ret){
                            var list_of_values = $('input.' + grp).map(function() {
                                    return this.value;
                                }).toArray();
                                
                                $.each( list_of_values, function(j, x) {
                                        var noInRange = parseInt(x) >= gdet.limit.min
                                        && parseInt(x) <= gdet.limit.max;
                                        /*var duplicate = list_of_values
                                        .indexOf(x) !== list_of_values
                                        .lastIndexOf(x);*/
                                        /*if (duplicate) {
                                           alert("duplicate values");
                                            ret = false;
                                            return true;
                                        } else
                                            */
                                        if (!noInRange) {
                                            alert("Ticket values are not in range.");
                                            ret = false;
                                            return false;
                                        }

                              });
                        }
          
                });
          
            
            
            
            
            
            return ret;
            //------------------------------


            //var id = $("#new_row_id").val();
            //var row = "<tr id='row_" + id + "'>";//
            var gameGroups = eval("Playwin.config.gameDetails.g"+Playwin.play.selectedGame+".ballGroups");
            // validate ticket data
            var dup = false;
            var nor = false;
            if (!$("#row_tcktprc_"+chkId).prop("checked")) {
                    $.each(
                                    gameGroups,
                                    function(n, gdet) {// iterate for each group
                                            if (!$("#lp_spotop_"+chkId).prop("checked") && n == "spotVal") { // if lucky pic then discard that row for validate
                                                    var list_of_values = $('input.spot_inputs').map(function() {
                                                                                                            return this.value;
                                                                                            })
                                                                                            .toArray();
                                                    // alert('input.'+n+val);
                                                    // alert(list_of_values);
                                                    //alert(list_of_values.toSource());
                                                    var duplicate = false;
                                                    var noInRange = true;
                                                    $.each(	list_of_values,	function(j,	x) {
                                                            
                                                            noInRange = parseInt(x) >= gdet.limit.min
                                                                            && parseInt(x) <= gdet.limit.max;
                                                            duplicate = list_of_values
                                                                            .indexOf(x) !== list_of_values
                                                                            .lastIndexOf(x);
                                                            
                                                            if(x != parseInt(x)){
                                                                nor = true;
                                                            }
                                                            
                                                            if (duplicate) {
                                                                    dup = true;
                                                            } else if (!noInRange) {
                                                                    nor = true;
                                                            }
                                                            ;
                                                    });
                            }

                    });
                    if (dup) {
                            alert("plz remove duplicate vales .");
                            return false;
                    } else if (nor) {
                            alert("values are not in range .");
                            return false;
                    }else{
                        return true;
                    }
            }else{
                return true;
            }
            
        },
        validateRow_old:function(chkId){
            var chkId=1;
            if($("#spot_option").val() == ""){
                    alert("Please select a spot .");return false;	
            }else  if($("#amt_option").val() == ""){
                    alert("Please select a amount to bet .");return false;
            }

            var id = $("#new_row_id").val();
            var row = "<tr id='row_" + id + "'>";//
            var gameGroups = Playwin.config.gameDetails.g9.ballGroups;
            // validate ticket data
            var dup = false;
            var nor = false;
            if (!$("#row_tcktprc_"+chkId).prop("checked")) {
                    $.each(
                                    gameGroups,
                                    function(n, gdet) {// iterate for each group
                                            if (!$("#lp_spotop_"+chkId).prop("checked") && n == "spotVal") { // if lucky pic then discard that row for validate
                                                    var list_of_values = $('input.spot_inputs').map(function() {
                                                                                                            return this.value;
                                                                                            })
                                                                                            .toArray();
                                                    // alert('input.'+n+val);
                                                    // alert(list_of_values);
                                                    //alert(list_of_values.toSource());
                                                    var duplicate = false;
                                                    var noInRange = true;
                                                    $.each(	list_of_values,	function(j,	x) {
                                                            
                                                            noInRange = parseInt(x) >= gdet.limit.min
                                                                            && parseInt(x) <= gdet.limit.max;
                                                            duplicate = list_of_values
                                                                            .indexOf(x) !== list_of_values
                                                                            .lastIndexOf(x);
                                                            
                                                            if(x != parseInt(x)){
                                                                nor = true;
                                                            }
                                                            
                                                            if (duplicate) {
                                                                    dup = true;
                                                            } else if (!noInRange) {
                                                                    nor = true;
                                                            }
                                                            ;
                                                    });
                            }

                    });
                    if (dup) {
                            alert("plz remove duplicate vales .");
                            return false;
                    } else if (nor) {
                            alert("values are not in range .");
                            return false;
                    }else{
                        return true;
                    }
            }else{
                return true;
            }
            
        },
        go:function(i){
                var b=$("#cardNo").val();
                var a=$("#pinNo").val();
                if(i!=9){
                    if($("#select-all-draw").val()==""&&$("#select-no-draw").val()==""){
                       alert("Please select a Draw OR no of draw !");
                        return false
                    }
                }
                var d=0;
            /*$(".ticket_price").each(function(){
                d=d+parseInt(this.value)//*10
                });
                $("#sum_amt").val(d);*/
            if(!b ||$.trim(b)==""){
                alert("Card Number can not be empty !","Error");
                return false
                }else{
                if(!a||$.trim(a)==""){
                    alert("Card Pin can not be empty .","Error");
                    return false
                    }else{
                    if($("#select-all-draw").val()==""&&$("#select-no-draw").val()==""){
                        alert("Please select 'Draw' OR 'No Of Draw' .","Error");
                        return false
                        }else{
                        if($("tbody#draws_table tr").length==0){
                            alert("Please add at least one ticket !","Ticket Error!");
                            return false
                            }
                        }
                }
            }
            $.ajax({
                url:Playwin.config.urls.checkBalance,
                method:"GET",
                data:"accountno="+$.trim(b)+"&password="+$.trim(a),
                success:function(h){
                    var f=$.xml2json(h);
                    if(f.Error||(f.Code&&(f.Code==-1||f.Code==99))){
                        alert(f.Error,"Invalid Card Error")
                        }else{
                        if(!f.RemainingAmount){
                            alert("Some error occurred when getting card balance. ","Card Read Error!")
                            }else{
                            if(f.RemainingAmount<parseInt($("#sum_amt").val())){
                                alert("Insufficient balance to place this bet.( Balance = Rs."+f.RemainingAmount+" ) ","Low Balance!")
                            }else{
                               var e=$("#sum_amt").val();
                                 
                               /*window.setTimeout(function(){         
                                    
                                    $("#betSubmitConfirmDialog").popup("open");                                            
                               }, 1000);*/
                                if(confirm("Your Card Balance is Rs. "+f.RemainingAmount+"."+" Rs. "+e+" will now be deducted from your card balance. Do you want to continue?")){
                                    //$("#betSubmitConfirmMsg").html("Your Card Balance is "+f.RemainingAmount+"."+" Rs. "+e+" will now be deducted from your card balance. Do you want to continue?")
                                    $( "#playCardSubmitPopUp" ).popup("close"); 
                                    Playwin.play.finalBetSubmit("balance");
                                }else{
                                    
                                }
                            }
            }
            }
            }
            }).fail(function(){
                alert("Error : Unable get card balance .")
                })
            },
            finalBetSubmit:function(){
                
                
                                var e=$("#sum_amt").val();
                                
                                
                               // var g=confirm(" Total Amount Rs. "+e+" will be deducted from your card balance. Do you want to continue ?"+"Your Card Balance is "+f.RemainingAmount+".");
                                //g.done(function(n){
                                //    if(!g){
                                //        return false
                                //        }else{
                                        var m="";
                                        var k="";
                                        var u=[];
                                        $(".ticket").each(function(){
                                            u.push(this.value)
                                            });
                                            var dataStr = "";
                                            var betStr = "";
                                            var betArr = [];
                                            /*$.each(Playwin.play.jsonDetails.tickets,
                                                    function(i, val) {
                                                            betArr.push(val.values.map(function(v) {
                                                            return v;
                                                    }));
                                            })
                                            k = betArr.join("|");*/
                                        k=u.join("|");
                                        
                                        /*
                                        BetString	0,0,0,0,0,0
                                        DrawPointer	1
                                        GameID          2
                                        ItzNumber	867012140114
                                        ItzPassword	1628
                                        TotalDraw	1
                                        mobileno	919967264985
                                        sale_amount	10
                                         */
                                        /*
                                        BetString	0,0,0,0,0,0
                                        DrawPointer	null
                                        GameID          2
                                        ItzNumber	867012140114
                                        ItzPassword	1628
                                        TotalDraw	1
                                        sale_amount	10
                                         */
                                        //ItzNumber=867012140114&ItzPassword=1628&BetString=0,0,0,0,0,0&GameID=2&DrawPointer=null&TotalDraw=1&sale_amount=10
                                        //$("#cardNo").val()
                                        m="ItzNumber="+$("#cardNo").val()+"&ItzPassword="+$("#pinNo").val()+"&BetString="+k+"&GameID="+Playwin.play.selectedGame;
                                        //if(Playwin.play.selectedGame!=9){
                                            var l,j;
                                           /* if($("#select-all-draw").val()!=""&&$("#draw_index_"+$("#select-all-draw").val()).val()==0){
                                                l=1;
                                                if($("#select-no-draw").val()==""){
                                                    j=1
                                                    }else{
                                                    j=$("#select-no-draw").val()
                                                    }
                                                }else{
                                            if($("#select-all-draw").val()!=""){
                                                l=$("#draw_index_"+$("#select-all-draw").val()).val();
                                                j=1
                                                }else{
                                                if($("#select-all-draw").val()==""){
                                                    l=1;
                                                    j=$("#select-no-draw").val()
                                                    }
                                                }
                                        }*/
                                                /*if($("#select-all-draw").val()!=""&&$("#select-all-draw").val()==0){
                                                    l=1;
                                                if($("#select-no-draw").val()==""){
                                                    j=1
                                                    }else{
                                                    j=$("#select-no-draw").val()
                                                    }
                                                }else{
                                                    if($("#select-all-draw").val()!=""){
                                                        l=$("#select-all-draw").val();
                                                        j=1
                                                        }else{
                                                        if($("#select-all-draw").val()==""){
                                                            l=1;
                                                            j=$("#select-no-draw").val()
                                                            }
                                                        }
                                                }*/
                                        
                                       
                                        l=$("#select-all-draw").val();
                                        j=$("#select-no-draw").val()
                                    m=m+"&DrawPointer="+l+"&TotalDraw="+j+"&sale_amount="+$("#sum_amt").val();
                                    //alert(m);return;
                                    //}
                                    $.ajax({
                                    url:Playwin.config.urls.betPlacement,
                                    method:"POST",
                                    data:m,
                                    success:function(q){
                                        
                                         //var res = "";
                                                 	//error without ticket
                                                    //var str = '<?xml version="1.0" encoding="UTF-8"?><Response><Status>-103</Status><StatusText>Myplawin Card Card is not valid.[Invalid Account Number or Password ]</StatusText></Response>' ;
                                                      
                            						//var str =  '<?xml version="1.0" encoding="UTF-8"?><Response><Status>30011</Status><StatusText>Bet can not placed.Please check your ITZ Card Statement for more details.</StatusText></Response>';
                            						 
                            						 
                                                     //multiple ticket success
                                                    //var str = "<Response><Status>0</Status><StatusText>SUCCESS</StatusText><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>590</DrawID><TSN>7E19-38F9-B039-D033</TSN><DrawDate>18/07/2013 22:00:00</DrawDate><SLSN>202338209</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>591</DrawID><TSN>5071-CAAA-9302-D033</TSN><DrawDate>25/07/2013 22:00:00</DrawDate><SLSN>202338210</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>592</DrawID><TSN>675A-5E6A-69ED-B033</TSN><DrawDate>01/08/2013 22:00:00</DrawDate><SLSN>202338211</SLSN><BetDate>07/16/2013 12:25</BetDate><GameName>Thu Super Lotto,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 01 02 03 04 05 06</BetLine></Panel><Promotion/></Ticket><CardBalance>14700.00</CardBalance></Response>"
                                         			
                                                     //one ticket success
                                                     //var str = "<?xml version='1.0' encoding='UTF-8'?><Response><Status>0</Status><StatusText>SUCCESS</StatusText><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>605</DrawID><TSN>2947-15CE-B743-A58D</TSN><DrawDate>04/03/2014 22:00:00</DrawDate><SLSN>202290065</SLSN><BetDate>03/03/2014 14:41</BetDate><GameName>Thunderball,,,,</GameName><Cost>10.00</Cost><Mrp>10.00</Mrp><Panel><LP>0</LP><BetLine>A: 04 05 06 07 08  TB 01 </BetLine></Panel><Promotion></Promotion></Ticket><CardBalance>260.00</CardBalance></Response>";
                                                  				
                                                     	
                                                 	//error one ticket
                                                    // var str = '<?xml version="1.0" encoding="UTF-8"?><Response><Status>0</Status><StatusText>SUCCESS</StatusText><Ticket><Status>35</Status><StatusText>|INVALIDPANELDATA</StatusText></Ticket></Response>';
                                                    // var str = '<?xml version="1.0" encoding="UTF-8"?><Response><Status>0</Status><StatusText>SUCCESS</StatusText><Ticket><Status>0</Status><StatusText>SUCCESS</StatusText><DrawID>58</DrawID><TSN>6828-4F21-DAEB-D971</TSN><DrawDate>12/03/2014 21:30:00</DrawDate><SLSN>200292786</SLSN><BetDate>03/11/2014 20:45</BetDate><GameName>Jaldi5-Double,Wednesday, ,,</GameName><Cost>20.00</Cost><Mrp>20.00</Mrp><Panel><LP>1</LP><BetLine>A: 02 07 16 28 30 </BetLine></Panel><Promotion></Promotion></Ticket><CardBalance>20.00</CardBalance></Response>';
                                                    // var xmlData = $.parseXML( str );// convert string to xml
                                         	    // var jsonObj = $.xml2json(xmlData);
                                 				//-----------------------------------XXX---------------------------------		
                                        var p = $.xml2json(q);//jsonObj;
                                        
                                        var s="";
                                        
                                        var gameDetails=eval("Playwin.config.gameDetails.g"+Playwin.play.selectedGame);
                                        var r={
                                            selectedGameId:Playwin.play.selectedGame,
                                            selectedGameName:gameDetails.name,
                                            errors:"",
                                            CardBalance:"",
                                            tickets:[]
                                        };

                                        var B=false;
                                        var o=0;
                                        if(p.Status!=0){
                                            s=s+" Error : "+p.StatusText;
                                            B=true
                                            }else{
                                            if(p.Status=="0"&&p.StatusText=="SUCCESS"){
                                                var t=false;
                                                r.CardBalance=p.CardBalance;
                                                //alert( r.CardBalance);
                                                // alert( p.toSource());
                                                if(p.Ticket instanceof Array){
                                                    $.each(p.Ticket,function(z,y){
                                                        var v={
                                                            err:false,
                                                            errMsg:"",
                                                            details:y
                                                        };

                                                        if(y.Status&&y.StatusText){
                                                            if(y.Status=="0"&&y.StatusText=="SUCCESS"){
                                                                o++;
                                                                v.err=false;
                                                                v.details=y
                                                                }else{
                                                                s=s+"Error : "+y.StatusText+"||";
                                                                v.err=true;
                                                                v.errMsg=y.StatusText
                                                                }
                                                            }else{
                                                        v.err=true;
                                                        v.errMsg=y.StatusText;
                                                        s=s+"Error : "+y.StatusText+"||"
                                                        }
                                                        r.tickets.push(v)
                                                        })
                                                }else{
                                                var w=p.Ticket;
                                                var x={
                                                    err:false,
                                                    errMsg:"",
                                                    details:w
                                                };

                                                if(w.Status&&w.StatusText){
                                                    if(w.Status=="0"&&w.StatusText=="SUCCESS"){
                                                        o++;
                                                        x.err=false
                                                        }else{
                                                        s=s+"Error : "+w.StatusText;
                                                        x.err=true;
                                                        x.errMsg=w.StatusText
                                                        }
                                                    }else{
                                                s=s+"Error : "+w.StatusText+"";
                                                x.err=true;
                                                x.errMsg=w.StatusText
                                                }
                                                r.tickets.push(x)
                                            }
                                        }
                                }
                               
                                Playwin.play.playGameResponse=r;
                            r=null;
                            if(o>0){
                                //window.localStorage.setItem("cardNo",$.trim(b));
                                //AppNamespace.app.navigate("playGameSuccess/"+Playwin.play.selectedGame)
                                    Playwin.play.playGameSuccessRender();
                                }else{
                                    alert(s)
                                }
                            },
                    complete:function(o){}
                })
            //}
            //})
                
            },
            calculateTotalBetAmount:function(){
                    var a=0;
                    $(".ticket_amt").each(function(){//ticket_price
                        a=a+parseInt(this.value)
                        });
                        //alert(a);
                        a=a*parseInt($("#select-no-draw").val()==""?1:$("#select-no-draw").val());
                    $("#sum_amt").val(a);
                    $("#sum_amt_text").html(a);
                    return a
            },
           kenoRowConfirm:function(){
               
                                            if($("#select-all-draw").val()==""&&$("#select-no-draw").val()==""){
                                                alert("Please select a Draw OR no of draw !");
                                                return false
                                            }
                                            
                                            if($("#draws_table tr").length == 10){
                                                alert("You can not add more than 10 panels !");
                                                return false
                                            }
                                           
                                            //alert(Playwin.play.selectedGame);
                                            if(Playwin.play.selectedGame == 9 ){
                                                if($("#amt_option").val()==""){
                                                    alert("Please select amount to bet !");
                                                    return false
                                                }  
                                                if($("#spot_option").val()==""){
                                                    alert("Please select spot !");
                                                    return false
                                                }  
                                            }                                           
                                            var values = $('.spot_inputs').map( //input
                                            function() {
                                                return parseInt($(this).val());
                                            }).toArray();                                          
                                            var checkId="#lp_spotop_1";
                                            if(!$(checkId).prop("checked")){
                                                
                                                //var valuesCheckArr = values.join(",");
                                                //var flagChk = valuesCheckArr=="" || valuesCheckArr.indexOf(",0,") != -1 || valuesCheckArr.indexOf(",0") != -1 || valuesCheckArr.indexOf("0,") != -1;
                                                        var flagChk = false;
                                                        $.each(values , function(i,v){
                                                            if(v == 0 || v == ""){
                                                                flagChk = true;
                                                            }
                                                        });
                                                        if(flagChk){
                                                           alert("Please select nos in Panel !");
                                                           return false; 
                                                        }
                                                
                                                var sorted_arr=values.slice(0);
                                                if(Playwin.play.selectedGame==2){
                                                    sorted_arr.pop()
                                                }
                                                sorted_arr=sorted_arr.sort();                
                                                var flag=false;
                                                for(var i=0;i<sorted_arr.length-1;i++){
                                                    if(sorted_arr[i+1]==sorted_arr[i]){
                                                        alert("Please select unique nos.in Panel");
                                                        flag=true;
                                                        break
                                                    }
                                                }
                                                if(flag){
                                                    return false
                                                }
                                            }else{
                                                for(var i=0;i<values.length;i++){}
                                            }
                                            
                                            
                                            
                                            var id=$("#new_row_id").val();
                                            var row="<tr id='row_"+id+"'>";
                                            var gid=Playwin.play.selectedGame;
                                            var gameGroups=eval("Playwin.config.gameDetails.g"+gid+".ballGroups");
                                            
                                            /*if($("#ticket_1").mobiscroll("getInst").getValue()==""){
                                                alert("Please set ticket values .");
                                                return false
                                            }*/
                                            //var values=$("#ticket_1").mobiscroll("getInst").getValue();
                                            var valuesArr = $('.spot_inputs').map( //input
                                            function() {
                                                return parseInt($(this).val());
                                            }).toArray();
                                            var values = valuesArr.join(",");
                                           
                                            /*if( values.split(',').join('').trim()=="" ){//check all empty values
                                                alert("Please set ticket values .");
                                                return false
                                            }*/
                                            //var checkId="";
                                            //checkId="#lp_spotop_1";//row_lb_1
                                            var ret = "";
                                            
                                           values = valuesArr;
                                            var vals=[];
                                            $("#pnlDetails").show();
                                            var gameDetails=eval("Playwin.config.gameDetails.g"+gid);//alert(gameDetails.toSource());
                                            var row="<tr id='row_"+id+"'>";
                                            row=row+"<td id='tckt_no_"+id+"' class='tckt_no'>"+($("#draws_table tr").length+1)+"</td>";
                                            if(gid==9){
                                                $.merge(vals,[$("#spot_option").val()]);
                                                $.merge(vals,valuesArr);
                                                $.merge(vals,[($("#amt_option").val()/10)]);
                                                row=row+"<td >"+$("#spot_option").val()+"</td>";//alert("1---"+row);
                                                row=row+"<td ><div id='ticket_div_"+id+"' class='ticket_div'>"+($.map(values,function(n){
                                                    return n=="0"?"LP":n
                                                }))+"<input id='ticket_"+id+"' class='ticket' type='hidden' value='"+vals+"'></div></td>";
                                                //alert("2---"+row);
                                                row=row+"<td ><input id='ticket_amt_"+id+"' class='ticket_amt' type='hidden' value='"+$("#amt_option").val()+"'>"+($("#amt_option").val())+"</td>"/* *10 */
                                            }else{
                                                row=row+"<td ><div id='ticket_div_"+id+"' class='ticket_div'>"+($.map(values,function(n){
                                                    return n=="0"?"LP":n
                                                }))+"<input id='ticket_"+id+"' class='ticket' type='hidden' value='";
                                                //alert("3---"+row);
                                                row=row+values+"'></div></td>";
                                                row=row+"<td ><input id='ticket_amt_"+id+"' class='ticket_amt' type='hidden' value='10'>10</td>";
                                                //row=row+"<td ></td>"
                                            }
                                            //alert("4--"+row);
                                            row=row+"<td style='' width='45px' id='row_adrm_"+id+"'><input type='hidden' id='row_id_"+id+"' name='row_id[]' value='"+id+"'/>";
                                            row=row+"<input type='hidden' id='ticket_price_"+id+"' name='ticket_price[]' class='ticket_price' value='"+gameDetails.ticketPrice+"'/>";
                                            row=row+"<div onClick='Playwin.play.removeTicket("+id+")' style='cursor:pointer'  id='row_rm_"+id+"' class='row_remove' title='Remove Draw'> <img src='../content/images/trash.png' class='deleteIcon'/> </div></td></tr>";
                                            
                                            $("#draws_table").append(row);
                                            var sum=Playwin.play.calculateTotalBetAmount();
                                            id=1+parseInt(id);
                                            $("#new_row_id").val(id);
                                            $("#ballTitle").hide();
                                            $("#addTicket").show();
                                            
                                            $("#lotto_table_div").hide();
                                            $("#show_lotto_table_div").show();
                                            
                                            
            },
            gameRowConfirm:function(noofrow){
               
                                            if($("#select-all-draw").val()==""&&$("#select-no-draw").val()==""){
                                                alert("Please select a Draw OR no of draw !");
                                                return false
                                            }
                                            
                                            if($("#draws_table tr").length == 10){
                                                alert("You can not add more than 10 panels !");
                                                return false
                                            }
                                           
//                                            //alert(Playwin.play.selectedGame);
//                                            if(Playwin.play.selectedGame == 9 ){
//                                                if($("#amt_option").val()==""){
//                                                    alert("Please select amount to bet !");
//                                                    return false
//                                                }  
//                                                if($("#spot_option").val()==""){
//                                                    alert("Please select spot !");
//                                                    return false
//                                                }  
//                                            }      
                                        
                                            var atLeastOneTicket = 0;
                                            var html = "";
                                            for(var indx=1;indx<=noofrow;indx++){                                              
                                                    
                                                    var values = $('.spotop'+indx).map( //input
                                                    function() {
                                                        return parseInt($(this).val());
                                                    }).toArray();   
                                                    
                                                    
                                                    var checkId="#lp_spotop_"+indx;
                                                    if(!$(checkId).prop("checked")){
                                                        
                                                        var valuesCheckArr = values.join(",");
                                                        
                                                        if(valuesCheckArr.replace(/0/g,"").replace(/,/g,"") == ""){
                                                            continue;
                                                        }else{
                                                            atLeastOneTicket=atLeastOneTicket+1;
                                                        }
                                                        
                                                        var flagChk = false;
                                                        $.each(values , function(i,v){
                                                            if(v == 0 || v == ""){
                                                                flagChk = true;
                                                            }
                                                        });
                                                        var pno = "";
                                                        if(indx==1)pno="A";
                                                        else if(indx==2)pno="B";
                                                        else if(indx==3)pno="C";
                                                        else if(indx==4)pno="D";
                                                        else if(indx==5)pno="E";
                                                        else if(indx==6)pno="F";
                                                        else if(indx==7)pno="G";
                                                        else if(indx==8)pno="H";
                                                        else if(indx==9)pno="I";
                                                        else if(indx==10)pno="J";
                                                        if(flagChk){
                                                           alert("Please select nos in Panel "+pno+"  !");
                                                           return false; 
                                                        } 

                                                        var sorted_arr=values.slice(0);
                                                        if(Playwin.play.selectedGame==2){
                                                            sorted_arr.pop()
                                                        }
                                                        sorted_arr=sorted_arr.sort();                
                                                        var flag=false;
                                                        for(var i=0;i<sorted_arr.length-1;i++){
                                                            if(sorted_arr[i+1]==sorted_arr[i]){
                                                                alert(" Please select unique nos. in Panel "+pno+"  !");
                                                                flag=true;
                                                                break
                                                            }
                                                        }
                                                        if(flag){
                                                            return false
                                                        }
                                                    }else{
                                                        atLeastOneTicket = 1;
                                                        for(var i=0;i<values.length;i++){}
                                                    }



                                                    var id=$("#new_row_id").val();
                                                    var row="<tr id='row_"+id+"'>";
                                                    var gid=Playwin.play.selectedGame;
                                                    var gameGroups=eval("Playwin.config.gameDetails.g"+gid+".ballGroups");

                                                    /*if($("#ticket_1").mobiscroll("getInst").getValue()==""){
                                                        alert("Please set ticket values .");
                                                        return false
                                                    }*/
                                                    //var values=$("#ticket_1").mobiscroll("getInst").getValue();
                                                    var valuesArr = $('.spotop'+indx).map( //input
                                                    function() {
                                                        return parseInt($(this).val());
                                                    }).toArray();
                                                    var values = valuesArr.join(",");

                                                    /*if( values.split(',').join('').trim()=="" ){//check all empty values
                                                        alert("Please set ticket values .");
                                                        return false
                                                    }*/
                                                    //var checkId="";
                                                    //checkId="#lp_spotop_1";//row_lb_1
                                                    var ret = "";

                                                   values = valuesArr;
                                                    var vals=[];
                                                    
                                                    var gameDetails=eval("Playwin.config.gameDetails.g"+gid);//alert(gameDetails.toSource());
                                                    var row="<tr id='row_"+id+"'>";
                                                    row=row+"<td id='tckt_no_"+id+"' class='tckt_no'>"+($("#draws_table tr").length+1)+"</td>";
                                                    if(gid==9){
                                                        $.merge(vals,[$("#spot_option").val()]);
                                                        $.merge(vals,valuesArr);
                                                        $.merge(vals,[($("#amt_option").val()/10)]);
                                                        row=row+"<td >"+$("#spot_option").val()+"</td>";//alert("1---"+row);
                                                        row=row+"<td ><div id='ticket_div_"+id+"' class='ticket_div'>"+($.map(values,function(n){
                                                            return n=="0"?"LP":n
                                                        }))+"<input id='ticket_"+id+"' class='ticket' type='hidden' value='"+vals+"'></div></td>";
                                                        //alert("2---"+row);
                                                        row=row+"<td ><input id='ticket_amt_"+id+"' class='ticket_amt' type='hidden' value='"+$("#amt_option").val()+"'>"+($("#amt_option").val())+"</td>"/* *10 */
                                                    }else{
                                                        row=row+"<td ><div id='ticket_div_"+id+"' class='ticket_div'>"+($.map(values,function(n){
                                                            return n=="0"?"LP":n
                                                        }))+"<input id='ticket_"+id+"' class='ticket' type='hidden' value='";
                                                        //alert("3---"+row);
                                                        row=row+values+"'></div></td>";
                                                        if(gid==11){
                                                            row=row+"<td ><input id='ticket_amt_"+id+"' class='ticket_amt' type='hidden' value='20'>20</td>";
                                                        }else{
                                                            row=row+"<td ><input id='ticket_amt_"+id+"' class='ticket_amt' type='hidden' value='10'>10</td>";
                                                        }
                                                        
                                                        //row=row+"<td ></td>"
                                                    }
                                                    //alert("4--"+row);
                                                    row=row+"<td style='' width='45px' id='row_adrm_"+id+"'><input type='hidden' id='row_id_"+id+"' name='row_id[]' value='"+id+"'/>";
                                                    row=row+"<input type='hidden' id='ticket_price_"+id+"' name='ticket_price[]' class='ticket_price' value='"+gameDetails.ticketPrice+"'/>";
                                                    row=row+"<div onClick='Playwin.play.removeTicket("+id+")' style='cursor:pointer'  id='row_rm_"+id+"' class='row_remove' title='Remove Draw'> <img src='../content/images/trash.png' class='deleteIcon'/> </div></td></tr>";
                                                    
                                                html = html + row;
                                            }
                                            $("#draws_table").html(html);
                                            if(atLeastOneTicket == 0){
                                                alert("ERROR : Please set at least one ticket values. ");
                                                return false
                                            }
                                            
                                            
                                            
                                            var sum=Playwin.play.calculateTotalBetAmount();
                                            id=1+parseInt(id);
                                            $("#new_row_id").val(id);
                                            $("#pnlDetails").show();
                                            $("#ballTitle").hide();
                                            $("#addTicket").show();
                                            $("#gamePanels").hide();
                                            Playwin.play.initCardSubmit("play");
                                            
                                            
            },
            removeTicket:function(b){
            $("#row_"+b).remove();
            /*$.each($(".tckt_no"),function(d,e){
                $("#"+this.id).html(d+1)
                });
            var a=0;
            $(".ticket_amt").each(function(){
                a=a+parseInt(this.value)*10
                });
            $("#sum_amt").val(a);
            $("#sum_amt_text").html(a);*/
            var sum=Playwin.play.calculateTotalBetAmount();
            var id=$("#new_row_id").val();
            id = parseInt(id) - 1;
            $("#new_row_id").val(id);
            //$("#ballTitle").hide();
            //$("#addTicket").show();
            return false
            },
           
     showOverlayBalance:function(){
            
            var no=$("#cardNo").val();
                var pass=$("#pinNo").val();
                if(!no||$.trim(no)==""){
                    alert("Card Number can not be empty !");
                    return false
                }else{
                    if(!pass||$.trim(pass)==""){
                        alert("Card Pin can not be empty .");
                        return false
                    }
                }  
            $( "#playCardSubmitPopUp" ).popup("close"); 
            $( "#loadPanelMsg" ).popup("open");  
            
            $.ajax({
                url:Playwin.config.urls.checkBalance,
                method:"GET",
                data:"accountno="+$.trim(no)+"&password="+$.trim(pass),
                success:function(xmlData){
                    var jsonObj=$.xml2json(xmlData);
                    $( "#loadPanelMsg" ).popup("close");  
                    
                    if(jsonObj.Error||(jsonObj.Code&&(jsonObj.Code==-1||jsonObj.Code==99))){
                        alert("Invalid Card Error : "+jsonObj.Error)
                    }else{
                        if(!jsonObj.RemainingAmount){
                            alert("Some error occurred when getting card balance. ")
                        }else{
                            
                           $("#card_balance").html(jsonObj.RemainingAmount);
                           $("#card_expiry").html(jsonObj.ExpiryDate);
                           $("#card_status").html(jsonObj.Status);
                           $( "#showBalance" ).popup("open");  
                            
                        }
                    }
                }
            }).fail(function(){
                $( "#loadPanelMsg" ).popup("close"); 
                alert("Error : Unable get card info.")
            })
        },
        
        loadLottoBox: function(val,noofrow){
            
								
                    // spot_input_boxes = spot_input_boxes +
                    // "<td><input type='hidden' value=''
                    // name='spot_inputs[]' id='spot_input_"+1+"'
                    // class='lottotbox spot_inputs'/></td>";
                    /* var gameGroups = eval("Playwin.config.gameDetails.g"+Playwin.play.selectedGame+".ballGroups");
                     var n = 1;
                     $.each(gameGroups,function(grp, gdet) { 
                         $.each(gdet.balls, function(m, bd) {
                            spot_input_boxes = spot_input_boxes
                                                    + "<td><input value='' name='spot_inputs[]' id='spot_input_"
                                                    + (n + 1)
                                                    + "' class='lottotbox "+grp+" spotop1 spot_inputs'/></td>";
                                                n++;
                         });
                     });



                    // spot_input_boxes = spot_input_boxes +
                    // "<td><input value='' name='spot_inputs[]'
                    // id='spot_input_"+(val+2)+"' class='lottotbox
                    // spot_inputs'/></td>";

                    spot_input_boxes = spot_input_boxes
                                    + "<td style='' width='45px'>"
                                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' id='lp_row_1' class='row_lp' type='checkbox'>"
                                    + "</td>"
                                    + "<td style='' width='45px' id='row_adrm_1'>"
                                    + "<a href='#' id='keno_row_confirm' class='row_add btn' title='Confirm'> <strong> Confirm <strong> </a>"
                                    + "</td>";
                    spot_input_boxes = spot_input_boxes
                                    + "<td style='' width='45px'>"
                                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id='lp_spotop_1' class='row_lp lprow' type='checkbox'>"
                                    + "</td>";*/
                    var n = 1 , grp = "";
                    var gameWheels = Playwin.play.getGameWheels(Playwin.play.selectedGame,false,"all");
                    var wholeHtml = "";
                    for(var indx=1;indx<=noofrow;indx++){
                        var pno = "";
                        if(indx==1)pno="A";
                        else if(indx==2)pno="B";
                        else if(indx==3)pno="C";
                        else if(indx==4)pno="D";
                        else if(indx==5)pno="E";
                        else if(indx==6)pno="F";
                        else if(indx==7)pno="G";
                        else if(indx==8)pno="H";
                        else if(indx==9)pno="I";
                        else if(indx==10)pno="J";
                        
                       var spot_input_boxes = "<div style='float:left;width:20px;height:34px;padding:7px 0 0 7px'>"+pno+" &nbsp;</div>";
                       
                       $.each(gameWheels,function(iw, vw) {                      
                        
                            spot_input_boxes = spot_input_boxes+"<div class='lotto_select'><select name='spot_inputs"+indx+"[]' id='spot_input"+indx+"_"
                                                        + (n + 1)
                                                        + "' class='lottotbox "+vw[0].label+" spotop"+indx+" spot_inputs'>"
                            var dataValue =vw[0]['values'];
                            $.each(vw[0]['keys'],function(ki, kd) {
                                spot_input_boxes = spot_input_boxes+"<option value='"+kd+"'>"+dataValue[ki]+"</option>"
                            });

                            spot_input_boxes = spot_input_boxes+"</select></div>"
                            n++;
                        });                    
                        spot_input_boxes = spot_input_boxes+"<div class='lotto_lp'><input type='hidden' id='lp_row_"+indx+"' class='row_lp'><input id='lp_spotop_"+indx+"' class='row_lp lprow regular-checkbox big-checkbox' type='checkbox'><label for='lp_spotop_"+indx+"' style='display:inline;'>LP</label></div>"; 
                        wholeHtml = wholeHtml + spot_input_boxes;
                        
                         
                        $("#tr_spot_inputs_"+indx).html(spot_input_boxes);
                       
                    }
                     n = n-1;

                    // spot_input_boxes = spot_input_boxes +
                    // "<td><input value='' name='spot_inputs[]'
                    // id='spot_input_"+(val+2)+"' class='lottotbox
                    // spot_inputs'/></td>";




                    //alert(spot_input_boxes);
                    
                   //var spot_confirm_boxes = "<td style='' colspan='"+n+"' id='row_adrm_1' align='right'>"
                   var spot_confirm_boxes = "";
                   if( Playwin.play.selectedGame == 9){
                       spot_confirm_boxes = "<div class='lotto_confirm' style='' colspan='"+n+"' id='row_adrm_1' align='right'>"
                        + "<div class='dx-button2' id='keno_row_confirm'><span class='dx-button-text'>Confirm</span></div>"
                                    + ""
                                    //+ "</td>";
                                    + "</div>";
                   }else{
                      // spot_confirm_boxes = "<div class='lotto_confirm' style='' colspan='"+n+"' id='row_adrm_1' align='right'>"
                      //  + "<div class='dx-button2' id='game_row_confirm'><span class='dx-button-text'>Confirm</span></div>"
                      //              + ""
                      //              //+ "</td>";
                      //              + "</div>";
                   }
                    

                    $("#tr_confirm_inputs").html(spot_confirm_boxes);

                    return false;
        },
        resetLottoBox: function(val,indx){
            
								
                    // spot_input_boxes = spot_input_boxes +
                    // "<td><input type='hidden' value=''
                    // name='spot_inputs[]' id='spot_input_"+1+"'
                    // class='lottotbox spot_inputs'/></td>";
                    /* var gameGroups = eval("Playwin.config.gameDetails.g"+Playwin.play.selectedGame+".ballGroups");
                     var n = 1;
                     $.each(gameGroups,function(grp, gdet) { 
                         $.each(gdet.balls, function(m, bd) {
                            spot_input_boxes = spot_input_boxes
                                                    + "<td><input value='' name='spot_inputs[]' id='spot_input_"
                                                    + (n + 1)
                                                    + "' class='lottotbox "+grp+" spotop1 spot_inputs'/></td>";
                                                n++;
                         });
                     });



                    // spot_input_boxes = spot_input_boxes +
                    // "<td><input value='' name='spot_inputs[]'
                    // id='spot_input_"+(val+2)+"' class='lottotbox
                    // spot_inputs'/></td>";

                    spot_input_boxes = spot_input_boxes
                                    + "<td style='' width='45px'>"
                                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' id='lp_row_1' class='row_lp' type='checkbox'>"
                                    + "</td>"
                                    + "<td style='' width='45px' id='row_adrm_1'>"
                                    + "<a href='#' id='keno_row_confirm' class='row_add btn' title='Confirm'> <strong> Confirm <strong> </a>"
                                    + "</td>";
                    spot_input_boxes = spot_input_boxes
                                    + "<td style='' width='45px'>"
                                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id='lp_spotop_1' class='row_lp lprow' type='checkbox'>"
                                    + "</td>";*/
                    var n = 1 , grp = "";
                    var gameWheels = Playwin.play.getGameWheels(Playwin.play.selectedGame,false,"all");
                    var wholeHtml = "";
                    //for(var indx=1;indx<=noofrow;indx++){
                       var pno
                       if(indx==1)pno="A";
                        else if(indx==2)pno="B";
                        else if(indx==3)pno="C";
                        else if(indx==4)pno="D";
                        else if(indx==5)pno="E";
                        else if(indx==6)pno="F";
                        else if(indx==7)pno="G";
                        else if(indx==8)pno="H";
                        else if(indx==9)pno="I";
                        else if(indx==10)pno="J";
                        
                       var spot_input_boxes = "<div style='float:left;width:20px;height:34px;padding:7px 0 0 7px'>"+pno+" &nbsp;</div>";
                       
                       
                       $.each(gameWheels,function(iw, vw) {                      
                        
                            spot_input_boxes = spot_input_boxes+"<div class='lotto_select'><select name='spot_inputs"+indx+"[]' id='spot_input"+indx+"_"
                                                        + (n + 1)
                                                        + "' class='lottotbox "+vw[0].label+" spotop"+indx+" spot_inputs'>"
                            var dataValue =vw[0]['values'];
                            $.each(vw[0]['keys'],function(ki, kd) {
                                spot_input_boxes = spot_input_boxes+"<option value='"+kd+"'>"+dataValue[ki]+"</option>"
                            });

                            spot_input_boxes = spot_input_boxes+"</select></div>"
                            n++;
                        });                    
                        spot_input_boxes = spot_input_boxes+"<div class='lotto_lp'><input type='hidden' id='lp_row_"+indx+"' class='row_lp'><input id='lp_spotop_"+indx+"' class='row_lp lprow regular-checkbox big-checkbox' type='checkbox'><label for='lp_spotop_"+indx+"' style='display:inline;'>LP</label></div>"; 
                        wholeHtml = wholeHtml + spot_input_boxes;
                        $("#tr_spot_inputs_"+indx).html(spot_input_boxes);
                       
                    //}
                     
                    return false;
        },
//        loadLottoBox: function(val,noofrow){
//            
//								
//                    // spot_input_boxes = spot_input_boxes +
//                    // "<td><input type='hidden' value=''
//                    // name='spot_inputs[]' id='spot_input_"+1+"'
//                    // class='lottotbox spot_inputs'/></td>";
//                    /* var gameGroups = eval("Playwin.config.gameDetails.g"+Playwin.play.selectedGame+".ballGroups");
//                     var n = 1;
//                     $.each(gameGroups,function(grp, gdet) { 
//                         $.each(gdet.balls, function(m, bd) {
//                            spot_input_boxes = spot_input_boxes
//                                                    + "<td><input value='' name='spot_inputs[]' id='spot_input_"
//                                                    + (n + 1)
//                                                    + "' class='lottotbox "+grp+" spotop1 spot_inputs'/></td>";
//                                                n++;
//                         });
//                     });
//
//
//
//                    // spot_input_boxes = spot_input_boxes +
//                    // "<td><input value='' name='spot_inputs[]'
//                    // id='spot_input_"+(val+2)+"' class='lottotbox
//                    // spot_inputs'/></td>";
//
//                    spot_input_boxes = spot_input_boxes
//                                    + "<td style='' width='45px'>"
//                                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' id='lp_row_1' class='row_lp' type='checkbox'>"
//                                    + "</td>"
//                                    + "<td style='' width='45px' id='row_adrm_1'>"
//                                    + "<a href='#' id='keno_row_confirm' class='row_add btn' title='Confirm'> <strong> Confirm <strong> </a>"
//                                    + "</td>";
//                    spot_input_boxes = spot_input_boxes
//                                    + "<td style='' width='45px'>"
//                                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id='lp_spotop_1' class='row_lp lprow' type='checkbox'>"
//                                    + "</td>";*/
//                    var n = 1 , grp = "";
//                    var gameWheels = Playwin.play.getGameWheels(Playwin.play.selectedGame,false,"all");
//                    var wholeHtml = "";
//                    for(var indx=1;indx<=noofrow;indx++){
//                       var spot_input_boxes = "";
//                       $.each(gameWheels,function(iw, vw) {                      
//                        
//                            spot_input_boxes = spot_input_boxes+"<div class='lotto_select'><select name='spot_inputs"+indx+"[]' id='spot_input"+indx+"_"
//                                                        + (n + 1)
//                                                        + "' class='lottotbox "+vw[0].label+" spotop spotrow"+indx+" spot_inputs'>"
//                            var dataValue =vw[0]['values'];
//                            $.each(vw[0]['keys'],function(ki, kd) {
//                                spot_input_boxes = spot_input_boxes+"<option value='"+kd+"'>"+dataValue[ki]+"</option>"
//                            });
//
//                            spot_input_boxes = spot_input_boxes+"</select></div>"
//                            n++;
//                        });
//                        spot_input_boxes = "";
//                        spot_input_boxes = spot_input_boxes+"<div class='lotto_lp'><input type='hidden' id='lp_row_"+indx+"' class='row_lp'><input id='lp_spotop_"+indx+"' class='row_lp lprow regular-checkbox big-checkbox' type='checkbox'><label for='lp_spotop_"+indx+"' style='display:inline;'>LP</label></div>"; 
//                        wholeHtml = wholeHtml + spot_input_boxes;
//                        $("#tr_spot_inputs_"+indx).html(spot_input_boxes);
//                       
//                    }
//                     n = n-1;
//
//                    // spot_input_boxes = spot_input_boxes +
//                    // "<td><input value='' name='spot_inputs[]'
//                    // id='spot_input_"+(val+2)+"' class='lottotbox
//                    // spot_inputs'/></td>";
//
//
//
//
//                    //alert(spot_input_boxes);
//                    
//                   //var spot_confirm_boxes = "<td style='' colspan='"+n+"' id='row_adrm_1' align='right'>"
//                    var spot_confirm_boxes = "<div class='lotto_confirm' style='' colspan='"+n+"' id='row_adrm_1' align='right'>"
//                        + "<div class='dx-button2' id='keno_row_confirm'><span class='dx-button-text'>Confirm</span></div>"
//                                    + ""
//                                    //+ "</td>";
//                                    + "</div>";
//
//                    $("#tr_confirm_inputs").html(spot_confirm_boxes);
//
//                    return false;
//        },
//        resetLottoBox: function(rowno){
//            
//                    var n = 1 , grp = "";
//                    var gameWheels = Playwin.play.getGameWheels(Playwin.play.selectedGame,false,"all");
//                    var wholeHtml = "";
//                    //for(var indx=1;indx<=noofrow;indx++){
//                       var spot_input_boxes = "";
//                       $.each(gameWheels,function(iw, vw) {                      
//                        
//                            spot_input_boxes = spot_input_boxes+"<div class='lotto_select'><select name='spot_inputs"+rowno+"[]' id='spot_input"+rowno+"_"
//                                                        + (n + 1)
//                                                        + "' class='lottotbox "+vw[0].label+" spotop"+rowno+" spot_inputs'>"
//                            var dataValue =vw[0]['values'];
//                            $.each(vw[0]['keys'],function(ki, kd) {
//                                spot_input_boxes = spot_input_boxes+"<option value='"+kd+"'>"+dataValue[ki]+"</option>"
//                            });
//
//                            spot_input_boxes = spot_input_boxes+"</select></div>"
//                            n++;
//                        });                    
//                        //spot_input_boxes = spot_input_boxes+"<div class='lotto_lp'><input type='hidden' id='lp_row_"+rowno+"' class='row_lp'><input id='lp_spotop_"+rowno+"' class='row_lp lprow regular-checkbox big-checkbox' type='checkbox'><label for='lp_spotop_"+rowno+"' style='display:inline;'>LP</label></div>"; 
//                        wholeHtml = wholeHtml + spot_input_boxes;
//                        $("#tr_spot_inputs_"+rowno).html(spot_input_boxes);
//                       
//                    //}
//                     n = n-1;
//                     var spot_confirm_boxes = "<div class='lotto_confirm' style='' colspan='"+n+"' id='row_adrm_1' align='right'>"
//                        + "<div class='dx-button2' id='keno_row_confirm'><span class='dx-button-text'>Confirm</span></div>"
//                                    + ""
//                                    + "</div>";
//
//                    $("#tr_confirm_inputs").html(spot_confirm_boxes);
//
//                    return false;
//        },
       spotChange: function(val){
            
								var spotBalls = {};
								for (i = 1; i <= val; i++) {
									spotBalls["b" + (i + 1)] = i + 1;
								}

								Playwin.config.gameDetails.g9.ballGroups.spotVal.balls = spotBalls;

								var noBetBalls = {};
								noBetBalls["b" + (i + 1)] = i;
								Playwin.config.gameDetails.g9.ballGroups.noOfBet.balls = noBetBalls;
								// alert(Playwin.config.gameDetails.g9.ballGroups.spotVal.balls.toSource());
								var spot_input_boxes = "";
								// spot_input_boxes = spot_input_boxes +
								// "<td><input type='hidden' value=''
								// name='spot_inputs[]' id='spot_input_"+1+"'
								// class='lottotbox spot_inputs'/></td>";
								/*for (i = 1; i <= val; i++) {
									spot_input_boxes = spot_input_boxes
											+ "<td><input value='' name='spot_inputs[]' id='spot_input_"	+ (i + 1)
											+ "' class='lottotbox spotop1 spot_inputs'/></td>";
								}*/
           
                                                                
                                                                
                                                                //alert(Playwin.play.selectedGame);
                                                                var isd = Playwin.play.getGameBalls(Playwin.play.selectedGame,false);
                                                                //alert(isd.toSource());
                                                                Playwin.play.loadLottoBox(isd.length,1);
                                                                
								// spot_input_boxes = spot_input_boxes +
								// "<td><input value='' name='spot_inputs[]'
								// id='spot_input_"+(val+2)+"' class='lottotbox
								// spot_inputs'/></td>";

								/*spot_input_boxes = spot_input_boxes
										+ "<td style='' width='45px'>"
										+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='hidden' id='lp_row_1' class='row_lp' type='checkbox'>"
										+ "</td>"
										+ "<td style='' width='45px' id='row_adrm_1'>"
										+ "<a href='#' id='keno_row_confirm' class='row_add btn' title='Confirm'> <strong> Confirm <strong> </a>"
										+ "</td>";
								spot_input_boxes = spot_input_boxes
										+ "<td style='' width='45px'>"
										+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id='lp_spotop_1' class='row_lp lprow' type='checkbox'>"
										+ "</td>";
								$("#tr_spot_inputs").html(spot_input_boxes);*/
								return false;
        }
       /* onSelect:function(f,d){
            var u=d.getValue();
            var g=u.slice(0);
            if(Playwin.play.selectedGame==2){
                g.pop()
            }
            g=g.sort();
            var j=false,a=[];
            for(var b=0;b<g.length-1;b++){
                if(g[b]!=0&&g[b+1]==g[b]){
                    j=true;
                    break
                }
            }
            if(j){
                var h=DevExpress.ui.dialog.alert("Please select UNIQUE Ticket Values .","Error!");
                h.done(function(i){
                    d.show();
                    return false
                })
            }else{
                var e="";
                var k=[];
                $.each(u,function(l,i){
                    k.push(i==0?"LP":i)
                });
                viewPlayGameModel.gameBalls(k)
            }
    }*/
     ,
    getGameBalls:function(gid,isLP)
    {
                                    var gameDetails=eval("Playwin.config.gameDetails.g"+gid);
                                    var gameGroups=gameDetails.ballGroups;
                                    var whls=[];
                                    $.each(gameGroups,function(n,gd){
                                        if(gd.show_in_balls=="yes"){
                                            $.each(gd.balls,function(m,bd){
                                                var wi=[];
                                                wi[0]={
                                                    label:"",
                                                    keys:[],
                                                    values:[]
                                                };

                                                /*if(isLP){
                                                        wi[0].keys.push("0");
                                                        wi[0].values.push("LP")
                                                    }else{
                                                        for(var i=gd.limit.min;i<=gd.limit.max;i++){
                                                            var x="";
                                                            x=i<10?"0"+i:i+"";
                                                            wi[0].keys.push(x);
                                                            wi[0].values.push(x)
                                                        }
                                                    }*/
                                                whls.push(wi)
                                            })
                                        }
                                    });
                                    return whls
                                },
     setOverLayResults:function(a,g){
     
    var overLayResults = [];
    //var b=new $.Deferred();
    $( "#resultLoading" ).popup( "open" );
    
    $.get(Playwin.config.urls.results+"?gameid="+a.id+"&count="+ $( "#resultGameCount" ).val()).done(function(d){
        $( "#resultLoading" ).popup( "close" );
        var e=$.xml2json(d);
        d=null;
        var j=(!e.GameResult)||(e.GameResult.length==0)?[]:e.GameResult;
        if(j.Game){
            j=[j]
            }
            var f=$.map(j,function(k){
            var h=$.trim(k.Result);
            var i=h.split(" ");
            var o="";
            $.each(i,function(l,m){
                if($.trim(m)!=""){
                    o=o+"<div "+(m!="TB"?"class='resultBall'>":"style='float:left;padding-top:5px;'>")+m+"</div>"
                    }
                });
        return{
            Game:k.Game,
            Result:k.Result,
            DrawID:k.DrawID,
            ResultHtml:o,
            DrawDate:Playwin.core.getFormatedDateTime(k.DrawDate,"dddd, dd-MMM-yyyy hh:mm TT"),
            NextDrawDate:Playwin.core.getFormatedDateTime(k.NextDrawDate,"dddd, dd-MMM-yyyy hh:mm TT"),
            NextFirstPrize:k.NextFirstPrize
            }
        });
    b.resolve(f);
    g.overLayResults(f);
    g.resultTitle(a.name);
    g.overlayVisible(true)
    }).fail(function(){
    g.loadPanelVisible(false);
    g.loadPanelMsg("Loading...");
    DevExpress.ui.dialog.alert("Error : Unable get results info.","Error!")
    })
},
getGameWheels:function(gid,lp,grpName){
    var gameDetails=eval("Playwin.config.gameDetails.g"+gid);
    //alert(gameDetails);
    var gameGroups=gameDetails.ballGroups;
    var whls=[];
    $.each(gameGroups,function(n,gd){
        if((grpName!="all"&&grpName!=n)){
            return
        }        
        if(gd.show_in_balls=="yes"){
            $.each(gd.balls,function(m,bd){
                var wi=[];
                wi[0]={
                    label:n,
                    keys:[],
                    values:[]
                };
                
                if(lp){
                        wi[0].keys.push("0");
                        wi[0].values.push("LP")
                }else{
                    wi[0].keys.push("0");
                    if(n=="grptb"){
                        wi[0].values.push("TB");
                    }else{
                        wi[0].values.push("");
                    }
                    
                    for(var i=gd.limit.min;i<=gd.limit.max;i++){
                        var x="";
                        x=i<10?"0"+i:i+"";
                        wi[0].keys.push(x);
                        wi[0].values.push(x)
                        }
                    }
                    whls.push(wi)
                })
        }
    });
return whls
}

}
