
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" >
                <title>Results</title>
		
<link rel="stylesheet" href="../themes/plwin.min.css" />
<link rel="stylesheet" href="../css/common.css" />
<link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
    <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />

                 <link rel="stylesheet" href="../css/GameList.css" />
                 
		<!--		<script src="http://code.jquery.com/jquery-1.10.2.min.js" ></script>-->
                <script src="../js/jquery.js" ></script>
<!--		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>-->
                <script src="../js/jquery.mobile-1.4.0.min.js"></script>
                
                <script src="../js/xml2json.js?1"></script>
                
                <script src="../js/fullcalendar.min.js"></script>
                
                <script src="../js/playwin.core.js" ></script>
                <script src="../js/playwin.config.js" ></script>
                <script src="../js/blinktext.js"></script>

	</head>
	<body>
            <style type="text/css">
            #resultListPopUp-popup {
                width: 90%;
                /*height: 100%;*/
                left:5%;
                right:5%;
            }
                
            h1 {color:red;}
            .dx-theme-ios .dx-list .dx-list-item, .dx-theme-ios .dx-list .dx-empty-message {
                border-bottom: 1px solid #E0E0E0;
                font-size: 13pt;
                font-weight: 200;
                line-height: 1.3636;
                overflow: hidden;
                padding: 13px 10px;
                position: relative;
                text-overflow: ellipsis;
                white-space: nowrap;
            }
                
                
                .dx-button {
	display:inline-block;
	margin:1px 0;
	cursor:pointer;
	text-align:center;
	vertical-align:middle;
	max-width:100%;
	-webkit-user-select:none;
	-moz-user-select:none;
	-ms-user-select:none;
	-o-user-select:none;
	user-select:none;
	-webkit-user-drag:none;
	-moz-user-drag:none;
	-ms-user-drag:none;
	-o-user-drag:none;
	user-drag:none
}
.dx-button1 {
	background-color:#efe95f;
	color:#385487;
	border:0;
	-webkit-box-shadow:2px 2px 2px 0 #4e87b1;
	-moz-box-shadow:2px 2px 2px 0 #4e87b1;
	-ms-box-shadow:2px 2px 2px 0 #4e87b1;
	-o-box-shadow:2px 2px 2px 0 #4e87b1;
	box-shadow:2px 2px 2px 0 #4e87b1;
	padding:6px;
	background:#efe95f url(/content/images/arrow.png) no-repeat 95% 9px
}
.blue .dx-button2, .yellow .dx-button2 {
	cursor:pointer;
	background-color:#226494;
	color:#fff;
	border:0;
	padding:2px;
	padding-top:0;
	-webkit-border-radius:2px;
	-moz-border-radius:2px;
	-ms-border-radius:2px;
	-o-border-radius:2px;
	border-radius:2px;
	margin: 2px 0;
}
.yellow .dx-button2 {
	background-color:#630
}
.dx-button2 .dx-button-text {
	font-size:9pt;
	font-family:Calibri;
	font-weight:bold;
	color:#fff;
	padding:0 2px;
	display:inline;
	text-shadow:0 -1px 0 rgba(0, 0, 0, .5)
}
.dx-button .dx-button-content {
	text-overflow:ellipsis;
	overflow:hidden;
	max-height:100%
}
.dx-button.dx-state-disabled {
	cursor:default
}
.dx-button .dx-button-link {
	text-decoration:none
}
.dx-button .dx-icon {
	display:inline-block;
	vertical-align:middle
}
.dx-button .dx-button-text {
	display:inline;
	vertical-align:middle
}</style>
		<div data-role="page" data-theme="a">
<!--			<div data-role="header" data-position="fixed">
                                <h1>Results</h1>
                                <a href="home.php" class="back_head" data-icon="home" data-iconpos="notext">Menu</a>
                               <div class="ticker">
                               <div class="blinking" ></div>
                        </div>
                        </div>-->
                          <?php include("../include/header.php"); ?>
			<div data-role="content" data-theme="a">
<!--				<p>Your theme was successfully downloaded. You can use this page as a reference for how to link it up!</p>
				<pre>
<strong>&lt;link rel=&quot;stylesheet&quot; href=&quot;themes/plwin.min.css&quot; /&gt;</strong>
&lt;link rel=&quot;stylesheet&quot; href=&quot;http://code.jquery.com/mobile/1.4.0/jquery.mobile.structure-1.4.0.min.css&quot; /&gt;
&lt;script src=&quot;http://code.jquery.com/jquery-1.10.2.min.js&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js&quot;&gt;&lt;/script&gt;
				</pre>
				<p>This is content color swatch "A" and a preview of a <a href="#" class="ui-link">link</a>.</p>
				<label for="slider1">Input slider:</label>
				<input type="range" name="slider1" id="slider1" value="50" min="0" max="100" data-theme="a" />
				<fieldset data-role="controlgroup"  data-type="horizontal" data-role="fieldcontain">
				<legend>Cache settings:</legend>
				<input type="radio" name="radio-choice-a1" id="radio-choice-a1" value="on" checked="checked" />
				<label for="radio-choice-a1">On</label>
				<input type="radio" name="radio-choice-a1" id="radio-choice-b1" value="off"  />
				<label for="radio-choice-b1">Off</label>
				</fieldset>-->

                        <div>
                            <script type= "text/javascript">
                               $(function() {
                                   //alert(Playwin.config.urls.results);
                                   $.support.cors = true;
                                   $.mobile.allowCrossDomainPages = true;
                                    
                                    /*$.ajax({
                                    success: function (data) {
                                    
                                    },
                                    timeout:120000,
                                    error:          function (req, status) {
                                    every time get error
                                    },
                                    cache:
                                    false,
                                    url: serviceUri
                                    }*/
                                    //document.domain = "myplaywin.com";
                                    
                                    /*$.ajax({
                                        type:"GET",
                                        //cache:false,
                                        dataType:"jsonp",
                                       // ContentType:"text/xml",
                                       // processData: false,
                                        url:Playwin.config.urls.results*/
                                     /*   
                                   $.ajax({
                                        url: Playwin.config.urls.results,
                                        type: "GET"
                                        
                                    })
                                    .done(function(b){
                                        alert("Inside");
                                        //var f=$.xml2json(b);
                                        alert(JSON.stringify(b));
                                    }).fail( function(xhr, textStatus, errorThrown) {
                                       alert(textStatus);
                                       alert(errorThrown);
                                       alert(xhr.responseText);
                                    });
                                    return;    
                                   */
                                   //gameslist
                                   
                                   function getWeekStatus(type,data){ 
                                            var id = data.id; 
                                            var gameDetails=eval("Playwin.config.gameDetails.g"+id);
                                            var val=0;
                                            val=new Date().getDay();
                                            var ret=true;
                                            if(type=="today"){
                                                /*if($.inArray(val,gameDetails.weekDays)!=-1){
                                                    ret=false;
                                                }*/
                                                var x = data.lastResult.NextDrawDateNF.split(" ");
                                                if(x[0].trim() == Playwin.core.getFormatedDateTime(new Date(),"dd/MM/yyyy",true)){
                                                    return false;//return inverse because get status logic based on ! condition
                                                }else{
                                                    return true;//return inverse because get status logic based on ! condition
                                                }
                                            }else{
                                                if(type=="weekly"&&gameDetails.type!="weekly"){
                                                    ret=true
                                                }else{
                                                    if(type=="daily"&&gameDetails.type!="daily"){
                                                        ret=true
                                                    }else{
                                                        ret=false
                                                    }
                                                }
                                            }
                                            return ret
                                        }
                                   var listHtml = "";
                                   var type = window.location.hash;
                                   if(!(window.location.hash == "#daily" || window.location.hash == "#weekly" || window.location.hash == "#today")){
                                       type = "#all";
                                   }
                                   var gameList=[];
                                   $.each(Playwin.config.gameList,function(i,v){
                                       //if(type == "all" || type == "#"+v.type ){   
                                       var weekStatus = ! getWeekStatus(type.substring(1,type.length),v);
                                       if(weekStatus){  
                                           
                                           //<div class=\"dx-button2\" style=\"width:72px\"><span class=\"dx-button-text\" id=\"gameresult_"+v.id+"\"  class=\"gameresult\"  >Past Results</span></div>
                                           //<div class=\"dx-button2\" style=\"width:72px\"><span class=\"dx-button-text gameplay\" id=\"gameplay_"+v.id+"\"    >GamePlay</span></div>
                                           //<a id=\"gameinfo_"+v.id+"\"  href=\"GameInfo.php#"+v.id+"\"   class=\"gameinfo\" type=\"button\" value=\"GameInfo\" >GameInfo</a>
                                           //<a id=\"gameplay_"+v.id+"\" href=gamePlay\"#"+v.id+"\" class=\"gameplay\" type=\"button\" value=\"GamePlay\" >GamePlay</a>
                                         // <div class='ui-btn  yellow'  style='background: none repeat scroll 0 0 #DFEAF2;color:black;text-align:left;'>
                                          listHtml = listHtml+"<li style='margin:10px 0;'"+
                                                           "class='yellow'><div style='padding:5px 3px'><div style='font-size:0.9em;padding:0;margin-left:5px;margin-top:2px;line-height:1em'><span>"+v.name+"</span><span id='rolldown_amt_"+v.id+"'  style='font-size:0.9em;font-weight:bold'></span><br/> </div><div style='margin-top:-3px;'><b style='font-size:0.8em;margin-left:5px'><span id='draw_date_"+v.id+"'></span> </b></div><div><div style='margin-top:-10px'><p id='resultDiv"+v.id+"' class='resultDiv'></p></div></div> <div  style='margin-top:-8px; clear:both'> <div class='dx-button2' style='width:70px ;float:left;margin-left:10px;'><span class='dx-button-text gameinfo' id='gameinfo_"+v.id+"'>Info</span></div> <div class='dx-button2' style='float:left;width:72px;margin-left:10px;'><span class='dx-button-text gameplay' id='gameplay_"+v.id+"'    >Play Game</span></div> <div class='dx-button2' style='width:85px;margin-left:10px;float:left'><span class='dx-button-text pastresult' id='gameresult_"+v.id+"'>Past Results</span></div> <div style=' clear:both'></div></div></div>"+
                                                           //"<div style=\"width: 40%; font-size: 13px;\">Time Left : <span id=\"timer_"+v.id+"\" class=\"timers\">00:00:00</span></div>"+
                                                           ""+
                                                           ""+
                                                           
                                                           "   </li>";
                                           gameList.push(v);
                                      
                                        }                                       
                                   });
                                   
                                    //gameList=viewModel.getGameList();
                                      // viewModel.dataSourceResult(gameList);
                                        /*window.setTimeout(
                                        function() {
                                            if($(".timers").length==0){
                                                window.setTimeout(
                                                    function() {
                                                        setTimers(gameList);
                                                    }, 5000);
                                            }else{
                                                setTimers(gameList);
                                            }
                                        }, 3000);*/
                                       
                                   
                                   
                                   $('#gameslist').html(listHtml);
                                  
                                  $('.gameinfo').on('click', function(){
                                       
                                       var id = this.id;
                                       var arr = id.split("_");
                                       //var href = window.location.href;
                                       //var harr = href.split("#");
                                       
                                      // window.location.href = harr[0]+"#"+arr[1];
                                      var hash = window.location.hash.substring(1, window.location.hash.length);
                                     window.location.replace("GameInfo.php"+"?hash="+hash+"&typePage=gameResult#"+arr[1]);                                         
                                       //window.location.reload();
                                        
                                  });
                                  $('.gameplay').on('click', function(){
                                      
                                       /*var id = this.id;
                                       var arr = id.split("_");
                                       //var href = window.location.href;
                                       //var harr = href.split("#");
                                       
                                      // window.location.href = harr[0]+"#"+arr[1];
                                       window.location.replace("playGame.php#"+arr[1]);
                                       */
                                       
                                        var e=new Date();
                                        if(!(e.getHours()>=6&&e.getHours()<=21)){
                                            alert("The panel is closed. Gaming allowed from 07:00 AM to 10:00 PM.");
                                            return;
                                        }
        
                                        
                                       var id = this.id;
                                       var arr = id.split("_");
                                       
                                       var hash = window.location.hash.substring(1, window.location.hash.length);
                                       
                                       window.location.replace("playGame.php"+"?hash="+hash+"&typePage=gameResult#"+arr[1]);
                                       return false;
                                      
                                        
                                  });
                                  $('.gameresult').on('click', function(){
                                       var id = this.id;
                                       var arr = id.split("_");
                                       //var href = window.location.href;
                                       //var harr = href.split("#");
                                      
                                      // window.location.href = harr[0]+"#"+arr[1];
                                       window.location.replace("GameResult.php#"+arr[1]);
                                       //window.location.reload();
                                        
                                  });
                                  
                                   /* $('#otpsubmit').on('click', function(){
                                       if(Otp == $('#otp').val()){
                                           alert("OTP varification successfull.")
                                       }else{
                                           alert("Wrong OTP.")
                                       }
                                       return false;
                                        
                                    });*/
                                    //window.localStorage.clear();
                                    Playwin.core.loadResultData("result");
                                    $( "#resultLoading" ).popup();
                                    //$( "#resultLoading" ).popup( "open" );
                                    
                                    $( "#noOfResultListPopUp" ).popup();
                                    $( "#resultListPopUp" ).popup();
                                     $( "#showMoreBallsPopUp" ).popup();
                                    
                                    
                                    
                                    /*$("#resultLoading").on('popupbeforeposition', 'div:jqmData(role="popup")', function() {
                                            var notDismissible = $(this).jqmData('dismissible') === false;
                                            if (notDismissible) {
                                              $('.ui-popup-screen').off();
                                            }
                                    });*/
                                    $(".pastresult").on('click', function() {
                                           var data = {};
                                           data.type = type;
                                           var arr = this.id.split("_");
                                           $("#noOfResultGameId").val(arr[1]);
                                           showNoOfResultList(data);
                                    });
                                     
                                    
                                  /* $(".noOfResultClass").on('click', function() {
                                           alert("clisl");
                                           var arr = this.id.split("_");
                                           var count = arr[1];
                                           showOverLayResults(count);
                                    });*/
                                    
        //isShowNoOfResultList:ko.observable(false),
        //selectedGameData:ko.observable({}),
        //noOfResultList:ko.observable([]),
        function showNoOfResultList(data){
            var arr=data.type=="daily"?[{
                id:5,
                caption:"Last 5 Results"
            },{
                id:10,
                caption:"Last 10 Results"
            },{
                id:20,
                caption:"Last 20 Results"
            },{
                id:50,
                caption:"Last 50 Results"
            },{
                id:100,
                caption:"All Results ( Max 100 )"
            }]:[{
                id:5,
                caption:"Last 5 Results"
            },{
                id:10,
                caption:"Last 10 Results"
            },{
                id:20,
                caption:"Last 20 Results"
            },{
                id:50,
                caption:"Last 50 Results"
            },{
                id:100,
                caption:"All Results ( Max 100 )"
            }];
            setNoOfResultList(arr);
             var data = eval("Playwin.config.gameDetails." + "g" + $("#noOfResultGameId").val());
             $("#noOfResultListTitle").html(data.name);
            $( "#noOfResultListPopUp" ).popup("open");
            //viewModel.selectedGameData(data)
        }
        function hideNoOfResultList(data){
            viewModel.isShowNoOfResultList(false)
        }
        
        function showOverLayResults(count){
            
            $( "#noOfResultListPopUp" ).popup("close");
            var GameID = $("#noOfResultGameId").val();
            //alert(GameID);
            var data = eval("Playwin.config.gameDetails." + "g" + GameID);
            
            //viewModel.gameType(data.type);
            // viewModel.resultCount(count);
            //setResultTitle(data.name);
            //viewModel.resultGameId(data.id);
             $("#resultListTitle").html(data.name);
            
            
            
            //viewModel.overlayVisible(true);
            //viewModel.selGameDataOverLay(data);
            setOverLayResults(data,count)
        }
       /* function setOverLayResults(data){
            Playwin.play.setOverLayResults(data,viewModel)
            
        }*/
        function setOverLayResults(data,count){
            var overLayResults = [];
            //var b=new $.Deferred();
            
            
            $.get(Playwin.config.urls.results+"&gameid="+data.id+"&count="+count).done(function(d){
                $( "#resultListPopUp" ).popup("close");
                var e=$.xml2json(d);
                d=null;
                var j=(!e.GameResult)||(e.GameResult.length==0)?[]:e.GameResult;
                if(j.Game){
                    j=[j]
                }
                var f=$.map(j,function(k){
                    var h=$.trim(k.Result);
                    var i=h.split(" ");
                    var o="";
                    $.each(i,function(l,m){
                        if($.trim(m)!=""){
                            o=o+"<div "+(m!="TB"?"class='resultBall'>":"style='float:left;padding-top:5px;'>")+m+"</div>"
                        }
                    });
                    return{
                        Game:k.Game,
                        Result:k.Result,
                        DrawID:k.DrawID,
                        ResultHtml:o,
                        DrawDate:Playwin.core.getFormatedDateTime(k.DrawDate,"dddd, dd-MMM-yyyy hh:mm TT"),
                        NextDrawDate:Playwin.core.getFormatedDateTime(k.NextDrawDate,"dddd, dd-MMM-yyyy hh:mm TT"),
                        NextFirstPrize:k.NextFirstPrize
                    }
                });
                    //b.resolve(f);
                    var str = "";
                    $.each(f,function(i,v){
                        //str = str + "<li value=\""+v.id+"\"><a id=\"noresult_"+v.id+"\" class=\"noOfResultClass\"  >"+v.caption+"</a></li>";
                        /* str = str +"<li class=\"ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-first-child ui-btn-up-c\" data-corners=\"false\" data-shadow=\"false\" data-iconshadow=\"true\" data-wrapperels=\"div\" data-icon=\"arrow-r\" data-iconpos=\"right\" data-theme=\"c\">"
                        +"<div class=\"ui-btn-inner ui-li\">"
                        +"<div class=\"ui-btn-text\">"
                        +"<a class=\"ui-link-inherit\" href=\"#\"  id=\"noresult_"+v.id+"\">"+v.caption+"</a>"
                        +"</div>"
                        +"<span class=\"ui-icon ui-icon-arrow-r ui-icon-shadow\"> </span>"
                        +"</div>"
                        +"</li>";*/

                        str = str +"<li class='list' style='margin:3px 0px; padding:4px 0px; font-size:90%'>"
                        +"<div style=\"font-size:90%;clear:both\" >"+v.DrawDate+"</div>"
                        +"<div style=\"font-size:90%; white-space:normal\">"
                        +"<span >"+v.ResultHtml
                        +"</span>"
                        +"</div>"
                        +"<div style=\"font-size:90%;clear:both\">"
                        +"Draw Id:"
                        +"<span data-bind=\"text:$data.DrawID\" style=\"font-size:90%\">"+v.DrawID+"</span>"
                        +"</div>"
                        +"</li>";



                    });
                    $("#resultList").html(str);//alert(str);
                    $( "#resultListPopUp" ).popup("open");
                }).fail(function(){
                    alert("Error : Unable get results info.")
                })
        }
        function setNoOfResultList(arr){
            //noOfResultList
            var str = "";
            
            $.each(arr,function(i,v){
                str = str + "<li value=\""+v.id+"\" class='list' style='padding: 5px 0;'><a id=\"noresult_"+v.id+"\" class=\"noOfResultClass\"  >"+v.caption+"</a></li>";
                /* str = str +"<li class=\"ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-first-child ui-btn-up-c\" data-corners=\"false\" data-shadow=\"false\" data-iconshadow=\"true\" data-wrapperels=\"div\" data-icon=\"arrow-r\" data-iconpos=\"right\" data-theme=\"c\">"
                +"<div class=\"ui-btn-inner ui-li\">"
                +"<div class=\"ui-btn-text\">"
                +"<a class=\"ui-link-inherit\" href=\"#\"  id=\"noresult_"+v.id+"\">"+v.caption+"</a>"
                +"</div>"
                +"<span class=\"ui-icon ui-icon-arrow-r ui-icon-shadow\"> </span>"
                +"</div>"
                +"</li>";*/
            });
            $("#noOfResultList").html(str);
            $(".noOfResultClass").on('click', function() {
                //                   var arr = this.id.split("_");
                //                   var count = arr[1];
                //                   Playwin.core.setBannerImage("pastResults");
                //                   showOverLayResults(count);
                   var id = this.id;
                   var arr = id.split("_");
                   //var href = window.location.href;
                   //var harr = href.split("#");

                  // window.location.href = harr[0]+"#"+arr[1];
                  var hash = window.location.hash.substring(1, window.location.hash.length);
                 window.location.replace("gameResultList.php#"+$("#noOfResultGameId").val()+"_"+arr[1]);  
            });
           
        
        }    
        function setResultTitle (title){
           $("#noOfResultListTitle").html(title);
           $("#resultListTitle").html(title);
           
        }  
                                    
        

                                    
                                    
                                    /*function setTimers(dataArr){
                                            //alert(JSON.stringify(dataArr));
                                            Playwin.core.TimerPlugin.Timer = [];
                                            Playwin.core.TimerPlugin.TotalSeconds = [];
                                             var x = "";
                                             var id = 0;
                                            $(".timers")
                                            .each(
                                                function() { //alert(this.id);
                                                    var arr = this.id
                                                    .split("_");
                                                    if (arr[0] == "timer") {//alert(arr[1]);
                                                        x = "";
                                                        id = 0;
                                                        $
                                                        .each(
                                                            dataArr,
                                                            function(
                                                                i,
                                                                v) {
                                                                    //alert(JSON.stringify(v));
                                                                   // alert(v.id);
                                                                   // alert(arr[1]);
                                                                   // alert(parseInt(v.id) == arr[1]);
                                                                if (parseInt(v.id) == arr[1]) {
                                                                    x = v.lastResult.NextDrawDateNF;
                                                                    alert(v.lastResult.NextDrawDateNF);
                                                                    id = v.id
                                                                }else{
                                                                    
                                                                }
                                                            });
                                                            //alert(x);
                                                        if (x == undefined || x == "") {
                                                            //alert("uder");
                                                            return true;
                                                        }
                                                        //alert(x);
                                                        var dtStr = x
                                                        .split(" ");
                                                        var dtStrArr = dtStr[0]
                                                        .split("/");
                                                        var tmStrArr = dtStr[1]
                                                        .split(":");
                                                        var dt = new Date(
                                                            dtStrArr[2],
                                                            dtStrArr[1] - 1,
                                                            dtStrArr[0],
                                                            tmStrArr[0],
                                                            tmStrArr[1],
                                                            tmStrArr[2],
                                                            0);
                                                        var diff = parseInt((dt
                                                            .getTime() / 1000)
                                                        - (new Date()
                                                            .getTime() / 1000));
                                                        Playwin.core.TimerPlugin
                                                        .createTimer(
                                                            "timer_"
                                                            + id,
                                                            diff)
                                                    }
                                                });
                                            window.setTimeout("Playwin.core.TimerPlugin.Start()",1000)

                                        }*/
                                   
                                    $(document).on("click", ".back_head", function () {
                    document.location.replace(this.href);
                    return false;
                });
                Playwin.core.setBannerImage("result");
                             });
                            </script>
                            <div data-role="popup" id="resultLoading" data-dismissible='false' class="ui-content">
                                    <p> Loading .... </p>
                            </div>
                            <div class="content-primary">	
                                <ul data-role="listview" id="gameslist" >
                                    
                                </ul>
                                <p style="text-align: center; margin-top: 10px" >
                           <a href="#" class="result" data-position-to="window"  data-inline="true"  data-theme="a"><img class="result" id="ad_image" src="" /> </a></p>
                            </div><!--/content-primary -->
                            
                            <div data-role="popup" id="noOfResultListPopUp" class="ui-content">
                                 
                                <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
                                 <input type="hidden" id="noOfResultGameId" value ="" />
                                  <div>
            
                                 <div id="noOfResultListTitle" class="dx-popup-title"></div>
                                    <ul data-role="listview" id="noOfResultList" >

                                    </ul>

                            </div>
                            </div>
                            <div data-role="popup" id="resultListPopUp" class="ui-content">
                            <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
                                 <div style="height:300px;overflow-y:scroll">
                                <div id="resultListTitle" class="dx-popup-title"></div>
                                <a href="#" class="pastResults" data-position-to="window"  data-inline="true"  data-theme="a"><img class="pastResults" id="ad_image" src="" /> </a> 
                                <input type="hidden" id="resultGameCount" value ="" />
                                 
                                    <ul  class="dx-list-item" data-role="listview" id="resultList" >

                                    </ul>
                                 </div>
                            </div>
                            <div data-role="popup" id="showMoreBallsPopUp" class="ui-content" style="width:200px; height:140px">
                                <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
                                <div id="showMoreBalls" ></div>

                            </div>
                        </div>

			</div>
                       
                              <!-- footer -->
       <?php include("../include/footer.php"); ?>

                           
       
		</div>
	</body>
</html>