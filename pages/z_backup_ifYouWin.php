<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dial A Card</title>


        <link rel="stylesheet" href="../themes/plwin.min.css" />
        <link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
        <link rel="stylesheet" href="../css/common.css"/>
            <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />
   

        <!--		<script src="http://code.jquery.com/jquery-1.10.2.min.js" ></script>-->
                <script src="../js/jquery.js" ></script>
<!--		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>-->
                <script src="../js/jquery.mobile-1.4.0.min.js"></script>
        <script src="../js/xml2json.js?1"></script>
        <script src="../js/aes.js?1"></script>

        <script src="../js/playwin.core.js" ></script>
        <script src="../js/playwin.config.js" ></script>
         <script src="../js/blinktext.js"></script>

    </head>
    <body>

        <script>
            $(document).on("pageinit", "#ifyou", function () {
                $(document).on("click", ".back_head", function () {
    
                    document.location.replace(this.href);
                    return false;
                });
            });
        </script>
        <div data-role="page" id="ifyou">
<!--            <div data-role="header" data-theme="a">
                <h2>If You Win
                </h2>
                <a href="Settings.php" class="back_head" data-icon="back" data-iconpos="notext"></a>
            </div>-->
           <?php include("../include/header.php"); ?>
            <div data-role="content" data-theme="a" id="searchPickPlace">
                <div>
                    <p> 
                        Playwin offers you the unique benefit of claiming all prizes up to Rs. 10,000/- in cash (tax free) from any authorized Playwin outlet. All Prizes above Rs.	10,000/- will be paid by cheque (Tax deducted)..

                        <br/>For all prizes above Rs. 10000/- (on a single ticket) you need to fill a claim form, along with all relevant details as mentioned below. The claim form can be 

                        submitted to the claims department at any of our Playwin offices or through our regional distributors.</p>

                    <h1>REQUIRE DOCUMENTS FOR CLAIM</h1>
                    <ol type="1" ><li>Claim Form</li>

                        <li>Original Playwin Card & Password Number (Original Ticket & Claim receipt,  this is incase of lottery played on terminal)</li>

                        <li>Notarized Affidavit on Stamp Paper (Mandatory) & (IN English Only)</li>
                        <li>Photographs attested by Bank / Any Gazette Officer/ Notary. (Photos must be stamped)</li>

                        <li>Pan Card Copy (compulsory)</li>

                        <li>Proof of Identity (Copy of any of the Following)
                            <ul type="circle">
                                <li>Passport Copy</li>

                                <li>Voters Id Card Copy</li>

                                <li>Driving License Copy</li></ul></li>

                        <li>Proof of Residence(Ration Card Copy/ Electricity or Telephone Bill)</li></ol>
                    <h1>SEND YOUR CLAIM TO</h1>

                    <p>Pan India Network Ltd., <br />
                        Kohinoor City 613, A-Wing,<br />
                        Off. LBS Road, Kurla (W),<br />
                        Mumbai - 400070</p>

                    <p>Kindly download the Sikkim claim form to claim winnings for:-

                        Thunderball, Thursday Super Lotto, Saturday Super Lotto, Fast Digit Lottery

                        and Keno </p>

                </div>


            </div>

             <!-- footer -->
       <?php include("../include/footer.php"); ?>
        </div>

    </body>
</html>
