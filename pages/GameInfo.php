<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">    <title>Playwin Mobile</title>


        <link rel="stylesheet" href="../themes/plwin.min.css" />
        <link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
        <link rel="stylesheet" href="../css/common.css"/>
            <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />
    

        <!--		<script src="http://code.jquery.com/jquery-1.10.2.min.js" ></script>-->
                <script src="../js/jquery.js" ></script>
<!--		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>-->
                <script src="../js/jquery.mobile-1.4.0.min.js"></script>
       
        <script src="../js/playwin.config.js" ></script>
         <script src="../js/blinktext.js"></script>
    </head>
    <body>

            <script>
                
                
                $(function() {
                    var str=window.location.hash;
                    str=str.substring(1, str.length);
                    
                    $(".prizeStructure").hide();
                    $(".aboutGame").hide();
                    
                    $("#pz"+str).show();
                    $("#game"+str).show();
                    
                    
                    $(document).on("click", ".back_head", function () {
                        document.location.replace(this.href);
                        return false;
                    });
                    $(document).on("click", ".playGame_head", function () {
                        document.location.replace(this.href+str);
                        return false;
                    });
                    
                    
                });
            </script>
            <div data-role="page" id="page1" >
                 <?php include("../include/header.php"); ?>
                

                <div data-role="content">
                    <div data-role="fieldcontain">
                        <div class="aboutGame" id="game1" style="display: none">
                           <img height="39" width="97" border="1" style="float:left; border: medium none;"  src="../content/images/1.png"/>
                            <br><h2>ABOUT THURSDAY SUPER LOTTO</h2>
                <ul><li>Draw every Thursday between 10.00 - 10.30 p.m. on Zee Zing & Zee ETC</li><li>Starting Jackpot Rs. 2 crores.</li><li>Ticket MRP - Rs. 10/-</li></ul><h2>DRAW DETAILS</h2><ul><li>6 numbers between 01 and 49 shall be drawn</li><li>If all the 6 numbers on your ticket match the 6 numbers drawn, in any order, you will win or share the jackpot prize. Prizes are also paid on matching 3,4, OR 5 numbers out of the 6 numbers drawn.</li><li>If no player wins the Jackpot, the entire Jackpot amount rollsover to the next week.</li></ul>
                        
                        </div>
                        
                        <div class="aboutGame" id="game2" style="display: none"><br>
                             <img height="39" width="97" border="1" style="float:left; border: medium none;"  src="../content/images/2.png"/>
                             <br><br><h2>ABOUT THUNDERBALL </h2><ul><li>Draw every Tuesday between 10.00 - 10.30 p.m. on Zee Zing & Zee ETC</li><li>Starting Jackpot Rs. 1 crore.</li><li>Ticket MRP - Rs. 10/-</li></ul><h2>Draw Details</h2><ul><li>5 nos between 01 and 42 and 1 number between 01 and 15 shall be drawn</li><li>If all the 5 numbers  and the Thunderball number on your ticket match the numbers drawn, in any order, you will win or share the jackpot prize. If no player wins the Jackpot, the entire Jackpot amount rollsover to the next week.</li></ul></div>
                        <div class="aboutGame" id="game3" style="display: none"><br>
                             <img height="39" width="97" border="1" style="float:left; border: medium none;"  src="../content/images/3.png"/>
                             <br><br><h2>ABOUT PLAYWIN JALDI 5 LOTTO </h2><ul><li>Starting Jackpot Rs. 2lacs per draw </li><li>Select 10 Digits (From 01 to 31, match all and win the jackpot.)</li><li>1st Prize will be given on matching all the ten digits </li><li>2nd Prize will be given on matching Eight Digits</li><li>3rd Prize will be given on matching Six Digits</li><li>4th Prize will be given on matching Four Digits</li><li>Draw Frequency: Monday - Sunday @ 9:00pm</li><li>Price of Ticket Rs. 10/-</li></ul></div>
                        <div class="aboutGame" id="game4" style="display: none">
                             <img height="39" width="97" border="1" style="float:left; border: medium none;"  src="../content/images/4.png"/>
                             <br><br><h2>ABOUT PLAYWIN SATURDAY SUPER LOTTO</h2><ul><li>Draw every Saturday between 10.00 - 10.30 p.m. on Zee Zing & Zee ETC</li><li>Starting Jackpot Rs. 2 crores.</li><li>Ticket MRP - Rs. 10/-</li></ul><h2>DRAW DETAILS</h2><ul><li>6 numbers between 01 and 49 shall be drawn</li><li>If all the 6 numbers on your ticket match the 6 numbers drawn, in any order, you will win or share the jackpot prize. Prizes are also paid on matching 3,4, OR 5 numbers out of the 6 numbers drawn.</li><li>If no player wins the Jackpot, the entire Jackpot amount rollsover to the next week.</li></ul></div>
                        <div class="aboutGame" id="game5" style="display: none"> 
                             <img height="39" width="97" border="1" style="float:left; border: medium none;"  src="../content/images/5.png"/>
                             <br><br><h2>ABOUT PLAYWIN JALDI 5 LOTTO</h2><ul><li>Draw every Friday between 08.00 - 08.30 p.m. on Zee 24 Taas</li><li>Jackpot Rs. 5 lacs</li><li>Ticket MRP - Rs. 10/-</li></ul><br><span class="blue_text00"><h2>DRAW  DETAILS</h2></span><ol style="line-height:25px"><li>5 numbers between 01 and 36 shall be drawn</li><li>If a player matches his 5 numbers with the 5 numbers drawn, he will win the Jackpot</li><li>If no player wins the Jackpot, the entire Jackpot Collection or Rs. 1 lac whichever is higher will be rolled down to Match 4 winners.</li><li>The maximum winning after Roll-Down, per winner of Match 4, shall be restricted to Rs 10,000</li><li>Any Balance left in Jackpot Roll down to Match 4 winners, shall be rolled down to Match 3 winners</li></ol></div>
                        <div class="aboutGame" id="game9" style="display: none"> 
                             <img height="39" width="97" border="1" style="float:left; border: medium none;"  src="../content/images/9.png"/>
                             <br><br><h2>ABOUT PLAYWIN KENO</h2><ul><li>Draw every Sunday, Monday & Friday at 9:15pm</li><li>Jackpot Rs. 21 Lacs.</li><li>Ticket MRP - Rs. 10/-</li></ul><br><span class="blue_text00"><h2>DRAW  DETAILS</h2></span><ol style="line-height:25px"><li>The Keno Game has a matrix of 80 numbers.</li><li>On each draw date, there will be a random drawing of 20 numbers from a range of 1-80</li><li>To participate in a Keno game, a player can select 2 up to 10 numbers from a range of 1-80.Each such set is called a "SPOT"</li><li>There are 9 spots available for play to the customer, ranging from Spot 2 to Spot 10.</li><li>You have the option to play multiple games for each spot selected, i.e. 1/2/5 or 10 games. Each KENO Game is priced at Rs.10.</li><li>If all the numbers in your chosen spot match the numbers drawn for the day in any order, you win the prize as per the pre-announced prize structure.</li></ol><br><span class="blue_text00">ALLOCATION OF PRIZE MONEY</span><ul><li>The State determines a prize structure for each game, the text of which may be obtained from the Sikkim State lottery office and at points of sale as well. The amount of each prize as a result of the draw is calculated in accordance with the prize structure as determined by the Government of Sikkim</li><li>The maximum prize liability on a single draw will be restricted to Rs.60 Lakhs on Match 10/10, 9/9 and 8/8. In case the total prize payout for each of the above prize levels exceeds Rs.60 Lakhs, the amount of Rs.60 Lakhs will be distributed equally amongst the winners in that category.</li></ul></div>
                        <div class="aboutGame" id="game11" style="display: none">
                             <img height="39" width="97" border="1" style="float:left; border: medium none;"  src="../content/images/11.png"/>
                             <br><br><h2>ABOUT PLAYWIN JALDI 5 DOUBLE LOTTO</h2><ul><li>Draw every Wednesday between 08.00 - 08.30 p.m. on Zee 24 Taas</li><li>Jackpot Rs. 10 lacs.</li><li>Ticket MRP - Rs. 20/-</li></ul><span class="blue_text00">DRAW  DETAILS</span><ol style="line-height:25px"><li>5 numbers between 01 and 36 shall be drawn</li><li>If a player matches his 5 numbers with the 5 numbers drawn, he will win the Jackpot</li><li>If no player wins the Jackpot, the entire Jackpot Collection or Rs. 2 lacs whichever is higher will be rolled down to Match 4 winners</li><li>The maximum winning after Roll-Down, per winner of Match 4, shall be restricted to Rs 10,000</li><li>Any Balance left in Jackpot Roll down to Match 4 winners, shall be rolled down to Match 3 winners</li></ol></div>
                    </div>



                </div>
            
                <div data-role="footer" data-position="fixed" >
<!--                            <div style="float: left"><h1>PlayWin</h1></div>-->
                                
                                 <!-- footer -->
       <?php include("../include/footer.php"); ?>
<!--                            <div style="float: left"><h1>PlayWin</h1></div>-->
			</div>

        </div><!-- /page -->

        <!-- Start page -->
        <div data-role="page" id="page2" >

<!--            <div data-role="header" data-id="header1" data-theme="a" data-position="fixed">
                <h1>Prize Structure</h1>
                <a href="home.php" data-icon="back" data-iconpos="notext" class="back_head"></a>
                <div data-role="navbar">
                    <ul>
                        <li><a href="#page1" data-icon="home" data-iconpos="top"  >Game Info</a></li>
                        <li><a href="#page2" data-icon="info" data-iconpos="top" class="ui-btn-active ui-state-persist">Prize Structure</a></li>

                    </ul>
                </div> /navbar 
            </div> /header -->
            <?php include("../include/header.php"); ?>
            <div data-role="content">
                <ul data-role="listview" >
                    <li>
                        <div class="prizeStructure" id="pz1" style="display: none">
                            <table cellspacing="1" cellpadding="3"><tbody><tr><th>Match Nos.</th><th>Prize (Rs)</th></tr><tr><td>6</td><td>2
                                Crore *</td></tr><tr><td>5</td><td>50000
                                #</td></tr><tr><td>4</td><td>500 </td></tr><tr><td>3</td><td>50</td></tr></tbody>
                            </table>
                            <div>* Min Assured & Pari Mutuel, # Pari Mutuel.</div>
                        </div>
                        
                        <div class="prizeStructure" id="pz2" style="display: none"><table  cellspacing="2" cellpadding="3"><tbody><tr><th>Match Nos.</th><th>Prize (Rs)</th></tr><tr><td>5,TB</td><td>1 Crore *</td></tr><tr><td>5</td><td>40,000 #</td></tr><tr><td>4,TB</td><td>25,000</td></tr><tr><td>4</td><td>2,500</td></tr><tr><td>3,TB</td><td>1000</td></tr><tr><td>3</td><td>50</td></tr><tr><td>2,TB</td><td>50</td></tr><tr><td>1,TB</td><td>20 </td></tr></tbody></table><div>* Min Assured & Pari Mutuel, # Pari Mutuel.</div></div>
                        <div class="prizeStructure" id="pz11" style="display: none"><table  cellspacing="1" cellpadding="3"><tbody><tr><th>Match Nos.</th><th>Prize (Rs)</th></tr><tr><td>5</td><td>10 Lacs *</td></tr><tr><td>4</td><td>7500</td></tr><tr><td>3</td><td>200</td></tr><tr><td>2</td><td>30</td></tr></tbody></table><div> * Pari Mutuel</div></div>
                        <div class="prizeStructure" id="pz4" style="display: none"><table  cellspacing="1" cellpadding="3"><tbody><tr><th>Match Nos.</th><th>Prize (Rs)</th></tr><tr><td>6</td><td>2 Crores * </td></tr><tr><td>5</td><td>50,000 #</td></tr><tr><td>4</td><td>500</td></tr><tr><td>3</td><td>50 </td></tr></tbody></table><div>* Min Assured & Pari Mutuel, # Pari Mutuel.</div></div>
                        <div class="prizeStructure" id="pz3" style="display: none">
                            <table  cellspacing="2" cellpadding="3">
                                <tbody>
                                    <tr>
                                        <th><td>Prize Level</td><th>Prize (Rs)</th></tr><tr><td>1st Prize </td><td>200,000 * </td></tr><tr><td>2nd Prize</td><td>1135 # </td></tr><tr><td>3rd Prize</td><td>50</td></tr><tr><td>4th Prize</td><td>20</td></tr></tbody></table><div>* Min Assured &amp; Pari Mutuel, # Pari Mutuel.<br> # The above prize amounts will vary depending on draw timing and draw date. </div></div>
                        <div class="prizeStructure" id="pz5" style="display: none"><table  cellspacing="1" cellpadding="3"><tbody><tr><th>Match Nos.</th><th>Prize (Rs)</th></tr><tr><td>5</td><td>5 Lacs *</td></tr><tr><td>4</td><td>5000</td></tr><tr><td>3</td><td>100</td></tr><tr><td>2</td><td>10</td></tr></tbody></table><div> * Pari Mutuel</div></div>
                        <div class="prizeStructure" id="pz9" style="display: none"><table cellspacing="1" cellpadding="0"><tbody><tr valign="center" align="center"><td width="10%" height="22"></td><td width="10%" height="22">2</td><td width="10%" height="22">3</td><td width="10%" height="22">4</td><td width="10%" height="22">5</td><td width="10%" height="22">6</td><td width="10%" height="22">7</td><td width="10%" height="22">8</td><td width="10%" height="22">9</td><td width="10%" height="22">10</td></tr><tr valign="center" align="center"><td height="22">2</td><td height="22">140</td><td height="22">40</td><td height="22">20</td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td></tr><tr valign="center" align="center"><td height="22">3</td><td height="22"></td><td height="22">200</td><td height="22">50</td><td height="22">30</td><td height="22">20</td><td height="22">20</td><td height="22"></td><td height="22"></td><td height="22"></td></tr><tr valign="center" align="center"><td height="22">4</td><td height="22"></td><td height="22"></td><td height="22">600</td><td height="22">250</td><td height="22">90</td><td height="22">30</td><td height="22">20</td><td height="22">20</td><td height="22">10</td></tr><tr valign="center" align="center"><td height="22">5</td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22">4500</td><td height="22">800</td><td height="22">150</td><td height="22">100</td><td height="22">50</td><td height="22">30</td></tr><tr valign="center" align="center"><td height="22">6</td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22">5000</td><td height="22">1300</td><td height="22">1000</td><td height="22">250</td><td height="22">120</td></tr><tr valign="center" align="center"><td height="22">7</td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22">40,000</td><td height="22">4965*</td><td height="22">1,100</td><td height="22">1,000</td></tr><tr valign="center" align="center"><td height="22">8</td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22">3.5 lacs</td><td height="22">55,000</td><td height="22">10,000</td></tr><tr valign="center" align="center"><td height="22">9</td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22">7.5 lacs</td><td height="22">80,000</td></tr><tr valign="center" align="center"><td height="22">10</td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22"></td><td height="22">21 lacs</td></tr></tbody></table></div>
                    </li>
                </ul>
            </div><!-- /content -->
            
            <div data-role="footer" data-position="fixed" >
                 <div>
                    © 2013-2014 Pan India Network Ltd.
                    <a href="Disclaimer.php" style="text-decoration: underline;cursor: pointer;color: #bbb">Disclaimer</a>
                     ,
                    <a href="TandC.php" style="text-decoration: underline;cursor: pointer;color: #bbb">T & C </a>
                    </div>

            </div>

        </div><!-- /page -->




    </body>
</html>
