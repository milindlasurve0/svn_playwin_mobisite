<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>    <title>Playwin Mobile</title>


        <link rel="stylesheet" href="../themes/plwin.min.css" />
        <link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
        <link rel="stylesheet" href="../css/common.css"/>
    <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />
    
        <!--		<script src="http://code.jquery.com/jquery-1.10.2.min.js" ></script>-->
                <script src="../js/jquery.js" ></script>
<!--		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>-->
                <script src="../js/jquery.mobile-1.4.0.min.js"></script>
        
        <script src="../js/playwin.config.js" ></script>
         <script src="../js/blinktext.js"></script>
    </head>
    <body>
        <script>
            
            $(document).on("pageinit", "#our_values", function () {
                $(document).on("click", ".back_head", function () {
    
                    document.location.replace(this.href);
                    return false;
                });
            });
        </script>
        <div data-role="page" id="our_values" data-theme="a">
<!--            <div data-role="header" data-position="fixed">
                <h2>Our Values</h2>
                <a href="AboutPlaywin.php" class="back_head" data-icon="back" data-iconpos="notext"></a>
            </div>-->
            <?php include("../include/header.php"); ?>
            <div data-role="content" data-theme="a">
                <div class="form " >                                
                    <div class="home-splash">
                        <div>

                            <div class="home-dx-logo"></div>
                        </div>
                        <h1>   Customer Focus : </h1>

                        <p>We consider our lottery players, retailers and distributors as our customers.. 

                        <ul><li>
                                To the lottery players we will provide exciting and healthy entertainment by introducing new games, and convenience by creating a network of terminals at strategic locations.</li>
                            <li>
                                To the retailers and distributors, we shall provide the necessary support to earn their loyalty and improve their earnings capacity.</li></ul></p>

                        <h1>Team Work :</h1>
                        <p>
                            We are a part of the 'Playwin Family'. We believe that with mutual trust, respect and understanding for colleagues across all levels, departments and locations, we would be able to create a happy and healthy work culture.</p>
                        <p>
                            To us team work particularly means.

                        <ul><li>
                                Commitment to adhere to mutually agreed timelines.
                            </li><li>
                                Being responsive to the needs and requirements of others.
                            </li>
                            <li>
                                Open and clear communication
                            </li></ul></p>

                        <h1>Integrity :</h1>
                        <p>
                            We see integrity at the core of our dealing with all our stakeholders. We are committed to 

                        <ul><li>
                                Ensuring transparency in game design and draws to the lottery players
                            </li><li>
                                Adhering to commitments made to retailers, distributors and suppliers
                            </li><li>
                                Following the set systems, processes and policies in letter and spirit for a smooth functioning of the organization
                            </li></ul></p>

                        <h1>Knowledge Management & Enhancement :</h1>

                        <p>
                            Being pioneers of the online lottery business in India, we have a rich repository of knowledge specific to this industry. And constant upgradation of individual and organizational learning will enable us to maintain our competitive advantage. This we will strive to achieve by

                        <ul><li>
                                Constantly seeking newer opportunities for deploying technology to other applications
                            </li><li>
                                Encouraging innovations of systems and processes to improve individual and organizational productivity
                            </li></ul></p>

                    </div>
                     <!-- footer -->
       <?php include("../include/footer.php"); ?>
                </div>
            </div>
        </div>
      
</body>
</html>
