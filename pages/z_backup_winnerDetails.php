<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>    <title>Playwin Mobile</title>


        <link rel="stylesheet" href="../themes/plwin.min.css" />
        <link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
        <link rel="stylesheet" href="../css/common.css"/>
        <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />
   

        <!--		<script src="http://code.jquery.com/jquery-1.10.2.min.js" ></script>-->
                <script src="../js/jquery.js" ></script>
<!--		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>-->
                <script src="../js/jquery.mobile-1.4.0.min.js"></script>
        <script src="../js/xml2json.js?1"></script>
        <script src="../js/playwin.core.js" ></script>
        <script src="../js/playwin.config.js" ></script>
         <script src="../js/blinktext.js"></script>

    </head>
    <body>


        <script>
             
             $(function() {
                var str=window.location.hash;
               
                str=str.substring(1, str.length);
                var img="";
                $.ajax({
                    type:"GET",
                    url:"https://www.myplaywin.com/PlaywinWinner.aspx?year="+str,
                    data:""
                }).done(function(a){
                    
                    var d=$.xml2json(a);
                  
                   
                    
                    if(!d.Winner){
                          img="<div>No Winners Found</div>";
                    }else{
                        $.each(d.Winner,function(n,g){
                        
                        img=img+"<li data-corners='false' data-shadow='false' data-iconshadow='true' data-wrapperels='div' data-theme='c' class='ui-btn ui-li ui-li-has-thumb ui-btn-up-c'><div class='ui-btn-inner ui-li'><div class='ui-btn-text' style='text-align:left'>"+
                            "<div ><img style='max-height: 6em;max-width: 6em; margin: 1.45em 0 0 0.45em;' src='"+"https://images.myplaywin.com/"+g.ImageName+"' class='ui-li-thumb'/></div>"+
                            "<div style='margin-left:6.1em' ><h3 class='ui-li-heading'>"+g.Name+"</h3>"+
                            "<p class='ui-li-desc'> <span style='color: black;font-weight: bold'>City : </span><strong>"+g.City+"</strong></p>"+
                            "<p class='ui-li-desc'> <span style='color: black;font-weight: bold'>Date: </span><strong>"+g.WinnerDate+"</strong></p>"+
                            "<p class='ui-li-desc'> <span style='color: black;font-weight: bold'>Game: </span><strong>"+g.GameName+"</strong></p>"+
                            "<p class='ui-li-desc'> <span style='color: black;font-weight: bold'>Prize: </span><strong>"+g.WinnerPrize+"</strong></p>"+
                            "<p class='ui-li-desc'> <span style='color: black;font-weight: bold'>Winning No: </span><strong>"+g.WinnerNumber+"</strong></p>"+
                            "</div></div></div>";
                        "</li>";
                        //"<li class='winner-list'><img src='"+"https://images.myplaywin.com/"+g.ImageName+"'/><h2>"+g.Name+"</h2><p>"+g.City+"</p></li>";
                    });
                    }
                    $("#winner").html(img);
                    // str=str+"<li class="+"list"+" id="+a+"><a rel=\"external\" href="+"winnerDetails.php#"+a+">"+a+"</a></li>";                 
                    
                    //$('[type="submit"]').button('enable');	
                }).fail(function(){
                    	
                    alert("Error : Please Check Your Connection .","Error!")
                }) ;
            
                   $(document).on("click", ".back_head", function () {
     document.location.replace(this.href);
     return false;
 });
            });
        

        </script>
        <div data-role="page" data-theme="a">
<!--            <div data-role="header" data-position="fixed">
                
                <h2>Winners</h2>
                <a href="YearsForWinner.php" class="back_head" data-icon="back" data-iconpos="notext"></a>
               
            </div>-->
            <?php include("../include/header.php"); ?>
            <div data-role="content" data-theme="a">

                <ul data-role="listview" data-theme="c" data-inset="true" id="winner">

                </ul>


            </div>

             <!-- footer -->
       <?php include("../include/footer.php"); ?>

        </div>


    </body>
</html>
