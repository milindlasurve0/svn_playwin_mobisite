<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">    <title>Playwin Mobile</title>
        <link rel="stylesheet" href="../themes/plwin.min.css" />
        <link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
        <link rel="stylesheet" href="../css/common.css"/>
        <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />

    </head>
    <body>
        
        <div data-role="page" id="about" data-theme="a">
            <div data-role="content" data-theme="a">
                <p> 
                    Playwin is the lottery & gaming brand of Pan India Network Ltd., part of the 2.4 Billion Dollar Essel Group. Pan India Network Ltd. is in the business of providing infrastructure, data communication, marketing support and service to facilitate a secure online lottery network. </p>
                <p>
                    Playwin has three big lotto games Thunder Ball, Thursday Super Lotto, & Saturday Super Lotto. The biggest jackpot hit from the Playwin is of Thursday Super Lotto game, amounting Rs. 17.29 crore & won by a resident of Kolkatta on May 25, 2006. 
                </p>
                <p>
                    Within a span of TEN years, Playwin has created 82 Crorepatis and over 4,000 lakhpati's. Playwin games are automated and legal, assuring each player of a fair playing ground.
                    The hassle-free and transparent system is the significant feature that distinguishes Playwin from the rest.
                </p>
                <p>
                    Playwin lotto games can be played on various platforms including retail outlets, internet and as well as through the SMS platform.
                </p>  
            </div>
        </div>
       

    </body>
</html>
