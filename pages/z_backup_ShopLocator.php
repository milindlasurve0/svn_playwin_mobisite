<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Shop Locator</title>


        <link rel="stylesheet" href="../themes/plwin.min.css" />
        <link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
        <link rel="stylesheet" href="../css/common.css"/>
            <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />
   

        <!--		<script src="http://code.jquery.com/jquery-1.10.2.min.js" ></script>-->
                <script src="../js/jquery.js" ></script>
<!--		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>-->
                <script src="../js/jquery.mobile-1.4.0.min.js"></script>
        
        <script src="../js/playwin.config.js" ></script>
         <script src="../js/blinktext.js"></script>

    </head>
    <body>
        <script>
            
            var shopsData={shopData:[{
                        ShopName:"Lucky Lottery",
                        Address:"31/32 Old Cloth Market",
                        City:"Ahmednagar",
                        State:"Maharashtra",
                        Postcode:414001,
                        ContactName:"Mr.Kamleshkumar",
                        ContactNumber:"9423792145",
                        Xcoord:74.74959,
                        Ycoord:19.09521,
                        uuid:1
                    },{
                        ShopName:"Star Lottery Centre",
                        Address:"Opp. Bank Of India  Padhye Wadi  Dist - Raigad",
                        City:"Alibag",
                        State:"Maharashtra",
                        Postcode:402201,
                        ContactName:"Mr.Prakash",
                        ContactNumber:"9422094112",
                        Xcoord:72.86792,
                        Ycoord:18.65549,
                        uuid:2
                    },{
                        ShopName:"Choice Point",
                        Address:"Shop No.6  Aayushi Building  B- Wing  J.B.Nagar  Next to  Canara Bank",
                        City:"Andheri(E)",
                        State:"Maharashtra",
                        Postcode:400059,
                        ContactName:"Mr.Ramnikbhai /Mr.Bipinbhai",
                        ContactNumber:"9820434422",
                        Xcoord:72.87813,
                        Ycoord:19.11099,
                        uuid:3
                    },{
                        ShopName:"Ameena Lottery Centre",
                        Address:"M.A.Road  Opp.Bus Depot  Near Railway Station",
                        City:"Andheri(W)",
                        State:"Maharashtra",
                        Postcode:400058,
                        ContactName:"Mr.Riyaz",
                        ContactNumber:"9920040222",
                        Xcoord:72.82916,
                        Ycoord:19.12974,
                        uuid:4
                    },{
                        ShopName:"Guruprasad Medical & General Stores",
                        Address:"Shop No.17  Twin Towers  Lokhandwala Complex",
                        City:"Andheri(W)",
                        State:"Maharashtra",
                        Postcode:400053,
                        ContactName:"Mr.Amit",
                        ContactNumber:"9892456199",
                        Xcoord:72.82348,
                        Ycoord:19.14457,
                        uuid:5
                    },{
                        ShopName:"Rukhmini Lottery Agency",
                        Address:"17,Gomtesh Market  New Gulmandi Road",
                        City:"Aurangabad",
                        State:"Maharashtra",
                        Postcode:431001,
                        ContactName:"Mr.Om Khandelwal",
                        ContactNumber:"9822899520",
                        Xcoord:75.32811,
                        Ycoord:19.88558,
                        uuid:6
                    },{
                        ShopName:"Satish Stores",
                        Address:"Linking Road  Opp. Amarsons  Near Petrol Pump",
                        City:"Bandra (W)",
                        State:"Maharashtra",
                        Postcode:400050,
                        ContactName:"Mr.Satish Shetty",
                        ContactNumber:"(022)26406612",
                        Xcoord:72.83083,
                        Ycoord:19.05523,
                        uuid:7
                    },{
                        ShopName:"Bipin Online Plaza",
                        Address:"C/99  Station Plaza  Near ICICI Bank  Station Road",
                        City:"Bhandup(W)",
                        State:"Maharashtra",
                        Postcode:400078,
                        ContactName:"Mr.Tarachandbhai",
                        ContactNumber:"9920334142",
                        Xcoord:72.93652,
                        Ycoord:19.14483,
                        uuid:8
                    },{
                        ShopName:"R.H.Chheda Online",
                        Address:"46  Indira Market  Station Road",
                        City:"Bhayandar(W)",
                        State:"Maharashtra",
                        Postcode:401101,
                        ContactName:"Mr.Rajesh.H.Chheda",
                        ContactNumber:"9867997399",
                        Xcoord:72.84982,
                        Ycoord:19.30632,
                        uuid:9
                    },{
                        ShopName:"Dharmendra Agency",
                        Address:"44,Bazar Peth Road  Tinbatti  Navi Chawl  Opp.Bharat Gas Centre",
                        City:"Bhiwandi",
                        State:"Maharashtra",
                        Postcode:421302,
                        ContactName:"Mr.Dharmendra Dhirajlal Shah",
                        ContactNumber:"9975233887",
                        Xcoord:73.04829,
                        Ycoord:19.28125,
                        uuid:10
                    },{
                        ShopName:"Chheda Lottery ",
                        Address:"R.M.Rabari Chawl  Carter Road No.8",
                        City:"Borivali(E)",
                        State:"Maharashtra",
                        Postcode:400066,
                        ContactName:"Mr.Khimji Gangji Chheda",
                        ContactNumber:"9220815305",
                        Xcoord:72.91062,
                        Ycoord:19.21471,
                        uuid:11
                    },{
                        ShopName:"Goraksha Online",
                        Address:"Room No.4  Kunj Vihar Housing Society  Chandavarkar Lane  Opp.Gora Gandhi Appartment",
                        City:"Borivali(W)",
                        State:"Maharashtra",
                        Postcode:400092,
                        ContactName:"Mr.Jignesh A Parekh ",
                        ContactNumber:"9892335555",
                        Xcoord:72.84564,
                        Ycoord:19.23723,
                        uuid:12
                    },{
                        ShopName:"Pramod A.Parekh",
                        Address:"C/o.Century Cards Shop No.1 Jaya Cinema Compound S.V.Road",
                        City:"Borivali(W)",
                        State:"Maharashtra",
                        Postcode:400092,
                        ContactName:"Mr.Pramod A Parekh ",
                        ContactNumber:"9892936856",
                        Xcoord:72.84564,
                        Ycoord:19.23723,
                        uuid:13
                    },{
                        ShopName:"Sri Ganesh Travels",
                        Address:"Shop No.3 Himmat Mension Opp Shimpoli Corner Next to Chirag Collection S.V.Road",
                        City:"Borivali(W)",
                        State:"Maharashtra",
                        Postcode:400092,
                        ContactName:"Mrs.Prakash Furia",
                        ContactNumber:"9969946310",
                        Xcoord:72.84564,
                        Ycoord:19.23723,
                        uuid:14
                    },{
                        ShopName:"Navneet Lottery Centre",
                        Address:"Shop No.33 C.S.T Subway (V.T.)",
                        City:"C.S.T.",
                        State:"Maharashtra",
                        Postcode:400001,
                        ContactName:"Mr.Tulsi ",
                        ContactNumber:"8451068369",
                        Xcoord:72.83604,
                        Ycoord:18.94393,
                        uuid:15
                    },{
                        ShopName:"Hiren Jayantilal Gada",
                        Address:"2/21 White House T.B.Kadam Marg Cross Road",
                        City:"Chinchpokli",
                        State:"Maharashtra",
                        Postcode:400027,
                        ContactName:"Mr.Hiren Jayantilal Gada",
                        ContactNumber:"9920510960",
                        Xcoord:72.83333,
                        Ycoord:18.98333,
                        uuid:16
                    },{
                        ShopName:"Nityanand Lottery Centre ",
                        Address:"196-A Sainath Estate Near Chunabhatti Rly. Station",
                        City:"Chunabhatti(E)",
                        State:"Maharashtra",
                        Postcode:400022,
                        ContactName:"Mr.Raghu S. Shetty",
                        ContactNumber:"9892002217",
                        Xcoord:72.86632,
                        Ycoord:19.04089,
                        uuid:17
                    },{
                        ShopName:"Sai Agency",
                        Address:"Shop No.18 & 19 Churchgate Subway",
                        City:"Churchgate",
                        State:"Maharashtra",
                        Postcode:400020,
                        ContactName:"Mr.Kantilal Dedhia/Mr.Nilesh Dedhia",
                        ContactNumber:"9821526557",
                        Xcoord:72.82644,
                        Ycoord:18.93512,
                        uuid:18
                    },{
                        ShopName:"Meeting Point",
                        Address:"Ground Floor Modhvanik Vidyarthi Bhavan Plot No.15 Opp.Fedral Bank S.G.J. Road",
                        City:"Dadar(E)",
                        State:"Maharashtra",
                        Postcode:400014,
                        ContactName:"Mr.Kishor Nenshi Shah",
                        ContactNumber:"9892562870",
                        Xcoord:72.84269,
                        Ycoord:19.00991,
                        uuid:19
                    },{
                        ShopName:"Lucky Star Online",
                        Address:"Shop No.7 Ahmed Umer Building Opp.Dadar Railway Station S.B.Marg",
                        City:"Dadar(W)",
                        State:"Maharashtra",
                        Postcode:400028,
                        ContactName:"Mr.Upendra",
                        ContactNumber:"9320767129",
                        Xcoord:72.83825,
                        Ycoord:19.01955,
                        uuid:20
                    },{
                        ShopName:"Parasmani Online",
                        Address:"Shop No.2 Gananath C.H.S.Ltd Dr.D'Silva Road",
                        City:"Dadar(W)",
                        State:"Maharashtra",
                        Postcode:400028,
                        ContactName:"Mr.Sachin Warange",
                        ContactNumber:"9892108877",
                        Xcoord:72.84182,
                        Ycoord:19.01904,
                        uuid:21
                    },{
                        ShopName:"Status Cover",
                        Address:"1 Mayekar Sadan Near Shakti Nagar & Shagun Hotel Behind Anand Nagar",
                        City:"Dahisar(E)",
                        State:"Maharashtra",
                        Postcode:400068,
                        ContactName:"Mr.Hitesh",
                        ContactNumber:"9821217114",
                        Xcoord:72.86558,
                        Ycoord:19.25568,
                        uuid:22
                    },{
                        ShopName:"Laxmi Lottery",
                        Address:"76 Nagindas Master Road Modi & Modi Bldg. No.2 Mendows Street",
                        City:"Fort",
                        State:"Maharashtra",
                        Postcode:400001,
                        ContactName:"Mr.Babu Kannan Mudliyar",
                        ContactNumber:"9967926798",
                        Xcoord:72.83678,
                        Ycoord:18.93466,
                        uuid:23
                    },{
                        ShopName:"Sai Ratna Online",
                        Address:"Shop No.9/A Seth Building Gunboe Street Opp.Swagat Hotel",
                        City:"Fort",
                        State:"Maharashtra",
                        Postcode:400001,
                        ContactName:"Mr.Jitendra / Mr.Sunil",
                        ContactNumber:"9892221046",
                        Xcoord:72.83678,
                        Ycoord:18.93466,
                        uuid:24
                    },{
                        ShopName:"Sai Baba Lottery Centre",
                        Address:"J.P.Road Opp.Neo Welcome Hotel",
                        City:"Ghatkopar(W)",
                        State:"Maharashtra",
                        Postcode:400086,
                        ContactName:"Mr.Mitesh/Mr.Shankarbhai",
                        ContactNumber:"9819953569",
                        Xcoord:72.90767,
                        Ycoord:19.09081,
                        uuid:25
                    },{
                        ShopName:"Narayan Lottery Centre",
                        Address:"Shop No.12 New Market Margao",
                        City:"Goa",
                        State:"Goa",
                        Postcode:403601,
                        ContactName:"Mr.Vishwanath.N.Borkar",
                        ContactNumber:"9822125932",
                        Xcoord:73.95968,
                        Ycoord:15.27159,
                        uuid:26
                    },{
                        ShopName:"Maharaja Agency",
                        Address:"Ward No.5 House No.139 Lyikar Galli Near Nutan Bank Main Road",
                        City:"Ichalkaranji",
                        State:"Maharashtra",
                        Postcode:416115,
                        ContactName:"Mr.Nurmohamad Abbas Bagwan",
                        ContactNumber:"9822068633",
                        Xcoord:74.4667,
                        Ycoord:16.7,
                        uuid:27
                    },{
                        ShopName:"Baheti Lottery",
                        Address:"Opp.Old Bus Stand",
                        City:"Jalgaon",
                        State:"Maharashtra",
                        Postcode:425001,
                        ContactName:"Mr.Vinod",
                        ContactNumber:"9420389239",
                        Xcoord:75.56765,
                        Ycoord:21.01351,
                        uuid:28
                    },{
                        ShopName:"Vyas Agency",
                        Address:"180 Polan Peth Old Cloth Market",
                        City:"Jalgaon",
                        State:"Maharashtra",
                        Postcode:425001,
                        ContactName:"Mr.Ritesh Vyas",
                        ContactNumber:"9890056854",
                        Xcoord:75.56397,
                        Ycoord:21.01332,
                        uuid:29
                    },{
                        ShopName:"Narayan Lottery Agency",
                        Address:"Kapad Bazar",
                        City:"Jalna",
                        State:"Maharashtra",
                        Postcode:431203,
                        ContactName:"Mr.Ramesh /Mr.Ritesh",
                        ContactNumber:"9403011455",
                        Xcoord:75.88718,
                        Ycoord:19.83895,
                        uuid:30
                    },{
                        ShopName:"Maru Online",
                        Address:"Shop No. 2 New Sai Darshan Society Nr.National Restaurant Bombay-Pune Road",
                        City:"Kalwa(W)",
                        State:"Maharashtra",
                        Postcode:400605,
                        ContactName:"Mr.Paresh Mavji Maru",
                        ContactNumber:"9920100229",
                        Xcoord:72.9992,
                        Ycoord:19.19439,
                        uuid:31
                    },{
                        ShopName:"Kiran Online",
                        Address:"Shop No.23 Grd Flr Borgaonkar Wadi Shopping Complex St.Road Near Suyesh Plaza",
                        City:"Kalyan(W)",
                        State:"Maharashtra",
                        Postcode:421301,
                        ContactName:"Mr.Pandarinath Ramdas Deshmukh",
                        ContactNumber:"(95251) 2431746",
                        Xcoord:73.12242,
                        Ycoord:19.24561,
                        uuid:32
                    },{
                        ShopName:"Prince Lottery Center",
                        Address:"Mohamd Ali Chowk Station Road",
                        City:"Kalyan(W)",
                        State:"Maharashtra",
                        Postcode:421301,
                        ContactName:"Mr.Mahendra/Mr.Amar",
                        ContactNumber:"9819574813",
                        Xcoord:73.12242,
                        Ycoord:19.24561,
                        uuid:33
                    },{
                        ShopName:"Shah Nanji Morarji & Co.",
                        Address:"Shop No.2 & 3 Amar Villa Building S.V.Road Near Saibaba Temple",
                        City:"Kandivali(W)",
                        State:"Maharashtra",
                        Postcode:400067,
                        ContactName:"Mr.Manoj Vasanji Gala.",
                        ContactNumber:"9920484660",
                        Xcoord:72.83087,
                        Ycoord:19.20994,
                        uuid:34
                    },{
                        ShopName:"Pariwar Tea Center",
                        Address:"E/5 Nutan Nagar Kanjur Village Road",
                        City:"Kanjurmarg(E)",
                        State:"Maharashtra",
                        Postcode:400042,
                        ContactName:"Mr.Ramesh Murji Gala",
                        ContactNumber:"9322223587",
                        Xcoord:72.9323,
                        Ycoord:19.12949,
                        uuid:35
                    },{
                        ShopName:"Om Lottery",
                        Address:"Main Road",
                        City:"Khamgaon",
                        State:"Maharashtra",
                        Postcode:444303,
                        ContactName:"Mr.Manish Omprakash Khetan",
                        ContactNumber:"9422184523",
                        Xcoord:76.19111,
                        Ycoord:20.52383,
                        uuid:36
                    },{
                        ShopName:"Ritswet Agencies",
                        Address:"Rajsarovar Niketan.315 - A Linking Road Next to Punjab and Sind Bank",
                        City:"Khar(W)",
                        State:"Maharashtra",
                        Postcode:400052,
                        ContactName:"Mr.Vinodbhai",
                        ContactNumber:"9773161642",
                        Xcoord:72.83613,
                        Ycoord:19.07073,
                        uuid:37
                    },{
                        ShopName:"Maharaja Enterprises",
                        Address:"Gala No.B- 19 Royal Plaza Dabholkar Corner Opp.S.T.Stand Opp.Tej Courier",
                        City:"Kolhapur ",
                        State:"Maharashtra",
                        Postcode:416001,
                        ContactName:"Mr.Firoj Abbas Bagwan",
                        ContactNumber:"9623181920",
                        Xcoord:74.24487,
                        Ycoord:16.69131,
                        uuid:38
                    },{
                        ShopName:"Parvati Lottery Centre ",
                        Address:"Mitra Nagar Shivaji Chowk",
                        City:"Latur",
                        State:"Maharashtra",
                        Postcode:413512,
                        ContactName:"Mr.Mallinath Manmthappa",
                        ContactNumber:"9421090091",
                        Xcoord:76.58097,
                        Ycoord:18.39768,
                        uuid:39
                    },{
                        ShopName:"Pooja Lottery Center",
                        Address:"Shah & Sanghi Compound Room No.56 Patra Shed Sitaram Jadhav Marg",
                        City:"Lower Parel",
                        State:"Maharashtra",
                        Postcode:400013,
                        ContactName:"Mr.Sachin Narayan Warang",
                        ContactNumber:"9869138864",
                        Xcoord:72.82911,
                        Ycoord:18.99658,
                        uuid:40
                    },{
                        ShopName:"Dhiren Mavji Gala",
                        Address:"C/o.Jivandhara Chemist 7 Navjivan Colony Mori Road",
                        City:"Mahim(W)",
                        State:"Maharashtra",
                        Postcode:400017,
                        ContactName:"Mr.Dhiren Mavji Gala",
                        ContactNumber:"9820050234",
                        Xcoord:72.84041,
                        Ycoord:19.04288,
                        uuid:41
                    },{
                        ShopName:"Jai Santoshi Maa Lottery Centre",
                        Address:"Shop No.1  Jain Mansion  Near Ashok Samrat Bldg.Daftari Road",
                        City:"Malad(E)",
                        State:"Maharashtra",
                        Postcode:400097,
                        ContactName:"Mr.Bhupesh/Mr.Paresh",
                        ContactNumber:"9987331841",
                        Xcoord:72.85541,
                        Ycoord:19.18024,
                        uuid:42
                    },{
                        ShopName:"Classic Online",
                        Address:"613 Sethna Building J.S.S.Road  Princess Street",
                        City:"Marine Lines",
                        State:"Maharashtra",
                        Postcode:400002,
                        ContactName:"Mr.Bipin Shah",
                        ContactNumber:"9821313901",
                        Xcoord:72.82787,
                        Ycoord:18.9456,
                        uuid:43
                    },{
                        ShopName:"Chheda Lottery Centre",
                        Address:"106  Majid Bunder Road",
                        City:"Masjid Bunder",
                        State:"Maharashtra",
                        Postcode:400003,
                        ContactName:"Mr.Kantibhai Chheda/ Mr.Kailash",
                        ContactNumber:"9820536853",
                        Xcoord:72.83973,
                        Ycoord:18.96186,
                        uuid:44
                    },{
                        ShopName:"Bhagyodaya Lottery Agency",
                        Address:"Shop No.7  harmeshwar Building  T.H.Kataria Marg",
                        City:"Matunga(W)",
                        State:"Maharashtra",
                        Postcode:400016,
                        ContactName:"Mr.Amrut/Sanjeev Miskin",
                        ContactNumber:"9892278094",
                        Xcoord:72.84545,
                        Ycoord:19.0308,
                        uuid:45
                    },{
                        ShopName:"Champion Lottery Centre",
                        Address:"73/77  Sheth Moti Shah Road  Opp Maruti Mandir  Love Lane",
                        City:"Mazagaon",
                        State:"Maharashtra",
                        Postcode:400010,
                        ContactName:"Mr.Nitin Mulji Gala",
                        ContactNumber:"9821402666",
                        Xcoord:72.83693,
                        Ycoord:18.97226,
                        uuid:46
                    },{
                        ShopName:"Jai Mata Di Online",
                        Address:"Shop No.2  B -30,Dreamland Hsg.Society  Sector -11  Next to Kotak Mahindra ATM  Shanti Nagar  Behind TMT Bus Stop",
                        City:"Mira Road[E]",
                        State:"Maharashtra",
                        Postcode:401107,
                        ContactName:"Mr.Bishwajyoti Ray",
                        ContactNumber:"9820043121",
                        Xcoord:72.86927,
                        Ycoord:19.2855,
                        uuid:47
                    },{
                        ShopName:"Adarsh Ice Cream Centre",
                        Address:"Shop No.7  Jalaram Estate  M.G.Road  Opp.Shiv Sena Shakha",
                        City:"Mulund(W)",
                        State:"Maharashtra",
                        Postcode:400080,
                        ContactName:"Mr.Dilip Tappubhai Barot",
                        ContactNumber:"9930005102",
                        Xcoord:72.9431,
                        Ycoord:19.17467,
                        uuid:48
                    },{
                        ShopName:"Bhavesh M.Ruparel (H.U.F)",
                        Address:"Shop No.4  Babu Niwas  S.V.P.Road  Nr Rly Station",
                        City:"Mulund(W)",
                        State:"Maharashtra",
                        Postcode:400080,
                        ContactName:"Mr.Bhavesh M.Ruparel (H.U.F)",
                        ContactNumber:"9987642616",
                        Xcoord:72.9431,
                        Ycoord:19.17467,
                        uuid:49
                    },{
                        ShopName:"Jai Santoshi Maa Lottery Bhandar",
                        Address:"Shop No.14  Bodke Bldg.S.V.P. Road  Near Railway Station",
                        City:"Mulund(W)",
                        State:"Maharashtra",
                        Postcode:400080,
                        ContactName:"Mr.Mohanlal/Sohanlal",
                        ContactNumber:"9819161758",
                        Xcoord:72.9431,
                        Ycoord:19.17467,
                        uuid:50
                    },{
                        ShopName:"Shree Gajanan Lottery Centre",
                        Address:"Shop No.11  S.V.P. Road  Station Road  Nr.Railway Station",
                        City:"Mulund(W)",
                        State:"Maharashtra",
                        Postcode:400080,
                        ContactName:"Mr.Manoj Kanhaiyalal Jaiswal",
                        ContactNumber:"9867971124",
                        Xcoord:72.9431,
                        Ycoord:19.17467,
                        uuid:51
                    },{
                        ShopName:"Mrs.Jigna Mehul Maroo",
                        Address:"287,Padmakunj  2nd Floor  Khapripura  Near K.B..Complex  Opp.M.G.Sales  Itwari",
                        City:"Nagpur",
                        State:"Maharashtra",
                        Postcode:440002,
                        ContactName:"Mr.Mehul Dhiraj Maroo",
                        ContactNumber:"9373103249",
                        Xcoord:79.10186,
                        Ycoord:21.15873,
                        uuid:52
                    },{
                        ShopName:"Raval Online",
                        Address:"Shop No.22  Gagan Marvel   I - Wing  Gagan Dream  Near Rashmi Resi. Complex  Vasant Link Road  Achole",
                        City:"Nalasopara(E)",
                        State:"Maharashtra",
                        Postcode:401209,
                        ContactName:"Mr.Ashish Khimji Chheda",
                        ContactNumber:"9892630535",
                        Xcoord:72.84564,
                        Ycoord:19.43276,
                        uuid:53
                    },{
                        ShopName:"Shree Mahalaxmi Lottery Bhandar",
                        Address:"Lottery House Building  Shubhas Chowk  Near Mangal Gate Police Chowki",
                        City:"Nandurbar",
                        State:"Maharashtra",
                        Postcode:425412,
                        ContactName:"Mr.Mahesh Laxmandas Jagyasi",
                        ContactNumber:"9822962206",
                        Xcoord:74.24202,
                        Ycoord:21.36607,
                        uuid:54
                    },{
                        ShopName:"Chirag Enterprises",
                        Address:"158 1st Floor  Sarda Sankul  Vakil Wadi  M.G.Road  Near Panchwati Hotel",
                        City:"Nasik",
                        State:"Maharashtra",
                        Postcode:422101,
                        ContactName:"Mr.Sashikant/Mr.Bablu",
                        ContactNumber:"9665034532",
                        Xcoord:73.7898,
                        Ycoord:19.99745,
                        uuid:55
                    },{
                        ShopName:"Shree Ganeshkrupa Agency",
                        Address:"Shop No.1  Janta Market  Sector - 3  Near Nerul Bus Depot",
                        City:"Nerul (E)",
                        State:"Maharashtra",
                        Postcode:400706,
                        ContactName:"Mr.Sunil/Mr.Mahadev",
                        ContactNumber:"9833213447",
                        Xcoord:73.02228,
                        Ycoord:19.03497,
                        uuid:56
                    },{
                        ShopName:"Mangalmurti Lottery Center",
                        Address:"29 Bhaji Market",
                        City:"Panvel",
                        State:"Maharashtra",
                        Postcode:410206,
                        ContactName:"Mr.Hemant Rameshchandra Agrawal",
                        ContactNumber:"9322839323",
                        Xcoord:73.11752,
                        Ycoord:18.9894,
                        uuid:57
                    },{
                        ShopName:"R.M.Gundecha",
                        Address:"Shree Gurudev Datta Lottery Bank 686  Shukrawar Peth  Opp.Shreenath Talkies  Shivaji Road",
                        City:"Pune",
                        State:"Maharashtra",
                        Postcode:411002,
                        ContactName:"Mr.Dilipbhai/Mr.Mamaji",
                        ContactNumber:"9689400843",
                        Xcoord:73.85801,
                        Ycoord:18.50878,
                        uuid:58
                    },{
                        ShopName:"Bhagyoday Lottery",
                        Address:"Ratandeep Appartment  Behind Hotel Prabha  Near Vitthal Mandir",
                        City:"Ratnagiri",
                        State:"Maharashtra",
                        Postcode:415612,
                        ContactName:"Mr.Pavankumar/Vijaykumar/Mr.Parasmal",
                        ContactNumber:"9420525544",
                        Xcoord:73.31202,
                        Ycoord:16.99022,
                        uuid:59
                    },{
                        ShopName:"Bhagyoday Lottery Centre",
                        Address:"Bhagyoday Group  Rajwada Chowk",
                        City:"Sangli",
                        State:"Maharashtra",
                        Postcode:416416,
                        ContactName:"Mr.Prakash /Mr.Omkar",
                        ContactNumber:"9511555171",
                        Xcoord:74.56417,
                        Ycoord:16.85438,
                        uuid:60
                    },{
                        ShopName:"Yash Online",
                        Address:"C/o.Mangal Zerox  M.G.Road  Opp.Vishal Jewellers  Near Railway Station",
                        City:"Santacruz(W)",
                        State:"Maharashtra",
                        Postcode:400054,
                        ContactName:"Mr.Lenish Chunilal Vira",
                        ContactNumber:"9821242884",
                        Xcoord:72.83768,
                        Ycoord:19.08151,
                        uuid:61
                    },{
                        ShopName:"Avishkar Restaurant",
                        Address:"Shop No.9  Basant Court Building  Opp.Railway Station",
                        City:"Sion(E)",
                        State:"Maharashtra",
                        Postcode:400022,
                        ContactName:"Mr.Padmanabh Poonja",
                        ContactNumber:"9867768494",
                        Xcoord:72.87222,
                        Ycoord:19.04145,
                        uuid:62
                    },{
                        ShopName:"Hitesh Stores",
                        Address:"Shop No.9/10  Sion Fish Market",
                        City:"Sion(E)",
                        State:"Maharashtra",
                        Postcode:400022,
                        ContactName:"Mr.Hitesh Mulchand Gala",
                        ContactNumber:"9820597653",
                        Xcoord:72.87222,
                        Ycoord:19.04145,
                        uuid:63
                    },{
                        ShopName:"Neelam Collection",
                        Address:"275/1  Gope Nivas  Near I.O.B.Bank",
                        City:"Sion(E)",
                        State:"Maharashtra",
                        Postcode:400022,
                        ContactName:"Mr.Vinod/Mr.Bharat",
                        ContactNumber:"9870481297",
                        Xcoord:72.87222,
                        Ycoord:19.04145,
                        uuid:64
                    },{
                        ShopName:"Gavdevi Lottery Centre",
                        Address:"Near Gavdevi Temple  Gokhale Road  Naupada",
                        City:"Thane(W)",
                        State:"Maharashtra",
                        Postcode:400601,
                        ContactName:"Mr.Bhupat Valjibhai Gantra",
                        ContactNumber:"9892584415",
                        Xcoord:72.97442,
                        Ycoord:19.18805,
                        uuid:65
                    },{
                        ShopName:"Hotel Veggies",
                        Address:"Shop No.2  Sumer Castle  Castle Mill Compound  Near Vikas Complex  L.B.S.Marg",
                        City:"Thane(W)",
                        State:"Maharashtra",
                        Postcode:400601,
                        ContactName:"Mr.Bhupat /Mr.Kapil/Mr.Vinit/Mr.Bablu",
                        ContactNumber:"9892584415",
                        Xcoord:72.97726,
                        Ycoord:19.22513,
                        uuid:66
                    },{
                        ShopName:"Jai Ganesh Online",
                        Address:"Gala No.3  Devdaya Nagar Road  Sahakar Nagar",
                        City:"Thane(W)",
                        State:"Maharashtra",
                        Postcode:400606,
                        ContactName:"Mr.Satish Gangaram Rathod",
                        ContactNumber:"9821137020",
                        Xcoord:72.95491,
                        Ycoord:19.22756,
                        uuid:67
                    },{
                        ShopName:"Momaima Lottery Center",
                        Address:"Near Ashok Talkies  Station Road",
                        City:"Thane(W)",
                        State:"Maharashtra",
                        Postcode:400601,
                        ContactName:"Mr.Mahesh Madhavji Thakker",
                        ContactNumber:"9930633350",
                        Xcoord:72.97547,
                        Ycoord:19.18782,
                        uuid:68
                    },{
                        ShopName:"Divya Global X",
                        Address:"Plot No. 7 Shop No.12  Sector-19D  Opp. APMC Market-II  Dana Bunder Gate No.- 1",
                        City:"Vashi",
                        State:"Maharashtra",
                        Postcode:400705,
                        ContactName:"Mr.Nilesh/Mr.Ashok",
                        ContactNumber:"9867626336",
                        Xcoord:73.00003,
                        Ycoord:19.07936,
                        uuid:69
                    },{
                        ShopName:"Family Online Lottery ",
                        Address:"Shop No.3  Baba Godekar Chawl  Station Road",
                        City:"Vikhroli[E]",
                        State:"Maharashtra",
                        Postcode:400083,
                        ContactName:"Mr.Jayesh Mahendra Dedhia",
                        ContactNumber:"8080651913",
                        Xcoord:72.93098,
                        Ycoord:19.11293,
                        uuid:70
                    },{
                        ShopName:"Dwarka Lottery ",
                        Address:"Below Canara Bank  Station Road",
                        City:"Vikhroli[W]",
                        State:"Maharashtra",
                        Postcode:400083,
                        ContactName:"Mr.Babli/Parsuhram",
                        ContactNumber:"9768312282",
                        Xcoord:72.92672,
                        Ycoord:19.11105,
                        uuid:71
                    },{
                        ShopName:"Siddharth Lottery Centre",
                        Address:"Beside West End Store  Near Vikhroli level Crossing",
                        City:"Vikhroli[W]",
                        State:"Maharashtra",
                        Postcode:400083,
                        ContactName:"Mr.Shankar /Mr.Babli/Parsuhram",
                        ContactNumber:"9768312282",
                        Xcoord:72.91948,
                        Ycoord:19.10918,
                        uuid:72
                    },{
                        ShopName:"Lucky Playland",
                        Address:"Shop No.5  Zaveri Building  D.J.Road  Near Railway Station",
                        City:"Vileparle[W]",
                        State:"Maharashtra",
                        Postcode:400056,
                        ContactName:"Miss.Lata",
                        ContactNumber:"9819923616",
                        Xcoord:72.83678,
                        Ycoord:19.10704,
                        uuid:73
                    },{
                        ShopName:"Plus Point",
                        Address:"Shop No-956  Katrak Road  Opp.Pickup Wine Shop  Near Bank of India",
                        City:"Wadala[W]",
                        State:"Maharashtra",
                        Postcode:400031,
                        ContactName:"Mr.Kapil /Mr.Kiran",
                        ContactNumber:"9820654767",
                        Xcoord:72.85819,
                        Ycoord:19.01211,
                        uuid:74
                    },{
                        ShopName:"Abhinandan Lottery",
                        Address:"Shop No.7  Transit Camp  Belani Nagar  Worli Village  Near Bus No.44/162 Last Stop",
                        City:"Worli",
                        State:"Maharashtra",
                        Postcode:400030,
                        ContactName:"Mr.Gopal Khamitkar",
                        ContactNumber:"9819995764",
                        Xcoord:72.82053,
                        Ycoord:19.01211,
                        uuid:75
                    },{
                        ShopName:"Rushabh Collection",
                        Address:"Shop No.10  258  Manjrekar Sada  Near Century Bazaar  Opp. Kharude Market",
                        City:"Worli",
                        State:"Maharashtra",
                        Postcode:400030,
                        ContactName:"Mr.Nitin /Mr.Parin",
                        ContactNumber:"9819656580",
                        Xcoord:72.82053,
                        Ycoord:19.01211,
                        uuid:76
                    },{
                        ShopName:"OM SAIRAM",
                        Address:"H NO M-83 HOUSING BOARD COLONY PENHA DE FRANCA BARDEZ TALUK ALTO ",
                        City:"Porvorim",
                        State:"Goa",
                        Postcode:403521,
                        ContactName:"Ms. Roopa",
                        ContactNumber:"9960997657",
                        Xcoord:73.83455,
                        Ycoord:15.5209,
                        uuid:76
                    }]};


            $(document).ready(function(){
                var listItems= "<option value=''>" + "Select State" + "</option>";
                var arr = [];
                
                /*for (var i = 0; i < shopsData.shopData.length; i++){
                    arr[] = [shopsData.shopData[i].State]
                    //listItems=listItems+"<option value='" + shopsData.shopData[i].State + "'>" + shopsData.shopData[i].State + "</option>";
                } */
        
        
                $.each(shopsData.shopData , function(i,v){
                    if(arr[v.State]){
                        return true;
                    }
                    arr[v.State] = v.State;
                    listItems=listItems+"<option value='" + v.State + "'>" + v.State + "</option>";
                });
                
                $("#select-state").html(listItems);
                  $("#select-state").val("");
                                   $('select#select-state').selectmenu('refresh');
 $("#select-city").val("");
                                   $('select#select-city').selectmenu('refresh');
            });  
            
          
                 
            
            $(document).on( "change","#select-state", function() {
            
                var listItems= "<option value=''>" + "Select City" + "</option>";
                var a=$("#states").val();   
                var arr1 = [];
               
                for (var i = 0; i < shopsData.shopData.length; i++){
                   
                    //arr1[shopsData.shopData[i].City] = shopsData.shopData[i].City;
                    if(this.value==shopsData.shopData[i].State){
                                   
                        listItems=listItems+"<option value='" + shopsData.shopData[i].City + "'>" + shopsData.shopData[i].City + "</option>";
                    }
                }
                $("#select-city").html(listItems);
               
            });
            
            $(document).on( "change","#select-city", function() {
            
                var str="";
                var i;
           
                
                for (i = 0; i < shopsData.shopData.length; i++) {
                    
                
                    if(this.value==shopsData.shopData[i].City){
                  
                        str=str+"<li style='list-style:none;; margin-top:15px'>"+
                            "<h2>"+shopsData.shopData[i].ShopName+"</h2>"+
                            "<p>"+shopsData.shopData[i].Address+"</p>"+
                            "<p>"+shopsData.shopData[i].City+"</p>"+
                            "<p>"+shopsData.shopData[i].State+"</p>"+
                            "<p>Contact: "+shopsData.shopData[i].ContactName+"-"+shopsData.shopData[i].ContactNumber+"</p>"+
                            "</li>";
                    } else{
                        // alert("Something is wrong");
                    }
                   
                    $("#shopLocation").html(str);
                };
               
                
            });
              $(document).on("pageinit", "#mypage56", function () {
                  $(document).on("click", ".back_head", function () {
    
                    document.location.replace(this.href);
                    return false;
                });
                  
              });
        </script>

        <div>

            <div data-role="page" id="mypage56">
<!--                <div data-role="header" data-theme="a">
                    <h2>Shop Locator
                    </h2>
                    <a href="Settings.php" class="back_head" data-icon="back" data-iconpos="notext"></a>
                </div>-->
                 <?php include("../include/header.php"); ?>
                <div data-role="content" data-theme="a" id="searchPickPlace">
                    <form>
                        <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">

                            <label for="select-native-14">Select A</label>
                            <select name="select-native-14" id="select-state">


                            </select>
                            <label for="select-native-15">Select B</label>
                            <select name="select-native-15" id="select-city">
                                <option value="">Select City</option>
                            </select>

                        </fieldset>
                    </form>
                    <ul id="shopLocation">


                    </ul>

                </div>
                <!-- footer -->
       <?php include("../include/footer.php"); ?>
            </div>


        </div>
    </body>
</html>
