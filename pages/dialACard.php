<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dial A Card</title>


        <link rel="stylesheet" href="../themes/plwin.min.css" />
        <link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
        <link rel="stylesheet" href="../css/common.css"/>
    <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />
    <script src="../js/blinktext.js"></script>

        <!--		<script src="http://code.jquery.com/jquery-1.10.2.min.js" ></script>-->
                <script src="../js/jquery.js" ></script>
<!--		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>-->
                <script src="../js/jquery.mobile-1.4.0.min.js"></script>
       
         <script src="../js/playwin.config.js" ></script>
         <script src="../js/blinktext.js"></script>
    </head>
    <body>
        <script>
            $(document).on("pageinit", "#dial", function () {
                $(document).on("click", ".back_head", function () {
    
                    document.location.replace(this.href);
                    return false;
                });
            });
        </script>
<div data-role="page" id="dial">
  <!-- header -->
  <div data-role="header" data-position="fixed" >
    <h1>Dial A Card </h1>
    <a href="Settings.php" class="back_head" data-icon="back" data-iconpos="notext"></a>
    <div class="ticker">
      <div class="blinking" ></div>
    </div>
  </div>
  <div data-role="content" data-theme="a" id="searchPickPlace">
    <div  data-bind="dxScrollView: { }">
      <h1>HOME DELIVERY</h1>
      <p>Call us on the following numbers, provide us with your detailed address & telephone number.<br/>
        + 91(22) 24721144</p>
      <h1>SMS</h1>
      <p>Just Sms MPHONE to 57575, a representative will get in touch with you at the earliest.</p>
      <h1>EMAIL</h1>
      <p>Your Myplaywin.Com card is just an email away, email us at help@myplaywin.com card, provide us with your detailed address & telephone number. </p>
    </div>
  </div>
  <!-- footer -->
  <?php include("../include/footer.php"); ?>
</div>
</body>
</html>
