<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="author" content="Playwin">
    <meta name="apple-itunes-app" content="app-id=845813290">
    
    <meta name="msApplication-ID" content="App" />
    <meta name="msApplication-PackageFamilyName" content="b5c6d8c9-8008-472a-9155-89a1fe6dceba" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="../css/jquery.smartbanner.css" type="text/css" media="screen">
    <meta name="msapplication-TileImage" content="../content/images/fav_icon.png" />
    <title>Playwin Mobile</title>
    <link rel="stylesheet" href="../themes/plwin.min.css" />
    <link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
    <link rel="stylesheet" href="../css/common.css"/>
    <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />
   
    <script src="../js/jquery.js" ></script>
    <script src="../js/jquery.mobile-1.4.0.min.js"></script>
    <script src="../js/xml2json.js?1"></script>
    <script src="../js/playwin.core.js" ></script> 
    <script src="../js/playwin.config.js" ></script>   
    <script src="../js/blinktext.js"></script>
    
</head>
<body class="bgNew">
	<script src="../js/jquery.smartbanner.js"></script>
   	<script type="text/javascript">
        $(function () { $.smartbanner({ daysHidden: 0, daysReminder: 0, title:'Playwin' }) })
    </script>
	<script type= "text/javascript">
            $(document).on("pageinit", "#home_new", function () {
                    
                $( "#otpPopUp" ).popup();                
                $(document).on("click", ".home_page", function () {
                    if(this.id != "myAcc"){
                        window.location.replace(this.href);
                        return false;
                    }                    
                    
                });
                
                //mobisite/pages
                Playwin.core.setBannerImage("home");       
            }
        );
        </script>
<div data-role="page" data-theme="a" id="home_new">

 
  <?php include("../include/header.php"); ?>
    
    
  <div data-role="content" data-theme="a">
    <div class="form " >
      <div class="home-splash">
        <div>
          <div class="home-dx-logo">
            
          </div>
        </div>
        <div class="home-demo-logo"></div>
        <div style="text-align:center"> 
          <div style="margin: 2% auto 0px; width: 280px;">  
          <a class="ui-btn-a home_page  but2" id="tadayLotto" href="gamesList.php#today" data-role="button" data-inline="true" data-theme="a"><img src="../content/images/todayDraw.png"  align="absmiddle" style="margin-right:5px" /> 
          Today's Draw/s</a> 
            <a class="ui-btn-a home_page but2"  id="dailyLotto" href="gamesList.php#daily" data-role="button" data-inline="true" data-theme="a" ><img src="../content/images/game.png"  align="absmiddle" style="margin-right:5px" /> 
          Lotto Games</a> 
            <a class="ui-btn-a home_page  but2"  id="weeklyLotto" href="gamesList.php#weekly" data-role="button" data-inline="true" data-theme="a"><img src="../content/images/game.png" align="absmiddle" style="margin-right:5px" />
          Weekly Games</a> 
            <a class="ui-btn-a home_page  but2" id="result" href="gameResult.php" data-role="button" data-inline="true" data-theme="a"><img src="../content/images/resultN.png"  align="absmiddle" style="margin-right:5px" /> 
          All  Results</a>
            <a class="ui-btn-a home_page  but2" id="about" href="AboutPlaywin.php" data-role="button" data-inline="true" data-theme="a"><img src="../content/images/about.png"  align="absmiddle" style="margin-right:5px" /> 
          About Playwin</a> 
            <a class="ui-btn-a home_page  but2" id="settings" href="Settings.php" data-role="button" data-inline="true" data-theme="a"><img src="../content/images/friend.png"  align="absmiddle" style="margin-right:5px" /> 
          MyPlaywin Card</a> 
            <a class="ui-btn-a home_page  but2" id="myAcc" href="https://www.itzcash.com/myplaywin/jsp/Login.jsp?displaymode=PLY" target="_blank" data-role="button" data-inline="true" data-theme="a"><img src="../content/images/myAcc.png"  align="absmiddle" style="margin-right:5px" /> 
          My Account</a> 
            <a class="ui-btn-a  but2" href="#otpPopUp" data-rel="popup" data-position-to="window"  data-role="button" data-inline="true"  data-theme="a"><img src="../content/images/social.png"  align="absmiddle" style="margin-right:5px" /> 
          Social Connect</a> <br>
          <a href="#" class="home" data-position-to="window"  data-inline="true"  data-theme="a"><img class="home" id="ad_image" src=""  align="absmiddle" style="margin-right:5px" /> 
          </a> 
          </div>
        </div>
      </div>
    </div>
  </div>
   <!-- footer -->
       <?php include("../include/footer.php"); ?>
  <div data-role="popup" id="otpPopUp" class="ui-content" style="height: 50px">
    <div style="height: 20%"> 
        <a target="_blank" class="youTube" href="http://www.youtube.com/myplaywin1234" > </a> 
        <a target="_blank" class="twitter" href="https://twitter.com/myplaywin" > </a> 
        <a target="_blank" class="facebook" href="https://www.facebook.com/playwin" > </a> 
        <a target="_blank" class="blogger" href="http://hopevsmoney.blogspot.com" > </a> 
    </div>
    <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a> </div>
</div>
</body>
</html>
