<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">    <title>Playwin Mobile</title>
<link rel="stylesheet" href="../themes/plwin.min.css" />
<link rel="stylesheet" href="../css/common.css" />
<link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
    <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />


    
<!--		<script src="http://code.jquery.com/jquery-1.10.2.min.js" ></script>-->
                <script src="../js/jquery.js" ></script>
<!--		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>-->
                <script src="../js/jquery.mobile-1.4.0.min.js"></script>
<script src="../js/xml2json.js?1"></script>
<script src="../js/fullcalendar.min.js?1"></script>
<script src="../js/playwin.core.js" ></script>
<script src="../js/playwin.config.js" ></script>
<script src="../js/playwin.play.js" ></script>
<script src="../js/blinktext.js"></script>

</head>
<body>
<style >
            .dx-button2 {
                background-color:#226494;
                color:#fff;
                border:0;
                padding:5px;
                -webkit-border-radius:3px;
                -moz-border-radius:3px;
                -ms-border-radius:3px;
                -o-border-radius:3px;
                border-radius:3px;
                float:left;
                margin:5px;
                font-size:90%;
                text-shadow:0 1px rgba(0, 0, 0, 0.6);
                cursor:pointer;
                text-align:center
            }
            .prizeStructure {
                margin:10px
            }
            .panel, .prizeStructure table {
                width:100%
            }
            .panel th, .prizeStructure th {
                font-weight:bold;
                font-size:90%;
                color:#226494;
                text-align:center;
                height:26px;
                background-color:#bad7ed;
                text-shadow:0 1px rgba(255, 255, 255, 0.6)
            }
            .panel td, .prizeStructure td {
                font-size:90%;
                color:#226494;
                background-color:#e4e6e8;
                cursor:pointer
            }
            .prizeStructure td {
                text-align:center
            }
            /*#pnlDetails .textbox {
                    color:#226494;
                    -webkit-box-shadow:2px 2px 2px 0 #6d6a66;
                    -moz-box-shadow:2px 2px 2px 0 #6d6a66;
                    -ms-box-shadow:2px 2px 2px 0 #6d6a66;
                    -o-box-shadow:2px 2px 2px 0 #6d6a66;
                    box-shadow:2px 2px 2px 0 #6d6a66;
                    border:0;
                    background:#fff;
                    padding:4px 4px 4px 6px;
                    font-size:1em;
                    margin:0;
                    margin-bottom:10px;
                    resize:none;
                    font-family:inherit
            }*/
            .resultTable {
                margin:35px 20px 20px;
                clear:both
            }
            .resultData {
                color:#226494;
                font-weight:bold;
                float:left
            }
            .resultTitle {
                color:#24292c;
                float:left;
                padding-right:10px
            }
            .resultRow {
                clear:both
            }
            .deleteIcon {
                background:url(/content/images/trash.png) no-repeat;
                width:16px;
                height:16px
            }
            .aboutGame {
                margin:10px;
                clear:both
            }
            .aboutGame ul li {
                list-style:outside
            }
            .gameBallsUl {
                cursor:pointer;
                width:100%;
                padding:10px
            }
            .question {
                font-weight:bold;
                cursor:pointer;
                margin-top:20px
            }
            .answer {
                margin-top:5px;
                font-size:90%
            }
            .faq {
                padding-left:15px
            }

        </style>
<div data-role="page" id="playGamePage" data-theme="a" style="padding-top:65px;">
<!--  <div data-role="header" data-position="fixed">
    <h1 id="main_heading"></h1>
    <a href="home.php" class="back_head" data-icon="back" data-iconpos="notext"></a>
    <div class="ticker">
    <div class="blinking" ></div>
    </div>
  </div>-->
    <?php include("../include/header.php"); ?>
  <div data-role="content" data-theme="a">
    <!--				<p>Your theme was successfully downloaded. You can use this page as a reference for how to link it up!</p>
                                                <pre>
                <strong>&lt;link rel=&quot;stylesheet&quot; href=&quot;themes/plwin.min.css&quot; /&gt;</strong>
                &lt;link rel=&quot;stylesheet&quot; href=&quot;http://code.jquery.com/mobile/1.4.0/jquery.mobile.structure-1.4.0.min.css&quot; /&gt;
                &lt;script src=&quot;http://code.jquery.com/jquery-1.10.2.min.js&quot;&gt;&lt;/script&gt;
                &lt;script src=&quot;http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js&quot;&gt;&lt;/script&gt;
                                                </pre>
                                                <p>This is content color swatch "A" and a preview of a <a href="#" class="ui-link">link</a>.</p>
                                                <label for="slider1">Input slider:</label>
                                                <input type="range" name="slider1" id="slider1" value="50" min="0" max="100" data-theme="a" />
                                                <fieldset data-role="controlgroup"  data-type="horizontal" data-role="fieldcontain">
                                                <legend>Cache settings:</legend>
                                                <input type="radio" name="radio-choice-a1" id="radio-choice-a1" value="on" checked="checked" />
                                                <label for="radio-choice-a1">On</label>
                                                <input type="radio" name="radio-choice-a1" id="radio-choice-b1" value="off"  />
                                                <label for="radio-choice-b1">Off</label>
                                                </fieldset>-->
    <div>
      <script type= "text/javascript">
                        var e=new Date();
                        if(!(e.getHours()>=6&&e.getHours()<=21)){
                            alert("The panel is closed. Gaming allowed from 07:00 AM to 10:00 PM.");
                            window.location.replace("home.php");
                        }
                                                
                        $(function() {
                            
                             
                            $(document).on("click", ".back_head", function () {
                                document.location.replace(this.href);
                                return false;
                            });
                
                            $(document).on('click','.gameplay', function(){
                                       
                                       var e=new Date();
                                        if(!(e.getHours()>=6&&e.getHours()<=21)){
                                            alert("The panel is closed. Gaming allowed from 07:00 AM to 10:00 PM.");
                                            return;
                                        }
        
                                        
                                       var id = this.id;
                                       var arr = id.split("_");
                                       
                                       window.location.replace("playGame.php#"+arr[1]);
                                       window.location.reload();
                                       return false;
                                        
                          });
                          
                          $(document).on('click','.drawSelTab', function(){
                              if($(this).attr("id") == "tab1"){
                                   
                                    $("#div2").hide();
                                    $("#div1").show()
                                    
                                    $("#tab1").addClass("sel");
                                    $("#tab2").removeClass("sel");
                                    
                                    if(parseInt($('select#select-no-draw').val()) > 0){
                                        $('select#select-no-draw').val('1');
                                        $('select#select-no-draw').selectmenu('refresh');
                                    }
                                    
                                    /*var val=$("#draw_options").val();
                                    var arr=val.split("_",3);
                                    var DrawID=arr[2];
                                    $("#all_draws").val(DrawID);
                                    $("#no_draw").val("1");
                                    var sum=Playwin.play.calculateTotalBetAmount();*/
                              }else{
                                    $("#tab2").addClass("sel");
                                    $("#tab1").removeClass("sel");
                                    
                                     $("#div2").show();
                                    $("#div1").hide();
                              }
                              
                          });
                          
                          
                            $(document).on("change", ".lprow", function() {
                                var id = this.id;
                                var idArr = id.split("_");
                                // $("#row_"+idArr[2]).remove();
                               //alert(idArr.toSource());
                                if (this.checked) {
                                    var strOpt = "<option value='0'>LP</option>";
                                    $("." + idArr[1] + idArr[2]).html(strOpt);
                                    $("." + idArr[1] + idArr[2]).prop("disabled", true);
                                } else {
                                    $("." + idArr[1] + idArr[2]).prop("disabled", false);
                                    var isd = Playwin.play.getGameBalls(Playwin.play.selectedGame,false);
                                    Playwin.play.resetLottoBox(isd.length,idArr[2]);
                                }
                                $("." + idArr[1] + idArr[2]).prop("value", "0");
                                //return false;
                            });
                            //$.mobile.page.prototype.options.domCache = false;
                            var hash = window.location.hash;
                            
                            var  GameIDHash = hash.substring(1, hash.length);
                            
                            var  GameIDArr = GameIDHash.split("&");
                            GameID = GameIDArr[0];
                            if(!GameID || GameID == ""){
                                alert("Invalid Game Id.");
                                window.location.replace("home.php");
                                return;
                                
                            }
                            Playwin.play.selectedGame = GameID
                            var ginfo ;
                            var firstDrawId ;
                            var cDraw ;
                            var cDrawName ;
                            var gameName ;

                            var nDraw ;
                            var availableDraws ;

                            $.get(Playwin.config.urls.gameDetails)
                            .done(function (xmlData) {
                                var jsonObj = $.xml2json(xmlData);

                                // Remove previously set game draws 
                                $.each(Playwin.config.gameList, function(i, val) {
                                    var g ;
                                    g = "Playwin.config.gameDetails." + "g" + val.id;
                                    var gid ;
                                    gid = "Playwin.config.gameDetails." + "g" + val.id + ".draws";
                                    if (eval(g) && eval(gid)) {
                                        eval(gid).splice(0, eval(gid).length);
                                    }
                                });

                                //push new draws
                                $.each(jsonObj.Draw, function(i, val) {
                                    var g ;
                                    g = "Playwin.config.gameDetails." + "g" + val.GameID;
                                    var gid ;
                                    gid = "Playwin.config.gameDetails." + "g" + val.GameID + ".draws";

                                    //eval(gid).splice(0, eval(gid).length);

                                    if (eval(g) && eval(gid)) {
                                        //alert(eval(gid).toSource());
                                        eval(gid).push(val);

                                    }
                                });
                                var gDraws = eval("Playwin.config.gameDetails." + "g" + GameID + ".draws");

                                //$( 'page' ).live( 'pageinit',function(event){

                                var htmlStr1 = "";
                                var htmlStr2 = "";
                                var cnt = 1;
                                htmlStr1 = "<option value=''>No Of Draw</option>";
                                htmlStr2 = "<option value=''>Draw date</option>";
                                
                                
                                $.each(gDraws,function(i,v){//set no of draw html
                                    /* id="select-no-draw" 
                                                      <option value="">Draw date </option>
                                                   id="select-all-draw"*/
                                    //v.DrawID
                                    htmlStr1 = htmlStr1+"<option value='"+cnt+"'>"+cnt+"</option>";
                                    htmlStr2 = htmlStr2+"<option value='"+(i+1)+"'>"+ Playwin.core.getFormatedDateTime(v.DrawTime,'dddd, dd-MMM-yyyy hh:mm TT')+"</option>";
                                    cnt++;
                                });
                                $("#select-no-draw").html(htmlStr1);

                                $("#select-all-draw").html(htmlStr2);


                                //});

                                //json = jsonObj; 
                                    
                                ginfo = eval("Playwin.config.gameDetails." + "g" + GameID) ;
                                firstDrawId = ginfo.draws[0].DrawID;
                                cDraw = ginfo.name + "( " + ginfo.amt + " )";
                                cDrawName = ginfo.name;
                                gameName =  ginfo.name ;//'Playing ' +

                                nDraw = Playwin.core.getFormatedDateTime(ginfo.draws[0].DrawTime, "dddd, dd-MMM-yyyy hh:mm TT");
                                availableDraws = ginfo.draws;
                                
                                 $("#main_heading_page").html(gameName);
                                $("#main_heading").html(gameName);
                                //$("#c_draw").html(ginfo.name + "( " + game_details.amt + " )");
                                //$("#c_draw_name").html(game_details.name);
                                /*Playwin.play.jsonDetails.currJackpot.id = Playwin.play.selectedGame;
                                                Playwin.play.jsonDetails.currJackpot.name = ginfo.name;
                                                Playwin.play.jsonDetails.currJackpot.amt = ginfo.amt;
                                                Playwin.play.jsonDetails.currJackpot.displayName = ginfo.name + "( " + ginfo.amt + " )";
                                                Playwin.play.jsonDetails.nextDraw = Playwin.core.getFormatedDateTime(ginfo.draws[0].DrawTime, "dddd, dd-MMM-yyyy hh:mm TT");
                                                //$("#next_draw").html(jsonDetails.nextDraw);
                                                //alert(ginfo.draws.toSource());
                                                Playwin.play.loadData(params.id);*/
                            })
                            .fail(function () {
                                alert("Error : Game details load Error .");
                            });    
                            
                            $( "#playCardSubmitPopUp" ).popup();
                            $( "#loadPanelMsg" ).popup();
                            $( "#showBalance" ).popup();
                            
                            $( "#betSubmitConfirmDialog" ).popup();
                                                   
                            
                              
                            $(document).on(
                            "click",
                            "#finalBetSubmit",
                            function() {
                                Playwin.play.finalBetSubmit("balance");
                            
                            });
                              
                            $(document).on(
                            "click",
                            "#showOverlayBalance",
                            function() {
                                Playwin.play.initCardSubmit("balance");
                                           
                            });
                            /*$(document).on(
                            "click",
                            "#showMyAccount",
                            function() {
                                window.location.replace("https://www.itzcash.com/myplaywin/jsp/Login.jsp?displaymode=PLY");
                                           
                            });*/
                            $(document)
                            .on(
                            "click",
                            "#play_game_not_keno",
                            function() {
                                Playwin.play.gameRowConfirm(10);
                                //Playwin.play.initCardSubmit("play");
                            });
                             
                            $(document)
                            .on(
                            "click",
                            "#play_game",
                            function() {

                                /*if( $("#select-all-draw").val() == "" && $("#select-no-draw").val() == ""){
                                                        alert("Please select No of draw OR Draw date !");
                                                        return false;
                                                }

                                                var res = Playwin.play.validateTickets(Playwin.play.selectedGame);
                                                // alert(res.toSource());
                                                if( res.status == false) {
                                                    if (res.dup_rows && res.dup_rows.length > 0) {
                                                        alert("Error:Bet Details ! Ticket "
                                                            + res.dup_rows.join()
                                                            + " has duplicate values !");

                                                    } else if (res.notinrange_rows && res.notinrange_rows.length > 0) {
                                                        alert("Error:Bet Details ! Values of Ticket "
                                                            + res.notinrange_rows.join()
                                                            + "  are not in range!");

                                                    }else{
                                                        alert(res.msg);
                                                    }

                                                    return false;
                                                }
                                                Playwin.play.jsonDetails.draw.id = Playwin.play.selectedGame == "9" ? "0"
                                                : $("#select-all-draw").val();
                                                Playwin.play.jsonDetails.draw.name = Playwin.play.selectedGame == "9" ? ""
                                                : $("option:selected", $("#select-all-draw")).text();
                                                Playwin.play.jsonDetails.noOfDraw = Playwin.play.selectedGame == "9" ? "0"
                                                : $("#select-no-draw").val();
                                                // Playwin.play.jsonDetails.tickets.push();

                                                //Playwin.play.jsonDetails.totalTicketPrice = Playwin.play.jsonDetails.ticketPrice * res.tickets.length ;

                                                //Playwin.play.jsonDetails.ticketPrice = game_details.ticketPrice;
                                                var str = $('input.ticket_price').map(function() {
                                                    return this.value;
                                                })
                                                .get().join("+");
                                                //alert(str);
                                                Playwin.play.jsonDetails.totalTicketPrice = eval(str);



                                                Playwin.play.jsonDetails.tickets = JSON.parse(JSON.stringify(res.tickets));
                                                alert(JSON.stringify(Playwin.play.jsonDetails));*/
                                // open playCard submit model
                                Playwin.play.initCardSubmit("play");
                                //AppNamespace.app.navigate("playCardSubmit");

                                //window.location = "play_card_submit.html";

                            });
                                            
                            $(document)
                            .on(
                            "click",
                            "#submit_card",
                            function() {
                                var type = $("#cardSubmitType").val();
                                if(type == "play"){
                                    Playwin.play.go(Playwin.play.selectedGame);
                                }else if(type == "balance"){
                                    Playwin.play.showOverlayBalance();
                                }
                                                   
                                /*var no = $("#cardNo").val();
                                                        var pass = $("#pinNo").val();
                                                        if (!no || $.trim(no) == "") {
                                                                alert("Card Number can not be empty .");
                                                                return false;
                                                        } else if (!pass || $.trim(pass) == "") {
                                                                alert("Card Pin can not be empty .");
                                                                return false;
                                                        } else if (!confirm("Amount of Rs. "+Playwin.play.jsonDetails.totalTicketPrice+" will be deducted from your card balance. Do you want to continue ?")) {
                                                                return false;
                                                        }
                                                        var dataStr = "";
                                                        var betStr = "";
                                                        var betArr = [];
                                                        $.each(Playwin.submitCard.ticketsJson.tickets,
                                                                        function(i, val) {
                                                                                betArr.push(val.values.map(function(v) {
                                                                                        return v;
                                                                                }));
                                                                        })
                                                        betStr = betArr.join("|");

                                                        dataStr = "ItzNumber="
                                                                        + $("#cardNo").val()
                                                                        + "&ItzPassword="
                                                                        + $("#pinNo").val()
                                                                        + "&BetString="
                                                                        + betStr
                                                                        + "&GameID="
                                                                        + Playwin.submitCard.ticketsJson.currJackpot.id
                                                                        + "&DrawPointer="
                                                                        + Playwin.submitCard.ticketsJson.draw.id
                                                                        + "&TotalDraw="
                                                                        + Playwin.submitCard.ticketsJson.noOfDraw;
                                                        $.ajax({
                                                                url : Playwin.config.urls.betPlacement,
                                                                method : "POST",
                                                                data : dataStr,
                                                                success : function(xmlData) {
                                                                        var jsonObj = $.xml2json(xmlData);
                                                                        // alert(jsonObj.toSource());
                                                                        if (jsonObj.Status == "-1") {// Error : - if invalid card data
                                                                                alert(jsonObj.StatusText);
                                                                        } else if (jsonObj.Status == "0") {
                                                                                if (jsonObj.Ticket.Status == "35") { // Error :- if invalid panelData
                                                                                        alert(jsonObj.Ticket.StatusText);
                                                                                } else {
                                                                                        // success
                                                                                }
                                                                        }
                                                                },
                                                                complete : function(data) {

                                                                }
                                                        })*/
                            });
                                                
                            $(document)
                            .on(
                            "click",
                            "#keno_row_confirm",
                            function(){ 
                                Playwin.play.kenoRowConfirm();
                            });
                            $(document)
                            .on(
                            "click",
                            "#game_row_confirm",
                            function(){ 
                                Playwin.play.gameRowConfirm(10);
                            });
                                   
                            $("#select-no-draw").on("change",function(){
                                // $("#sum_amt").val("");
                                $("#select-all-draw").val("1");
                                $('select#select-all-draw').selectmenu('refresh');
                                //$('select#select-all-draw option').attr('selected', false);
                                //$('#select-all-draw option[value=0]').prop('selected', true);
                                //$('select#select-all-draw').selectmenu('refresh');
                                Playwin.play.calculateTotalBetAmount();
                            });
                            $("#select-all-draw").on("change",function(){
                                $('select#select-no-draw').val('1');
                                $('select#select-no-draw').selectmenu('refresh');
                                //$('select#select-no-draw').val('0');
                                //$('#select-no-draw option[value=0]').prop('selected', true);
                            });

                            if(GameID == 9){
                                $(".isNotKenoGame").remove();
                                //$("#isNotKenoGame").html("");
                                
                                $(document).on(
                                "change",
                                "#spot_option",
                                function() {
                                    var val = this.value;
                                    Playwin.play.spotChange(val);
                                });
                                
                                $(document).on(
                                "click",
                                "#show_lotto_div",
                                function() {
                                     $("#lotto_table_div").show();
                                     $("#show_lotto_table_div").hide();
                                });
                               
                                $(document)
                                .on(
                                "change",
                                "#amt_option",
                                function() {
                                    // show prize structure table
                                    $("#prize_amt_div").hide();
                                    var tblStr = "<table style='display:none'><thead><td>Match</td><td>Prize</td></thead><tbody id='prize_structure'></tbody></table>";
                                    $("#prize_amt_div").html(tblStr);
                                    var tblBody = "";
                                    var matchPrz = Playwin.play.getPrizeStructure($("#spot_option").val(),$("#amt_option").val());
                                    //alert(matchPrz.toSource());
                                    $.each(matchPrz, function(i, val) {
                                        tblBody = tblBody + "<tr><td>" + val.match
                                            + "</td><td>" + val.prize
                                            + "</td></tr>";
                                    })
                                    $("#prize_structure").html(tblBody);
                                                              
                                });
                             
                                Playwin.play.init(GameID);
                        
                            }else{
                                
                                //json = jsonObj; 
                                
                                //function changeNoOFDraw (ko_object, js_evt){
                                //$("#select-no-draw").on("change",function(){
                                
                                //alert(Playwin.play.selectedGame);
                                
                               
                                var isd = Playwin.play.getGameBalls(Playwin.play.selectedGame,false);
                                // alert(isd.length);
                                // isd = null;
                                
                                Playwin.play.loadLottoBox(isd.length,10);
                                
                                                               
                                
                                
                                $("#add_ticket_row").on("click",function(){
                                    addNewTicketRow();
                                });
                                function addNewTicketRow(){
                                    var id = $("#new_row_id").val();
                                    if ($('#draws_table tr').length > 11) {// add only 10 rows at a time
                                        alert("You can not add more then 10 tickets .");
                                        return false;
                                    }

                                    Playwin.play.addTicketRow(Playwin.play.selectedGame, id);
                                    id = 1 + parseInt(id);
                                    $("#new_row_id").val(id);

                                    // $("#row_id_"+id).val(id);
                                    return false;
                                }

                                /*function setNoDrawList(c,d){
                                               var noDrawList;
                                                noDrawList=[];
                                                //viewPlayGameModel.noOfDraw([]);
                                                $.each(viewPlayGameModel.availableDraws(),function(i,v){
                                                    viewPlayGameModel.noOfDraw.push({
                                                        key:i+1,
                                                        value:i+1
                                                    })
                                                });                           
                                }*/



                                Playwin.play.init(GameID);
                                 
                                
                                //$("#isKenoGame").html("");
                                $(".isKenoGame").remove();
                            }
                                    
                             Playwin.core.setBannerImage("playGameSuccess");
                            
                        });
                    </script>
    </div>
    <!-- plage game html -->
    <div  >
      <div  >
<!--        <h1 id="gameName"></h1>-->
        <style>
                            input.lottotbox{
                                border-radius: 5px 5px 5px 5px;
                                height: 25px;
                                margin: 5px;
                                padding: 2px;
                                width: 25px;
                            }
                            .row_remove #add_row{
                                text-decoration: none;
                            }
                            #err_span {
                                color: red;
                            }
                        </style>
        <div data-role="popup" id="loadPanelMsg" class="ui-content">
          <p>Loading...</p>
        </div>
        <input id="cardSubmitType" type="hidden" value=""/>
        <input id="gameType" type="hidden" value="<?php echo isset($_REQUEST["hash"]) ? $_REQUEST["hash"] : "" ; ?>"/>
        <input id="typePage" type="hidden" value="<?php echo isset($_REQUEST["typePage"]) ? $_REQUEST["typePage"] : "" ; ?>"/>
        <div data-role="popup" id="showBalance" class="ui-content"> <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
          <div>
            <div class="dx-popup-title">Card Details</div>
            <p><span style="font-weight:bold">Remaining Amount:</span><span id="card_balance"></span></p>
            <p><span style="font-weight:bold">Expiry Date:</span><span id="card_expiry"></span></p>
            <p><span style="font-weight:bold">Status:</span><span id="card_status"></span></p>
          </div>
        </div>
        <div data-role="popup" id="playCardSubmitPopUp" data-dismissible='false' class="ui-content" style="height:160px;"> <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
          <div>
           <div class="dx-popup-title">Please Enter Card Details</div>
           <input type="text" name="cardNo" id="cardNo" placeholder="MPC Card No" value="" data-clear-btn="true" size="20" class="ui-shadow-inset"/>
            <input type="password" name="pinNo" id="pinNo" placeholder="Password" value="" data-clear-btn="true" class="ui-shadow-inset"/>
            <div class="dx-button2" id="submit_card"><span class="dx-button-text">Submit</span></div>
          </div>
        </div>
        
        
        
<!--        <a href="#betSubmitConfirmDialog" data-rel="popup" data-position-to="window" data-transition="pop" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-delete ui-btn-icon-left ui-btn-b">Delete page...</a>-->
        <div data-role="popup" id="betSubmitConfirmDialog" data-overlay-theme="b" data-theme="b" data-dismissible="false" style="max-width:400px;">
            <div data-role="header" data-theme="b">
            <h1 id="betSubmitConfirm">Bet Submit Confirm !</h1>
            </div>
            <div role="main" class="ui-content">
                <h3 class="ui-title" id="betSubmitConfirmMsg" style="color:black">Are you sure you want to submit this bet?</h3>
                <a id="finalBetSubmit" href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b" data-rel="back" data-transition="flow">Yes</a>
                <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-b" data-rel="back">No</a>
                
            </div>
        </div>
        
        
        <div data-role="fieldcontain"  style="margin-top:-20px;">
          
          <style type="text/css">
              .Tab {
	background:url("../content/images/tabBG.gif?1") repeat-x scroll left bottom;
	float:left;
	width:100%
}
.Tab ul {
	padding:10px 0 0 10px;
	margin:0;
	height:100%;
	list-style:none outside none
}
.Tab ul li {
	float:left;
	margin-right:5px;
	padding-top:0;
	list-style:none outside none
}
.Tab ul li div {
	padding:3px 10px;
	color:#878787;
	font-size:90%;
	font-weight:bold;
	border-width:2px 1px 0;
	border-style:solid solid none;
	border-color:#c3c3c3 #c3c3c3 -moz-use-text-color;
	-moz-border-top-colors:none;
	-moz-border-right-colors:none;
	-moz-border-bottom-colors:none;
	-moz-border-left-colors:none;
	border-image:none;
	background-image:-webkit-linear-gradient(-90deg, #ddd 0, #eee 100%);
	background-image:-moz-linear-gradient(-90deg, #ddd 0, #eee 100%);
	background-image:-ms-linear-gradient(-90deg, #ddd 0, #eee 100%);
	background-image:-o-linear-gradient(-90deg, #ddd 0, #eee 100%);
	background-image:linear-gradient(180deg, #ddd 0, #eee 100%);
	border-top-left-radius:4px;
	border-top-right-radius:4px;
	margin-top:10px;
	display:inline-block;
	cursor:pointer
}
.Tab div.sel {
	color:#2c2903;
	font-size:100%;
	font-weight:bold;
	padding:8px 10px 7px;
	border-image:none;
	border-color:#de7319 #de7319 -moz-use-text-color;
	background-image:-webkit-linear-gradient(-90deg, #ffec00 0, #eee 100%);
	background-image:-moz-linear-gradient(-90deg, #ffec00 0, #eee 100%);
	background-image:-ms-linear-gradient(-90deg, #ffec00 0, #eee 100%);
	background-image:-o-linear-gradient(-90deg, #ffec00 0, #eee 100%);
	background-image:linear-gradient(180deg, #ffec00 0, #eee 100%);
	font-weight:bold;
	border-top-left-radius:4px;
	border-top-right-radius:4px;
	margin-top:0;
	cursor:pointer
}

.stepSide {
	margin-top:10px;
	width:90%;
	background:url("../content/images/stepSide.png") no-repeat
}
.stepNo {
	background:url("../content/images/step.png") no-repeat;
	height:30px;
	padding:5px 10px;
	float:left
}
.stepTitle {
	padding:5px 0;
	margin-left:40px
}
          </style>
          <div class="Tab">
            <ul>
            <li>
            <div id="tab1" class="sel drawSelTab" >Draw Dates</div>
            </li>
            <li>
            <div id="tab2" class="drawSelTab" >No Of Draws</div>
            </li>
            </ul>
            </div>
          
         
         <div style="clear:both; padding-top:2px">
          <div id="div1" > 
                    <div class="stepSide">
                        <div class="stepNo">1.</div>
                        <div class="stepTitle">Select Draw Date</div>                                        
                    </div>
                    <div class="borderBox">
                          <select name="select-all-draw" id="select-all-draw" data-inline="true">
                            <option value="">Draw date </option>
                          </select>
                    </div>
                    
                </div>
           <div id="div2" style="display: none;"> 
                    <div class="stepSide">
                        <div class="stepNo">1.</div>
                        <div class="stepTitle">Select No. of Draws</div>                                        
                    </div>
                    <div class="borderBox">
                          <select name="select-no-draw" id="select-no-draw" data-inline="true">
                            <option value="">No Of Draw </option>
                          </select>
                    </div>
                    
                </div>
         </div>
          
        </div>
        <div id="isNotKenoGame" class="isNotKenoGame">
          <!-- ----------------------------------- -->
          <!--            <div data-role="fieldcontain">
                                        <label>Current Draw : </label><label id="c_draw" data-bind="text: cDraw"></label>
                                        </div>
                                        <div data-role="fieldcontain">
                                        <label>Next Draw : </label><label id="next_draw" data-bind="text: nDraw"></label>
                                        </div>-->
          <!--             <select id="all_draws"></select> -->
          <!--             <select id="all_draws" data-bind="options: availableDraws" optionsText: 'DrawTime' ></select> -->
          <!--<table>
            <tbody id="spot_inputs">
              <tr>
                <td><input type='hidden' id='new_row_id' name='new_row_id' value='1'/></td>
              </tr>
              <tr id="tr_spot_inputs"></tr>
              <tr id="tr_confirm_inputs"></tr>
            </tbody>
          </table> -->
           <div class="lotto_table_div">
            <div class="lotto_tbody_div" id="spot_inputs">
              <div class="lotto_tr_div">
                <div class="lotto_td_div"><input type='hidden' id='new_row_id' name='new_row_id' value='1'/></div>
              </div>
              
              <div class="lotto_tr_div" id="tr_spot_inputs_1"></div>
              <div class="lotto_tr_div" id="tr_spot_inputs_2"></div>
              <div class="lotto_tr_div" id="tr_spot_inputs_3"></div>
              <div class="lotto_tr_div" id="tr_spot_inputs_4"></div>
              <div class="lotto_tr_div" id="tr_spot_inputs_5"></div>
              <div class="lotto_tr_div" id="tr_spot_inputs_6"></div>
              <div class="lotto_tr_div" id="tr_spot_inputs_7"></div>
              <div class="lotto_tr_div" id="tr_spot_inputs_8"></div>
              <div class="lotto_tr_div" id="tr_spot_inputs_9"></div>
              <div class="lotto_tr_div" id="tr_spot_inputs_10"></div>
              
              
              <div class="lotto_tr_div" id="tr_confirm_inputs"></div>
            </div>
          </div>
          <!--                            <table>
                                                            <tbody >id="draws_table"
                                                                <tr>
                                                                    <td colspan="6">
                                                                        <strong>Tickets</strong>
                                                                    </td>
                                                                    <td>
                                                                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LP</strong>
                                                                    </td>
                                                                    <td>
                                                                        <button id="add_ticket_row" data-bind="click: addNewTicketRow"  title="Add New Ticket">Add</button>
                                                                        <input type="hidden" id="new_row_id" value="2">
                                                                         <a href='#' id="row_add" onclick="" title="Add New Draw"> <strong> Add </strong> </a> 
                                                                    </td>
                                                                </tr>
                                                                                     <tr id="row_1"> 
                            
                                                                                         <td style=""> 
                                                                                             <input id="txtp1t1_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp1" type="text"> 
                                                                                         </td> 
                                                                                         <td style=""> 
                                                                                             <input id="txtp1t2_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp1" type="text"> 
                                                                                         </td> 
                                                                                         <td style=""> 
                                                                                             <input id="txtp1t3_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp1" type="text"> 
                                                                                         </td> 
                                                                                         <td style=""> 
                                                                                             <input id="txtp1t4_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp1" type="text"> 
                                                                                         </td> 
                                                                                         <td style=""> 
                                                                                             <input id="txtp1t5_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp1" type="text"> 
                                                                                         </td> 
                                                                                         <td style=""> 
                                                                                             <input id="txtp1t6_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp1" type="text"> 
                                                                                         </td> 
                                                                                         <td style="" width="45px"> 
                                                                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="lp_lb_1" class="row_lp" type="checkbox"> 
                                                                                         </td> 
                                                                                         <td style="" width="45px" id="row_adrm_1"> 
                                                                                             <input type="hidden" id='row_id_1' name="row_id[]" value="1"/> 
                                                                                             <a class="row_remove" id="row_rm_1" href="javascript:void(0)" title="Remove Draw"> <strong> Del </strong> </a> 
                                                                                         </td> 
                                                                                     </tr> 
                            
                            
                                                                <tr id="row_1">
                                                                    <td style="">
                                                                        <input id="txtp1t_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp11 spot_inputs" type="text">
                                                                    </td>
                                                                    <td style="">
                                                                        <input id="txtp1t_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp11 spot_inputs" type="text">
                                                                    </td>
                                                                    <td style="">
                                                                        <input id="txtp1t_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp11 spot_inputs" type="text">
                                                                    </td><td style="">
                                                                        <input id="txtp1t_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp11 spot_inputs" type="text">
                                                                    </td>
                                                                    <td style="">
                                                                        <input id="txtp1t_grp1" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp11 spot_inputs" type="text">
                                                                    </td>
                                                                    <td style="">
                                                                        <input id="txtp1t_grp2" name="txtp1[]" maxlength="2" class="lottotbox lb1 tp1 grp21 spot_inputs" type="text">
                                                                    </td>
                                                                    <td style="" width="45px">
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="row_lb_1" class="row_lp lprow" type="checkbox">
                                                                    </td>
                                                                    <td style="" id="row_adrm_1" width="45px">
                                                                        <input id="row_id_1" name="row_id[]" value="1" type="hidden">
                                                                        <input id="ticket_price_1" name="ticket_price[]" class="ticket_price" value="10" type="hidden"><a href="#" id="row_rm_1" class="row_remove" title="Remove Draw"> <strong> Del <strong> </strong></strong></a>
                                                                    </td>
                                                                </tr>
                                                                <tr id="row_2"><td style=""><input id="txtp2t_grp1" name="txtp2[]" maxlength="2" class="lottotbox lb2 tp2 grp12" type="text"></td><td style=""><input id="txtp2t_grp1" name="txtp2[]" maxlength="2" class="lottotbox lb2 tp2 grp12" type="text"></td><td style=""><input id="txtp2t_grp1" name="txtp2[]" maxlength="2" class="lottotbox lb2 tp2 grp12" type="text"></td><td style=""><input id="txtp2t_grp1" name="txtp2[]" maxlength="2" class="lottotbox lb2 tp2 grp12" type="text"></td><td style=""><input id="txtp2t_grp1" name="txtp2[]" maxlength="2" class="lottotbox lb2 tp2 grp12" type="text"></td><td style=""><input id="txtp2t_grp2" name="txtp2[]" maxlength="2" class="lottotbox lb2 tp2 grp22" type="text"></td><td style="" width="45px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="row_lb_2" class="row_lp" type="checkbox"></td><td style="" id="row_adrm_2" width="45px"><input id="row_id_2" name="row_id[]" value="2" type="hidden"><input id="ticket_price_2" name="ticket_price[]" class="ticket_price" value="10" type="hidden"><a href="#" id="row_rm_2" class="row_remove" title="Remove Draw"> <strong> Del <strong> </strong></strong></a></td></tr><tr id="row_3"><td style=""><input id="txtp3t_grp1" name="txtp3[]" maxlength="2" class="lottotbox lb3 tp3 grp13" type="text"></td><td style=""><input id="txtp3t_grp1" name="txtp3[]" maxlength="2" class="lottotbox lb3 tp3 grp13" type="text"></td><td style=""><input id="txtp3t_grp1" name="txtp3[]" maxlength="2" class="lottotbox lb3 tp3 grp13" type="text"></td><td style=""><input id="txtp3t_grp1" name="txtp3[]" maxlength="2" class="lottotbox lb3 tp3 grp13" type="text"></td><td style=""><input id="txtp3t_grp1" name="txtp3[]" maxlength="2" class="lottotbox lb3 tp3 grp13" type="text"></td><td style=""><input id="txtp3t_grp2" name="txtp3[]" maxlength="2" class="lottotbox lb3 tp3 grp23" type="text"></td><td style="" width="45px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="row_lb_3" class="row_lp" type="checkbox"></td><td style="" id="row_adrm_3" width="45px"><input id="row_id_3" name="row_id[]" value="3" type="hidden"><input id="ticket_price_3" name="ticket_price[]" class="ticket_price" value="10" type="hidden"><a href="#" id="row_rm_3" class="row_remove" title="Remove Draw"> <strong> Del <strong> </strong></strong></a></td></tr><tr id="row_4"><td style=""><input id="txtp4t_grp1" name="txtp4[]" maxlength="2" class="lottotbox lb4 tp4 grp14" type="text"></td><td style=""><input id="txtp4t_grp1" name="txtp4[]" maxlength="2" class="lottotbox lb4 tp4 grp14" type="text"></td><td style=""><input id="txtp4t_grp1" name="txtp4[]" maxlength="2" class="lottotbox lb4 tp4 grp14" type="text"></td><td style=""><input id="txtp4t_grp1" name="txtp4[]" maxlength="2" class="lottotbox lb4 tp4 grp14" type="text"></td><td style=""><input id="txtp4t_grp1" name="txtp4[]" maxlength="2" class="lottotbox lb4 tp4 grp14" type="text"></td><td style=""><input id="txtp4t_grp2" name="txtp4[]" maxlength="2" class="lottotbox lb4 tp4 grp24" type="text"></td><td style="" width="45px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="row_lb_4" class="row_lp" type="checkbox"></td><td style="" id="row_adrm_4" width="45px"><input id="row_id_4" name="row_id[]" value="4" type="hidden"><input id="ticket_price_4" name="ticket_price[]" class="ticket_price" value="10" type="hidden"><a href="#" id="row_rm_4" class="row_remove" title="Remove Draw"> <strong> Del <strong> </strong></strong></a></td></tr><tr id="row_5"><td style=""><input id="txtp5t_grp1" name="txtp5[]" maxlength="2" class="lottotbox lb5 tp5 grp15" type="text"></td><td style=""><input id="txtp5t_grp1" name="txtp5[]" maxlength="2" class="lottotbox lb5 tp5 grp15" type="text"></td><td style=""><input id="txtp5t_grp1" name="txtp5[]" maxlength="2" class="lottotbox lb5 tp5 grp15" type="text"></td><td style=""><input id="txtp5t_grp1" name="txtp5[]" maxlength="2" class="lottotbox lb5 tp5 grp15" type="text"></td><td style=""><input id="txtp5t_grp1" name="txtp5[]" maxlength="2" class="lottotbox lb5 tp5 grp15" type="text"></td><td style=""><input id="txtp5t_grp2" name="txtp5[]" maxlength="2" class="lottotbox lb5 tp5 grp25" type="text"></td><td style="" width="45px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="row_lb_5" class="row_lp" type="checkbox"></td><td style="" id="row_adrm_5" width="45px"><input id="row_id_5" name="row_id[]" value="5" type="hidden"><input id="ticket_price_5" name="ticket_price[]" class="ticket_price" value="10" type="hidden"><a href="#" id="row_rm_5" class="row_remove" title="Remove Draw"> <strong> Del <strong> </strong></strong></a>
                                                                    </td>
                                                                </tr>
                            
                                                            </tbody>
                                                        </table> -->
          <!--                            <table>
                                                            <tbody>
                                                                <tr>
                                                                    
                                                                    <td ><input type="hidden" class="" id="sum_amt" value="0" /> <input type="button" id="play_game" class="play_button" value="Play"/></td>
                                                                    <td > <input type="button" id="showOverlayBalance" class="play_button" value="Check Balance"/></td>
                                                                    <td > <input type="button" id="showMyAccount" class="play_button" value="View Transaction"/></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>-->
        </div>
        <!-- ----------------------------------- -->
        <!---------------Keno View-------------------->
        <div id="isKenoGame" class="isKenoGame">
          <!--         <h1 data-bind="text: message"></h1> -->
          <style>
                                input.lottotbox{
                                    border-radius: 5px 5px 5px 5px;
                                    height: 25px;
                                    margin: 5px;
                                    padding: 2px;
                                    width: 25px;
                                }
                                .row_remove #add_row{
                                    text-decoration: none;
                                }
                            </style>
          <div>
            <!--                                <label>Spot : </label><select id="spot_option" data-bind="options: spotOptions,event: { change:spotChange}"></select></br> 
                                                                <label>Amount : </label>
                                                                             <select id="amt_option" data-bind="options: amtOptions"></select> 
                                                                <select id="amt_option" data-bind="            		
                                                                        options: amtOptions,
                                                                        optionsText: 'amt',
                                                                        optionsValue: 'id',                   
                                                                        optionsCaption: 'Choose a Amt for Bet...'">
                                                                </select>-->
            <div data-role="fieldcontain">
             		<div class="stepSide">
                        <div class="stepNo">2.</div>
                        <div class="stepTitle">Select No. of Spots &amp; Amount</div>                                        
                    </div>
            
              <select name="spot_option" id="spot_option" data-inline="true">
                <option value="">Spot </option>
                <option value="2">2 </option>
                <option value="3">3 </option>
                <option value="4">4 </option>
                <option value="5">5 </option>
                <option value="6">6 </option>
                <option value="7">7 </option>
                <option value="8">8 </option>
                <option value="9">9 </option>
                <option value="10">10 </option>
              </select>
              <select name="amt_option" id="amt_option" data-inline="true">
                <option value="">Amount </option>
                <option value="10">10 </option>
                <option value="20">20 </option>
                <option value="30">30 </option>
                <option value="40">40 </option>
                <option value="50">50 </option>
                <option value="60">60 </option>
                <option value="70">70 </option>
                <option value="80">80 </option>
                <option value="90">90 </option>
                <option value="100">100 </option>
              </select>
            </div>
           <!-- <table>
              <tbody id="spot_inputs">
                <tr>
                  <td><input type='hidden' id='new_row_id' name='new_row_id' value='1'/></td>
                </tr>
                <tr id="tr_spot_inputs"></tr>
                 <tr id="tr_confirm_inputs"></tr>
              </tbody>
            </table> -->
          <div id="show_lotto_table_div" class="lotto_table_div" style="display:none"><a id="show_lotto_div" >Add Panel</a></div>
          <div id ="lotto_table_div" class="lotto_table_div">
            <div class="lotto_tbody_div" id="spot_inputs">
              <div class="lotto_tr_div">
                <div class="lotto_td_div"><input type='hidden' id='new_row_id' name='new_row_id' value='1'/></div>
              </div>
              <div class="lotto_tr_div" id="tr_spot_inputs_1"></div>
              <div class="lotto_tr_div" id="tr_confirm_inputs"></div>
            </div>
          </div>
              
            <br/>
            <br/>
          </div>
          <div id = "prize_amt_div"></div>
        </div>
        <!-------------------------------------------------------------->
      </div>
        <div id="pnlDetails" class="isNotKenoGame"
                         style=" margin-top: 0; clear:both">
        <table id="gamePanels" class="panel" style="display:none">
          <thead class="isNotKenoGame">
            <tr>
              <th><strong>Panel no.</strong></th>
              <th><strong>Selected Tickets</strong></th>
              <th><strong>Amount</strong></th>
              <th> <!--                         <strong>Remove</strong> -->
              </th>
            </tr>
          </thead>          
          <tbody id="draws_table">
          </tbody>
        </table>     
      <table >
        <tbody>
          <tr>
            <!--                                            <td></td><td><input type="button" id="play_game" class="play_button" value="Play"></td>-->
            <td ><input type="hidden" class="" id="sum_amt" value="0" />
            <div class="dx-button2" id="play_game_not_keno"><span class="dx-button-text">Play Now</span></div> 
             </td>
            <td ><div class="dx-button2" id="showOverlayBalance"><span class="dx-button-text">Check Balance</span></div></td>
            <td ><a target="_blank" href="https://www.itzcash.com/myplaywin/jsp/Login.jsp?displaymode=PLY" ><div class="dx-button2" id="showMyAccount"><span class="dx-button-text">View Transactions</span></div></a> </td>
          </tr>
        </tbody>
      </table>
      </div>
      <!--panel table start-->
      <div id="pnlDetails" class="isKenoGame"
                         style="display: none; margin-top: 0; clear:both">
        <table id="gamePanels" class="panel">
          
          <thead class="isKenoGame">
            <tr>
              <th><strong>Panel no.</strong></th>
              <th ><strong>Spots</strong></th>
              <th><strong>Selected Tickets</strong></th>
              <th><strong>Amount</strong></th>
              <th> <!--                         <strong>Remove</strong> -->
              </th>
            </tr>
          </thead>
          <tbody id="draws_table">
          </tbody>
        </table>     
      <table>
        <tbody>
          <tr>
            <!--                                            <td></td><td><input type="button" id="play_game" class="play_button" value="Play"></td>-->
            <td ><input type="hidden" class="" id="sum_amt" value="0" />
            <div class="dx-button2" id="play_game"><span class="dx-button-text">Play Now</span></div> 
             </td>
            <td ><div class="dx-button2" id="showOverlayBalance"><span class="dx-button-text">Check Balance</span></div></td>
            <td ><a target="_blank" href="https://www.itzcash.com/myplaywin/jsp/Login.jsp?displaymode=PLY" ><div class="dx-button2" id="showMyAccount"><span class="dx-button-text">View Transactions</span></div></a> </td>
          </tr>
        </tbody>
      </table>
      </div>
      <!--panel table end-->
    </div>
    <!--   html end -->
  </div>
  <!-- content end -->
<!--  <div data-role="footer" data-position="fixed" >
    <div> © 2013-2014 Pan India Network Ltd. <a href="Disclaimer.php" style="text-decoration: underline;cursor: pointer;color: #bbb">Disclaimer</a> , <a href="TandC.php" style="text-decoration: underline;cursor: pointer;color: #bbb">T & C </a> </div>
  </div>-->
 <?php include("../include/footer.php"); ?>
</div>
<div data-role="page"  data-theme="a" id="playGameSuccess" style="display:none;">
  <div data-role="header" data-position="fixed">
    <!--                            <div style="float: left"><h1>PlayWin</h1></div>-->
    <h1 id="main_heading"></h1>
    <a href="home.php" class="back_head" data-icon="home" data-iconpos="notext"></a>
    <!--                            <div style="float: left"><h1>PlayWin</h1></div>-->
  </div>
  <script type="text/javascript">
                 //Playwin.play.playGameResponse
  
             </script>
  <div id="playGameSuccessContent" data-role="content" data-theme="a"> </div>
  <p style="text-align: center; margin-top: 10px; display: none" id="gameBanner" > <a href="#" class="playGameSuccess" data-position-to="window"  data-inline="true"  data-theme="a"><img class="playGameSuccess" id="ad_image" src="" /> </a></p>
   <!-- footer -->
      
</div>
</body>
</html>
