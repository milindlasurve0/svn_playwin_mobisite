
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" >
                <title>Results</title>
		
<link rel="stylesheet" href="../themes/plwin.min.css" />
<link rel="stylesheet" href="../css/common.css" />
<link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
    <link rel="stylesheet" href="../themes/jquery.mobile.structure.css" />

                 <link rel="stylesheet" href="../css/GameList.css" />
                 
		<!--		<script src="http://code.jquery.com/jquery-1.10.2.min.js" ></script>-->
                <script src="../js/jquery.js" ></script>
<!--		<script src="http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>-->
                <script src="../js/jquery.mobile-1.4.0.min.js"></script>
                
                <script src="../js/xml2json.js?1"></script>
                
                <script src="../js/fullcalendar.min.js"></script>
                
                <script src="../js/playwin.core.js" ></script>
                <script src="../js/playwin.config.js" ></script>
                <script src="../js/blinktext.js"></script>

	</head>
	<body>
            <style type="text/css">
            #resultListPopUp-popup {
                width: 90%;
                /*height: 100%;*/
                left:5%;
                right:5%;
            }
                
            h1 {color:red;}
            .dx-theme-ios .dx-list .dx-list-item, .dx-theme-ios .dx-list .dx-empty-message {
                border-bottom: 1px solid #E0E0E0;
                font-size: 13pt;
                font-weight: 200;
                line-height: 1.3636;
                overflow: hidden;
                padding: 13px 10px;
                position: relative;
                text-overflow: ellipsis;
                white-space: nowrap;
            }
                
                
                .dx-button {
	display:inline-block;
	margin:1px 0;
	cursor:pointer;
	text-align:center;
	vertical-align:middle;
	max-width:100%;
	-webkit-user-select:none;
	-moz-user-select:none;
	-ms-user-select:none;
	-o-user-select:none;
	user-select:none;
	-webkit-user-drag:none;
	-moz-user-drag:none;
	-ms-user-drag:none;
	-o-user-drag:none;
	user-drag:none
}
.dx-button1 {
	background-color:#efe95f;
	color:#385487;
	border:0;
	-webkit-box-shadow:2px 2px 2px 0 #4e87b1;
	-moz-box-shadow:2px 2px 2px 0 #4e87b1;
	-ms-box-shadow:2px 2px 2px 0 #4e87b1;
	-o-box-shadow:2px 2px 2px 0 #4e87b1;
	box-shadow:2px 2px 2px 0 #4e87b1;
	padding:6px;
	background:#efe95f url(/content/images/arrow.png) no-repeat 95% 9px
}
.blue .dx-button2, .yellow .dx-button2 {
	cursor:pointer;
	background-color:#226494;
	color:#fff;
	border:0;
	padding:2px;
	padding-top:0;
	-webkit-border-radius:2px;
	-moz-border-radius:2px;
	-ms-border-radius:2px;
	-o-border-radius:2px;
	border-radius:2px;
	margin: 2px 0;
}
.yellow .dx-button2 {
	background-color:#630
}
.dx-button2 .dx-button-text {
	font-size:9pt;
	font-family:Calibri;
	font-weight:bold;
	color:#fff;
	padding:0 2px;
	display:inline;
	text-shadow:0 -1px 0 rgba(0, 0, 0, .5)
}
.dx-button .dx-button-content {
	text-overflow:ellipsis;
	overflow:hidden;
	max-height:100%
}
.dx-button.dx-state-disabled {
	cursor:default
}
.dx-button .dx-button-link {
	text-decoration:none
}
.dx-button .dx-icon {
	display:inline-block;
	vertical-align:middle
}
.dx-button .dx-button-text {
	display:inline;
	vertical-align:middle
}</style>
		<div data-role="page" data-theme="a">
<!--			<div data-role="header" data-position="fixed">
                                <h1>Results</h1>
                                <a href="home.php" class="back_head" data-icon="home" data-iconpos="notext">Menu</a>
                               <div class="ticker">
                               <div class="blinking" ></div>
                        </div>
                        </div>-->
                          <?php include("../include/header.php"); ?>
			<div data-role="content" data-theme="a">
<!--				<p>Your theme was successfully downloaded. You can use this page as a reference for how to link it up!</p>
				<pre>
<strong>&lt;link rel=&quot;stylesheet&quot; href=&quot;themes/plwin.min.css&quot; /&gt;</strong>
&lt;link rel=&quot;stylesheet&quot; href=&quot;http://code.jquery.com/mobile/1.4.0/jquery.mobile.structure-1.4.0.min.css&quot; /&gt;
&lt;script src=&quot;http://code.jquery.com/jquery-1.10.2.min.js&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;http://code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js&quot;&gt;&lt;/script&gt;
				</pre>
				<p>This is content color swatch "A" and a preview of a <a href="#" class="ui-link">link</a>.</p>
				<label for="slider1">Input slider:</label>
				<input type="range" name="slider1" id="slider1" value="50" min="0" max="100" data-theme="a" />
				<fieldset data-role="controlgroup"  data-type="horizontal" data-role="fieldcontain">
				<legend>Cache settings:</legend>
				<input type="radio" name="radio-choice-a1" id="radio-choice-a1" value="on" checked="checked" />
				<label for="radio-choice-a1">On</label>
				<input type="radio" name="radio-choice-a1" id="radio-choice-b1" value="off"  />
				<label for="radio-choice-b1">Off</label>
				</fieldset>-->

                        <div>
                            <script type= "text/javascript">
                              eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('$(7(){$.2C.2B=W;$.2D.2E=W;7 1X(t,2){3 J=1l("z.13.J.g"+2);3 I=0;I=1R 23().2F();3 M=W;A(t=="2a"){A($.2A(I,J.2z)!=-1){M=11}}1r{A(t=="1o"&&J.t!="1o"){M=W}1r{A(t=="18"&&J.t!="18"){M=W}1r{M=11}}}Z M}3 1c="";3 t=s.q.9;A(!(s.q.9=="#18"||s.q.9=="#1o"||s.q.9=="#2a")){t="#2u"}3 1x=[];$.1k(z.13.1x,7(i,v){3 24=!1X(t.1h(1,t.Y),v.2);A(24){1c=1c+"<L 8=\'x:X 0;\'"+"u=\'2t\'><4 8=\'V:U 1n\'><4 8=\'C-E:0.1V;V:0;x-B:U;x-T:2v;2w-2y:2x\'><c>"+v.1u+"</c><c 2=\'2G"+v.2+"\'  8=\'C-E:0.1V;C-2H:2s\'></c><2Q/> </4><4 8=\'x-T:-1n;\'><b 8=\'C-E:0.2S;x-B:U\'><c 2=\'2T"+v.2+"\'></c> </b></4><4><4 8=\'x-T:-X\'><p 2=\'1D"+v.2+"\' u=\'1D\'></p></4></4> <4  8=\'x-T:-2U; 17:1f\'> <4 u=\'P-1m\' 8=\'1t:2P ;1a:B;x-B:X;\'><c u=\'P-1w-16 29\' 2=\'2O"+v.2+"\'>2J</c></4> <4 u=\'P-1m\' 8=\'1a:B;1t:2I;x-B:X;\'><c u=\'P-1w-16 2f\' 2=\'2K"+v.2+"\'    >2L 1d</c></4> <4 u=\'P-1m\' 8=\'1t:2N;x-B:X;1a:B\'><c u=\'P-1w-16 1T\' 2=\'2M"+v.2+"\'>2V y</c></4> <4 8=\' 17:1f\'></4></4></4>"+""+""+"   </L>";1x.2r(v)}});$(\'#2p\').H(1c);$(\'.29\').R(\'N\',7(){3 2=O.2;3 r=2.K("Q");3 9=s.q.9.1h(1,s.q.9.Y);s.q.14("2o.1g"+"?9="+9+"&1L=26#"+r[1])});$(\'.2f\').R(\'N\',7(){3 e=1R 23();A(!(e.2g()>=6&&e.2g()<=21)){1Z("2k 2l 2n 2m. 2q 2j 2R 34:1F 3t 3s 10:1F 3u.");Z}3 2=O.2;3 r=2.K("Q");3 9=s.q.9.1h(1,s.q.9.Y);s.q.14("3v.1g"+"?9="+9+"&1L=26#"+r[1]);Z 11});$(\'.3w\').R(\'N\',7(){3 2=O.2;3 r=2.K("Q");s.q.14("1b.1g#"+r[1])});z.1i.3r("1O");$("#3q").F();$("#1v").F();$("#1y").F();$("#3m").F();$(".1T").R(\'N\',7(){3 n={};n.t=t;3 r=O.2.K("Q");$("#1j").I(r[1]);1U(n)});7 1U(n){3 r=n.t=="18"?[{2:5,w:"D 5 y"},{2:10,w:"D 10 y"},{2:20,w:"D 20 y"},{2:1e,w:"D 1e y"},{2:19,w:"1A y ( 1M 19 )"}]:[{2:5,w:"D 5 y"},{2:10,w:"D 10 y"},{2:20,w:"D 20 y"},{2:1e,w:"D 1e y"},{2:19,w:"1A y ( 1M 19 )"}];1S(r);3 n=1l("z.13.J."+"g"+$("#1j").I());$("#1H").H(n.1u);$("#1v").F("22")}7 3l(n){3y.3n(11)}7 3o(12){$("#1v").F("1G");3 1N=$("#1j").I();3 n=1l("z.13.J."+"g"+1N);$("#1K").H(n.1u);1I(n,12)}7 1I(n,12){3 3p=[];$.25(z.13.3x.1Y+"&3A="+n.2+"&12="+12).3z(7(d){$("#1y").F("1G");3 e=$.3B(d);d=3j;3 j=(!e.1b)||(e.1b.Y==0)?[]:e.1b;A(j.1d){j=[j]}3 f=$.2W(j,7(k){3 h=$.1z(k.1s);3 i=h.K(" ");3 o="";$.1k(i,7(l,m){A($.1z(m)!=""){o=o+"<4 "+(m!="33"?"u=\'3k\'>":"8=\'1a:B;V-T:U;\'>")+m+"</4>"}});Z{1d:k.1d,1s:k.1s,15:k.15,27:o,1q:z.1i.1C(k.1q,"1W, 1Q-2h-2d 2c:2b 2i"),1J:z.1i.1C(k.1J,"1W, 1Q-2h-2d 2c:2b 2i"),28:k.28}});3 G="";$.1k(f,7(i,v){G=G+"<L u=\'1B\' 8=\'x:1n 2e; V:35 2e; C-E:S%\'>"+"<4 8=\\"C-E:S%;17:1f\\" >"+v.1q+"</4>"+"<4 8=\\"C-E:S%; 36-32:31\\">"+"<c >"+v.27+"</c>"+"</4>"+"<4 8=\\"C-E:S%;17:1f\\">"+"2X 2Y:"+"<c n-2Z=\\"16:$n.15\\" 8=\\"C-E:S%\\">"+v.15+"</c>"+"</4>"+"</L>"});$("#30").H(G);$("#1y").F("22")}).37(7(){1Z("38 : 3f 25 1Y 3g.")})}7 1S(r){3 G="";$.1k(r,7(i,v){G=G+"<L 3h=\\""+v.2+"\\" u=\'1B\' 8=\'V: U 0;\'><a 2=\\"3i"+v.2+"\\" u=\\"1E\\"  >"+v.w+"</a></L>"});$("#3e").H(G);$(".1E").R(\'N\',7(){3 2=O.2;3 r=2.K("Q");3 9=s.q.9.1h(1,s.q.9.Y);s.q.14("3d.1g#"+$("#1j").I()+"Q"+r[1])})}7 39(1p){$("#1H").H(1p);$("#1K").H(1p)}$(1P).R("N",".3a",7(){1P.q.14(O.3b);Z 11});z.1i.3c("1O")});',62,224,'||id|var|div|||function|style|hash|||span|||||||||||data|||location|arr|window|type|class||caption|margin|Results|Playwin|if|left|font|Last|size|popup|str|html|val|gameDetails|split|li|ret|click|this|dx|_|on|90|top|5px|padding|true|10px|length|return||false|count|config|replace|DrawID|text|clear|daily|100|float|GameResult|listHtml|Game|50|both|php|substring|core|noOfResultGameId|each|eval|button2|3px|weekly|title|DrawDate|else|Result|width|name|noOfResultListPopUp|button|gameList|resultListPopUp|trim|All|list|getFormatedDateTime|resultDiv|noOfResultClass|00|close|noOfResultListTitle|setOverLayResults|NextDrawDate|resultListTitle|typePage|Max|GameID|result|document|dd|new|setNoOfResultList|pastresult|showNoOfResultList|9em|dddd|getWeekStatus|results|alert|||open|Date|weekStatus|get|gameResult|ResultHtml|NextFirstPrize|gameinfo|today|mm|hh|yyyy|0px|gameplay|getHours|MMM|TT|allowed|The|panel|closed|is|GameInfo|gameslist|Gaming|push|bold|yellow|all|2px|line|1em|height|weekDays|inArray|cors|support|mobile|allowCrossDomainPages|getDay|rolldown_amt_|weight|72px|Info|gameplay_|Play|gameresult_|85px|gameinfo_|70px|br|from|8em|draw_date_|8px|Past|map|Draw|Id|bind|resultList|normal|space|TB|07|4px|white|fail|Error|setResultTitle|back_head|href|setBannerImage|gameResultList|noOfResultList|Unable|info|value|noresult_|null|resultBall|hideNoOfResultList|showMoreBallsPopUp|isShowNoOfResultList|showOverLayResults|overLayResults|resultLoading|loadResultData|to|AM|PM|playGame|gameresult|urls|viewModel|done|gameid|xml2json'.split('|'),0,{}))

                            </script>
                            <div data-role="popup" id="resultLoading" data-dismissible='false' class="ui-content">
                                    <p> Loading .... </p>
                            </div>
                            <div class="content-primary">	
                                <ul data-role="listview" id="gameslist" >
                                    
                                </ul>
                                <p style="text-align: center; margin-top: 10px" >
                           <a href="#" class="result" data-position-to="window"  data-inline="true"  data-theme="a"><img class="result" id="ad_image" src="" /> </a></p>
                            </div><!--/content-primary -->
                            
                            <div data-role="popup" id="noOfResultListPopUp" class="ui-content">
                                 
                                <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
                                 <input type="hidden" id="noOfResultGameId" value ="" />
                                  <div>
            
                                 <div id="noOfResultListTitle" class="dx-popup-title"></div>
                                    <ul data-role="listview" id="noOfResultList" >

                                    </ul>

                            </div>
                            </div>
                            <div data-role="popup" id="resultListPopUp" class="ui-content">
                            <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
                                 <div style="height:300px;overflow-y:scroll">
                                <div id="resultListTitle" class="dx-popup-title"></div>
                                <a href="#" class="pastResults" data-position-to="window"  data-inline="true"  data-theme="a"><img class="pastResults" id="ad_image" src="" /> </a> 
                                <input type="hidden" id="resultGameCount" value ="" />
                                 
                                    <ul  class="dx-list-item" data-role="listview" id="resultList" >

                                    </ul>
                                 </div>
                            </div>
                            <div data-role="popup" id="showMoreBallsPopUp" class="ui-content" style="width:200px; height:140px">
                                <a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>
                                <div id="showMoreBalls" ></div>

                            </div>
                        </div>

			</div>
                       
                              <!-- footer -->
       <?php include("../include/footer.php"); ?>

                           
       
		</div>
	</body>
</html>